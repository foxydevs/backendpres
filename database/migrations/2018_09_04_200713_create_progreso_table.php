<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgresoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('progreso', function (Blueprint $table) {
            $table->increments('id');
            $table->string('foto_espalda')->nullable()->default(null);
            $table->string('foto_frente')->nullable()->default(null);
            $table->string('comentario')->nullable()->default(null);
            $table->string('complexion')->nullable()->default(null);
            $table->string('imc')->nullable()->default(null);
            $table->string('opcion1')->nullable()->default(null);
            $table->string('opcion2')->nullable()->default(null);
            $table->timestamp('fecha')->useCurrent();
            $table->double('peso')->nullable()->default(null);
            $table->double('grasa')->nullable()->default(null);
            $table->double('masa_muscular')->nullable()->default(null);
            $table->double('imc_num')->nullable()->default(null);
            $table->double('opcionNum1')->nullable()->default(null);
            $table->double('opcionNum2')->nullable()->default(null);
            $table->integer('state')->default(1);
            $table->integer('tipo')->default(1);

            $table->integer('client')->nullable()->default(null)->unsigned();
            $table->foreign('client')->references('id')->on('users')->onDelete('cascade');

            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('progreso');
    }
}
