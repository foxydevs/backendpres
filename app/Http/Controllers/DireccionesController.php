<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Direcciones;
use Response;
use Validator;
class DireccionesController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
    return Response::json(Direcciones::all(), 200);
}

public function getThisByFilter(Request $request, $id,$state)
{
    if($request->get('filter')){
        switch ($request->get('filter')) {
            case 'state':{
                $objectSee = Direcciones::whereRaw('app=? and state=?',[$id,$state])->with('apps','clientes')->get();
                break;
            }
            case 'type':{
                $objectSee = Direcciones::whereRaw('app=? and tipo=?',[$id,$state])->with('apps','clientes')->get();
                break;
            }
            default:{
                $objectSee = Direcciones::whereRaw('app=? and state=?',[$id,$state])->with('apps','clientes')->get();
                break;
            }

        }
    }else{
        $objectSee = Direcciones::whereRaw('app=? and state=?',[$id,$state])->with('apps','clientes')->get();
    }

    if ($objectSee) {
        return Response::json($objectSee, 200);

    }
    else {
        $returnData = array (
            'status' => 404,
            'message' => 'No record found'
        );
        return Response::json($returnData, 404);
    }
}

public function getThisByUser($id)
{
    $objectSee = Direcciones::where('app','=',$id)->with('apps')->get();
    if ($objectSee) {

        return Response::json($objectSee, 200);

    }
    else {
        $returnData = array (
            'status' => 404,
            'message' => 'No record found'
        );
        return Response::json($returnData, 404);
    }
}

public function getThisByClient($id)
{
    $objectSee = Direcciones::where('client','=',$id)->with('clientes')->get();
    if ($objectSee) {

        return Response::json($objectSee, 200);

    }
    else {
        $returnData = array (
            'status' => 404,
            'message' => 'No record found'
        );
        return Response::json($returnData, 404);
    }
}

/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
    //
}

/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
    $validator = Validator::make($request->all(), [
        'client'          => 'required',
        'app'          => 'required',
    ]);
    if ( $validator->fails() ) {
        $returnData = array (
            'status' => 400,
            'message' => 'Invalid Parameters',
            'validator' => $validator
        );
        return Response::json($returnData, 400);
    }
    else {
        try {
            $newObject = new Direcciones();
            $newObject->direccion            = $request->get('direccion');
            $newObject->ciudad            = $request->get('ciudad');
            $newObject->calle            = $request->get('calle');
            $newObject->numero            = $request->get('numero');
            $newObject->observacion            = $request->get('observacion');
            $newObject->tipo            = $request->get('tipo');
            $newObject->state            = $request->get('state');
            $newObject->app            = $request->get('app');
            $newObject->client            = $request->get('client');
            $newObject->save();
            return Response::json($newObject, 200);

        } catch (Exception $e) {
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
    }
}

public function uploadAvatar(Request $request, $id)
{
    $validator = Validator::make($request->all(), [
        'avatar'      => 'required|image|mimes:jpeg,png,jpg'
    ]);

    if ($validator->fails()) {
        $returnData = array(
            'status' => 400,
            'message' => 'Invalid Parameters',
            'validator' => $validator->messages()->toJson()
        );
        return Response::json($returnData, 400);
    }
    else {
        try {

            $path = Storage::disk('s3')->put($request->carpeta, $request->avatar);

            $objectUpdate->picture = Storage::disk('s3')->url($path);
            $objectUpdate->save();

            return Response::json($objectUpdate, 200);

        }
        catch (Exception $e) {
            $returnData = array(
                'status' => 500,
                'message' => $e->getMessage()
            );
        }

    }
}

/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
    $objectSee = Direcciones::find($id);
    if ($objectSee) {

        return Response::json($objectSee, 200);

    }
    else {
        $returnData = array (
            'status' => 404,
            'message' => 'No record found'
        );
        return Response::json($returnData, 404);
    }
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
    //
}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
    $objectUpdate = Direcciones::find($id);
    if ($objectUpdate) {
        try {
            $newObject->direccion            = $request->get('direccion', $objectUpdate->direccion);
            $newObject->ciudad            = $request->get('ciudad', $objectUpdate->ciudad);
            $newObject->calle            = $request->get('calle', $objectUpdate->calle);
            $newObject->numero            = $request->get('numero', $objectUpdate->numero);
            $newObject->observacion            = $request->get('observacion', $objectUpdate->observacion);
            $newObject->tipo            = $request->get('tipo', $objectUpdate->tipo);
            $newObject->state            = $request->get('state', $objectUpdate->state);
            $newObject->app            = $request->get('app', $objectUpdate->app);
            $newObject->client            = $request->get('client', $objectUpdate->client);
            $objectUpdate->save();
            return Response::json($objectUpdate, 200);
        } catch (Exception $e) {
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
    }
    else {
        $returnData = array (
            'status' => 404,
            'message' => 'No record found'
        );
        return Response::json($returnData, 404);
    }
}


/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
    $objectDelete = Direcciones::find($id);
    if ($objectDelete) {
        try {
            Direcciones::destroy($id);
            return Response::json($objectDelete, 200);
        } catch (Exception $e) {
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
    }
    else {
        $returnData = array (
            'status' => 404,
            'message' => 'No record found'
        );
        return Response::json($returnData, 404);
    }
}
}
