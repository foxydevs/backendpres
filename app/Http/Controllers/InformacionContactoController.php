<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\InformacionContacto;
use Response;
use Validator;

class InformacionContactoController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(InformacionContacto::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = InformacionContacto::whereRaw('user=? and state=?',[$id,$state])->with('apps','clients','users')->get();
                    break;
                }
                case 'type':{
                    $objectSee = InformacionContacto::whereRaw('user=? and tipo=?',[$id,$state])->with('apps','clients','users')->get();
                    break;
                }
                default:{
                    $objectSee = InformacionContacto::whereRaw('user=? and state=?',[$id,$state])->with('apps','clients','users')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = InformacionContacto::whereRaw('user=? and state=?',[$id,$state])->with('apps','clients','users')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = InformacionContacto::where('app','=',$id)->with('apps','clients','users')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = InformacionContacto::where('app','=',$id)->with('apps','clients','users')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'app'          => 'required',
            'title'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new InformacionContacto();
                $newObject->title            = $request->get('title',null);
                $newObject->description            = $request->get('description',null);
                $newObject->icono            = $request->get('icono',null);
                $newObject->color            = $request->get('color',null);
                $newObject->colorBg            = $request->get('colorBg',null);
                $newObject->round            = $request->get('round',null);
                $newObject->direccion            = $request->get('direccion',null);
                $newObject->picture            = $request->get('picture',null);
                $newObject->video            = $request->get('video',null);
                $newObject->link            = $request->get('link',null);
                $newObject->state            = $request->get('state',null);
                $newObject->type            = $request->get('type',null);
                $newObject->app            = $request->get('app',null);
                $newObject->client            = $request->get('client',null);
                $newObject->user            = $request->get('user',null);
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    public function uploadAvatar(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'avatar'      => 'required|image|mimes:jpeg,png,jpg'
        ]);
    
        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
    
                $path = Storage::disk('s3')->put($request->carpeta, $request->avatar);
    
                $objectUpdate->picture = Storage::disk('s3')->url($path);
                $objectUpdate->save();
    
                return Response::json($objectUpdate, 200);
    
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
            }
    
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = InformacionContacto::find($id);
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = InformacionContacto::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->title            = $request->get('title', $objectUpdate->title);
                $objectUpdate->description            = $request->get('description', $objectUpdate->description);
                $objectUpdate->icono            = $request->get('icono', $objectUpdate->icono);
                $objectUpdate->color            = $request->get('color', $objectUpdate->color);
                $objectUpdate->colorBg            = $request->get('colorBg', $objectUpdate->colorBg);
                $objectUpdate->round            = $request->get('round', $objectUpdate->round);
                $objectUpdate->direccion            = $request->get('direccion', $objectUpdate->direccion);
                $objectUpdate->picture            = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->video            = $request->get('video', $objectUpdate->video);
                $objectUpdate->link            = $request->get('link', $objectUpdate->link);
                $objectUpdate->state            = $request->get('state', $objectUpdate->state);
                $objectUpdate->type            = $request->get('type', $objectUpdate->type);
                $objectUpdate->app            = $request->get('app', $objectUpdate->app);
                $objectUpdate->client            = $request->get('client', $objectUpdate->client);
                $objectUpdate->user            = $request->get('user', $objectUpdate->user);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = InformacionContacto::find($id);
        if ($objectDelete) {
            try {
                InformacionContacto::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
