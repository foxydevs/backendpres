<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Retos extends Model
{
    protected $table = 'retos';

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function comments() {
        return $this->hasMany('App\RetosComments', 'reto')->whereRaw('parent IS NULL')->with('comments','users');
    }
}
