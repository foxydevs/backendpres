<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo')->nullable()->default(null);
            $table->string('descripcion')->nullable()->default(null);
            $table->string('comentario')->nullable()->default(null);
            $table->timestamp('fechaInicio')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fechaFin')->nullable()->default(null);

            $table->integer('aprobacion')->default(0);
            $table->integer('state')->default(1);
            
            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            
            $table->integer('client')->nullable()->default(null)->unsigned();
            $table->foreign('client')->references('id')->on('users')->onDelete('cascade');
            
            $table->integer('product')->nullable()->default(null)->unsigned();
            $table->foreign('product')->references('id')->on('products')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas');
    }
}
