<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkoutsComments extends Model
{
    protected $table = 'workouts_comments';
}
