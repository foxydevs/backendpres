<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tips3;
use Response;
use Validator;

class Tips3Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Tips3::all(), 200);
    }

    public function getThisByUser($id)
    {
        $objectSee = Tips3::where('user','=',$id)->with('users')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'          => 'required',
            'user'                 => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Tips3();
                $newObject->title            = $request->get('title');
                $newObject->description            = $request->get('description');
                $newObject->picture            = $request->get('picture');
                $newObject->state            = $request->get('state',1);
                $newObject->user            = $request->get('user');
                $newObject->product            = $request->get('product');
                $newObject->event            = $request->get('event');
                $newObject->link            = $request->get('link');
                $newObject->video            = $request->get('video');
                $newObject->save();
                return Response::json($newObject, 200);
            
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Tips3::find($id);
        if ($objectSee) {
            $objectSee->users;
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    public function getTips3ByUser($id)
    {
        $objectSee = Tips3::where('user','=',$id)->with('users')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Tips3::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->title            = $request->get('title', $objectUpdate->title);
                $objectUpdate->description            = $request->get('description', $objectUpdate->description);
                $objectUpdate->picture            = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->state            = $request->get('state', $objectUpdate->state);
                $objectUpdate->user            = $request->get('user', $objectUpdate->user);
                $objectUpdate->product            = $request->get('product', $objectUpdate->product);
                $objectUpdate->event            = $request->get('event', $objectUpdate->event);
                $objectUpdate->link            = $request->get('link', $objectUpdate->link);
                $objectUpdate->video            = $request->get('video', $objectUpdate->video);
                $objectUpdate->save();
                $objectUpdate->users;
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Tips3::find($id);
        if ($objectDelete) {
            try {
                Tips3::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
