<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\UserInterest;
use App\Users;
use Response;
use Validator;
use Storage;
use DB;

class UserInterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(UserInterest::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'name' => 'required'
        ]);
        if($validator->fails() ) {
            $returnData = array (
                'status'    => 400,
                'message'   => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        } else {
           try {
               $newObject = new UserInterest();
               $newObject->user  = $request->get('user');
               $newObject->name  = $request->get('name');
               $newObject->save();
               return Response::json($newObject, 200);
           } catch (Exception $e) {
               $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
               );
               return Response::json($returnData, 500);
           }
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = UserInterest::find($id);
        if ($objectSee) {
            $objectSee->user;
            $objectSee->userInterest;
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = UserInterest::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->user = $request->get('user', $objectUpdate->user);
                $objectUpdate->name = $request->get('name', $objectUpdate->name);

                $objectUpdate->userInterest;
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function interestsByUser($id)
    {
        try{
            $myInterest = UserInterest::where('user', '=', $id)->groupBy('name')->pluck('name');
            $myFriendsInterest = DB::table('interest')->select(DB::raw('name, count(*) as user_count'))->whereNotIn('name', $myInterest)->groupBy('name')->get();
            $myInterest = UserInterest::where('user', '=', $id)->get();
            
            $returnData = array (
                'myInterest' => $myInterest,
                'otherInterests' => $myFriendsInterest
            );

            return Response::json($returnData, 200);
        }catch(Exception $e){
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = UserInterest::find($id);
        if ($objectDelete) {
            try {
                UserInterest::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
