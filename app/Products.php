<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';

    public function categorys() {
        return $this->hasOne('App\Categorys', 'id', 'category');
    }
    

    public function pictures() {
        return $this->hasMany('App\PicturesProducts', 'product', 'id');
    }
    

    public function presentaciones_varias() {
        return $this->hasMany('App\PresentacionProducto', 'product', 'id');
    }
    

    public function presentaciones() {
        return $this->hasOne('App\Presentacion', 'id', 'presentacion');
    }
}
