<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motivacion extends Model
{
    protected $table = 'motivación';

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function comments() {
        return $this->hasMany('App\BlogComments', 'blog');
    }
}
