<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Users;
use App\Events;
use App\UserEvents;
use App\UsersClients;
use Response;
use Validator;
use Auth;

class AuthenticateController extends Controller
{
    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'username'  => 'required',
            'password'  => 'required'
        ]);

        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {

                $userdata = array(
                    'username'  => $request->get('username'),
                    'password'  => $request->get('password')
                );

                if(Auth::attempt($userdata, true)) {
                    $user = Users::find(Auth::user()->id);
                    $user->last_conection = date('Y-m-d H:i:s');
                    $user->one_signal_id = $request->get('one_signal_id', $user->one_signal_id);
                    $user->save();
                    return Response::json($user, 200);
                }
                else {
                    $returnData = array (
                        'status' => 401,
                        'message' => 'No valid Username or Password'
                    );
                    return Response::json($returnData, 401);
                }

                return Response::json($newObject, 200);

            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    public function loginID($id, Request $request) {
        $validator = Validator::make($request->all(), [
            'username'  => 'required',
            'password'  => 'required'
        ]);

        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {

                $userdata = array(
                    'username'  => $request->get('username'),
                    'password'  => $request->get('password')
                );

                if(Auth::attempt($userdata, true)) {
                    $user = Users::find(Auth::user()->id);
                    $user->last_conection = date('Y-m-d H:i:s');
                    $user->one_signal_id = $request->get('one_signal_id', $user->one_signal_id);
                    $user->save();
                    $client = UsersClients::whereRaw('user=? and client=?',[$id,$user->id])->first();

                    if($client)
                    {
                        return Response::json($user, 200);
                    
                    }else{
                        
                        $client = UsersClients::whereRaw('user=?',[$user->id])->first();
                        if($client){
                            return Response::json($user, 200);
                        }else{
                            if($user->id==$id){
                                return Response::json($user, 200);
                            }else{
                                try {
                                    $newObject = new UsersClients();
                                    $newObject->user            = $id;
                                    $newObject->client          = $user->id;
                                    $newObject->save();
                                    return Response::json($user, 200);
                                
                                } catch (Exception $e) {
                                    $returnData = array (
                                        'status' => 500,
                                        'message' => $e->getMessage()
                                    );
                                    return Response::json($returnData, 500);
                                }
                            }
                        }
                    }
                }
                else {
                    $returnData = array (
                        'status' => 401,
                        'message' => 'No valid Username or Password'
                    );
                    return Response::json($returnData, 401);
                }

                return Response::json($newObject, 200);

            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    public function loginAdmin(Request $request) {
        $validator = Validator::make($request->all(), [
            'username'  => 'required',
            'password'  => 'required'
        ]);

        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {

                $userdata = array(
                    'username'  => $request->get('username'),
                    'password'  => $request->get('password')
                );

                if(Auth::attempt($userdata, true)) {
                    $user = Users::where('id', '=', Auth::user()->id)->where('admin','=','1')->first();
                    if($user){
                        $token = JWTAuth::fromUser($user);
                        $user->token = $token;
                        return Response::json($user, 200);
                    } else {
                        $returnData = array (
                            'status' => 401,
                            'message' => 'No valid Username or Password'
                        );
                        return Response::json($returnData, 401);
                    }
                }
                else {
                    $returnData = array (
                        'status' => 401,
                        'message' => 'No valid Username or Password'
                    );
                    return Response::json($returnData, 401);
                }

                return Response::json($newObject, 200);

            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    public function logout(Request $request) {
        return 'adios';
    }
}
