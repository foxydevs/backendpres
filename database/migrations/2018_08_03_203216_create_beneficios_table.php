<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeneficiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->string('link1')->nullable()->default(null);
            $table->string('link2')->nullable()->default(null);
            $table->string('link3')->nullable()->default(null);
            $table->integer('state')->default(1);

            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');

            $table->integer('comercio')->nullable()->default(null)->unsigned();
            $table->foreign('comercio')->references('id')->on('comercios')->onDelete('cascade');

            $table->integer('event')->nullable()->default(null)->unsigned();
            $table->foreign('event')->references('id')->on('events')->onDelete('cascade');

            $table->integer('tipo')->nullable()->default(null)->unsigned();
            $table->foreign('tipo')->references('id')->on('tipo_beneficios')->onDelete('cascade');

            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beneficios');
    }
}
