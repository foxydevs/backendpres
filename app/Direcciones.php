<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direcciones extends Model
{
    protected $table = 'direcciones';

    public function apps() {
        return $this->hasOne('App\Users', 'id', 'app');
    }

    public function clientes() {
        return $this->hasOne('App\Users', 'id', 'client');
    }
}
