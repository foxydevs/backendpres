<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ventas extends Model
{
    protected $table = 'ventas';

    public function detalle(){
        return $this->hasMany('App\VentasDetalle','venta','id')->with('productos');
    }
    public function ordenes(){
        return $this->hasMany('App\Orders','venta','id')->with('products');
    }

    public function clientes(){
        return $this->hasOne('App\Users','id','cliente');
    }

    public function tipos(){
        return $this->hasOne('App\TiposVenta','id','tipo');
    }
}
