<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(ModulosSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(AccesosSeeder::class);
        $this->call(CategorysSeeder::class);
        $this->call(ProductsSeeder::class);
        $this->call(EventsSeeder::class);
        /*$this->call(UserInterestSeeder::class);
        $this->call(UserFriendsSeeder::class);
        $this->call(AdvertisingSeeder::class);*/
    }
}