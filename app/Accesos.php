<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Accesos extends Model
{
    use SoftDeletes;
    protected $table = 'accesos';

    public function modulos(){
        return $this->hasOne('App\Modulos','id','modulo');
    }

    public function modulosHide(){
        return $this->hasOne('App\Modulos','id','modulo')->whereRaw('tipo=0 and estado=1');
    }

    public function modulosShow(){
        return $this->hasOne('App\Modulos','id','modulo')->whereRaw('tipo=1 and estado=1');
    }
}
