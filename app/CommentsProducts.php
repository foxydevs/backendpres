<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentsProducts extends Model
{
    protected $table = 'comments_products';

    public function thiss() {
        return $this->hasOne('App\Products', 'id', 'product');
    }

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }
}
