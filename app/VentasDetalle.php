<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentasDetalle extends Model
{
    protected $table = 'ventasdetalle';

    public function productos(){
        return $this->hasOne('App\Products','id','producto');
    }
    public function presentaciones(){
        return $this->hasOne('App\PresentacionProducto','id','presentacion');
    }
}
