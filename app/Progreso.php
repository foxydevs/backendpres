<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Progreso extends Model
{
    protected $table = 'progreso';

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function clients() {
        return $this->hasOne('App\Users', 'id', 'client');
    }

}
