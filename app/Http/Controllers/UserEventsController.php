<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\UserEvents;
use Response;
use Validator;
use Storage;
use DB;

class UserEventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(UserEvents::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = $request->get('event');
        $user = $request->get('user');
        $UserEvent  = UserEvents::whereRaw("user = ? AND event = ?", [$user,$event])->first();
        if(!$UserEvent){
            $validator = Validator::make($request->all(), [
                'state'          => 'required',
                'user'          => 'required',
                'event'          => 'required'
            ]);
            if ( $validator->fails() ) {
                $returnData = array (
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $newObject = new UserEvents();
                    $newObject->state            = $request->get('state');
                    $newObject->event            = $event;
                    $newObject->user            = $user;
                    $newObject->save();
                    return Response::json($newObject, 200);
                
                } catch (Exception $e) {
                    $returnData = array (
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                    return Response::json($returnData, 500);
                }
                
            }
        } else {
            $UserEvent  = UserEvents::whereRaw("user = ? AND event = ?", [$user,$event])->first();
            if ($UserEvent) {
                try {
                    $UserEvent->state = $request->get('state', $UserEvent->state);
            
                    $UserEvent->save();
                    return Response::json($UserEvent, 200);
                } catch (Exception $e) {
                    $returnData = array (
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                    return Response::json($returnData, 500);
                }
            }
            else {
                $returnData = array (
                    'status' => 404,
                    'message' => 'No record found'
                );
                return Response::json($returnData, 404);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = UserEvents::find($id);
        if ($objectSee) {
            $objectSee->state;

            $objectSee->users;
            $objectSee->events;
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = UserEvents::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
        
                $objectUpdate->users;
                $objectUpdate->events;
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = UserEvents::find($id);
        if ($objectDelete) {
            try {
                UserEvents::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
