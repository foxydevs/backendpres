<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promociones extends Model
{
    protected $table = 'promociones';

    public function events() {
        return $this->hasOne('App\Events', 'id', 'event');
    }

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function comments() {
        return $this->hasMany('App\PromocionesComments', 'promocion');
    }
}
