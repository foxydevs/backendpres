<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersEnterprise extends Model
{
    protected $table = 'users_enterprise';
}
