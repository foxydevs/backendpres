<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject')->nullable()->default(null);
            $table->string('message')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->string('link1')->nullable()->default(null);
            $table->string('link2')->nullable()->default(null);
            $table->string('link3')->nullable()->default(null);
            $table->integer('state')->default(1);
            $table->integer('tipo')->default(1);
            
            $table->integer('user_send')->nullable()->default(null)->unsigned();
            $table->foreign('user_send')->references('id')->on('users')->onDelete('cascade');

            $table->integer('user_receipt')->nullable()->default(null)->unsigned();
            $table->foreign('user_receipt')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments_users');
    }
}
