<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Ubicaciones;
use App\Cerca;
use Response;
use DB;
use Validator;

class UbicacionesController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(Ubicaciones::with('clientes','apps')->get(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Ubicaciones::whereRaw('state=?',[$id,$state])->with('clientes','apps')->get();
                    break;
                }
                case 'type':{
                    $objectSee = Ubicaciones::whereRaw('tipo=?',[$id,$state])->with('clientes','apps')->get();
                    break;
                }
                case 'app':{
                    $objectSee = Ubicaciones::whereRaw('app=?',[$id,$state])->with('clientes','apps')->get();
                    break;
                }
                case 'cliente':{
                    $objectSee = Ubicaciones::whereRaw('cliente=?',[$id,$state])->with('clientes','apps')->get();
                    break;
                }
                default:{
                    $objectSee = Ubicaciones::whereRaw('state=?',[$id,$state])->with('clientes','apps')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Ubicaciones::whereRaw('state=?',[$id,$state])->with('clientes','apps')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = Ubicaciones::where('usuario','=',$id)->with('clientes','apps')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = Ubicaciones::where('app','=',$id)->with('clientes','apps')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    function getBoundaries($lat, $lng, $distance = 1, $earthRadius = 6371)
    {
        $return = array();
        
        // Los angulos para cada dirección
        $cardinalCoords = array('north' => '0',
                                'south' => '180',
                                'east' => '90',
                                'west' => '270');
        $rLat = deg2rad($lat);
        $rLng = deg2rad($lng);
        $rAngDist = $distance/$earthRadius;
        foreach ($cardinalCoords as $name => $angle)
        {
            $rAngle = deg2rad($angle);
            $rLatB = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
            $rLonB = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));
            $return[$name] = array('lat' => (float) rad2deg($rLatB), 
                                    'lng' => (float) rad2deg($rLonB));
        }
        return array('min_lat'  => $return['south']['lat'],
                    'max_lat' => $return['north']['lat'],
                    'min_lng' => $return['west']['lng'],
                    'max_lng' => $return['east']['lng']);
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
            } else {
                return $miles;
            }
    }
    public function pinNear(Request $request){
        try {

            $objectSee = Cerca::all();
            if ($objectSee) {
                $instanceIds = collect();
                foreach ($objectSee as $value) {
                    $lat = $value['latitude'];
                    $lon = $value['longitude'];
                    $distance = $value['distance'];
                    $box = $this->getBoundaries($lat, $lon, $distance);

                    $Near = DB::table('ubicaciones')->select(DB::raw('id,fecha,hora, (6371 * ACOS( 
                                                    SIN(RADIANS(latitude)) 
                                                    * SIN(RADIANS(' . $lat . ')) 
                                                    + COS(RADIANS(longitude - ' . $lon . ')) 
                                                    * COS(RADIANS(latitude)) 
                                                    * COS(RADIANS(' . $lat . '))
                                                    )
                                    ) AS distance'))->whereRaw('((latitude BETWEEN ? AND ?) AND (longitude BETWEEN ? AND ?)) HAVING distance < ? ', 
                                    [$box['min_lat'], $box['max_lat'], $box['min_lng'], $box['max_lng'], $distance])->get();
                    
                    
                    foreach ($Near as $value) {
                        $instanceIds->push($value->id);
                    }
                }
                    $Ubicaciones = Ubicaciones::whereIn('id', $instanceIds)->orderby('fecha','asc')->orderby('hora','asc')->with('clientes','apps')->get();

                    return Response::json($Ubicaciones, 200);
            }
            else {
                $returnData = array (
                    'status' => 404,
                    'message' => 'No record found'
                );
                return Response::json($returnData, 404);
            }
        } catch (Exception $e) {
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
    }

    public function pinNearMine(Request $request, $id){
        try {

            $objectSee = Cerca::whereRaw('id=?',[$id])->first();
            if ($objectSee) {
                $instanceIds = collect();
                    $lat = $objectSee->latitude;
                    $lon = $objectSee->longitude;
                    $distance = $objectSee->distance;
                    $box = $this->getBoundaries($lat, $lon, $distance);

                    $Near = DB::table('ubicaciones')->select(DB::raw('id,fecha,hora, (6371 * ACOS( 
                                                    SIN(RADIANS(latitude)) 
                                                    * SIN(RADIANS(' . $lat . ')) 
                                                    + COS(RADIANS(longitude - ' . $lon . ')) 
                                                    * COS(RADIANS(latitude)) 
                                                    * COS(RADIANS(' . $lat . '))
                                                    )
                                    ) AS distance'))->whereRaw('((latitude BETWEEN ? AND ?) AND (longitude BETWEEN ? AND ?)) HAVING distance < ? ', 
                                    [$box['min_lat'], $box['max_lat'], $box['min_lng'], $box['max_lng'], $distance])->get();
                    
                    
                    foreach ($Near as $value) {
                        $instanceIds->push($value->id);
                    }
                    $Ubicaciones = Ubicaciones::whereIn('id', $instanceIds)->orderby('fecha','asc')->orderby('hora','asc')->with('clientes','apps')->get();

                    return Response::json($Ubicaciones, 200);
            }
            else {
                $returnData = array (
                    'status' => 404,
                    'message' => 'No record found'
                );
                return Response::json($returnData, 404);
            }
        } catch (Exception $e) {
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
    }
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude'          => 'required',
            'longitude'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Ubicaciones();
                $newObject->fecha            = $request->get('fecha', null);
                $newObject->hora            = $request->get('hora', null);
                $newObject->place_id            = $request->get('place_id', null);
                $newObject->place            = $request->get('place', null);
                $newObject->direccion            = $request->get('direccion', null);
                $newObject->picture            = $request->get('picture', null);
                $newObject->description            = $request->get('description', null);
                $newObject->latitude            = $request->get('latitude', null);
                $newObject->longitude            = $request->get('longitude', null);
                $newObject->tipo            = $request->get('tipo', null);
                $newObject->state            = $request->get('state', null);
                $newObject->cliente            = $request->get('cliente', null);
                $newObject->app            = $request->get('app', null);
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    public function uploadAvatar(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'avatar'      => 'required|image|mimes:jpeg,png,jpg'
        ]);
    
        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
    
                $path = Storage::disk('s3')->put($request->carpeta, $request->avatar);
    
                $objectUpdate->picture = Storage::disk('s3')->url($path);
                $objectUpdate->save();
    
                return Response::json($objectUpdate, 200);
    
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
            }
    
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = Ubicaciones::whereRaw('id=?',$id)->with('clientes','apps')->first();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = Ubicaciones::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->fecha            = $request->get('fecha', $objectUpdate->fecha);
                $objectUpdate->hora            = $request->get('hora', $objectUpdate->hora);
                $objectUpdate->place_id            = $request->get('place_id', $objectUpdate->place_id);
                $objectUpdate->place            = $request->get('place', $objectUpdate->place);
                $objectUpdate->direccion            = $request->get('direccion', $objectUpdate->direccion);
                $objectUpdate->picture            = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->description            = $request->get('description', $objectUpdate->description);
                $objectUpdate->latitude            = $request->get('latitude', $objectUpdate->latitude);
                $objectUpdate->longitude            = $request->get('longitude', $objectUpdate->longitude);
                $objectUpdate->tipo            = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->state            = $request->get('state', $objectUpdate->state);
                $objectUpdate->cliente            = $request->get('cliente', $objectUpdate->cliente);
                $objectUpdate->app            = $request->get('app', $objectUpdate->app);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = Ubicaciones::find($id);
        if ($objectDelete) {
            try {
                Ubicaciones::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
