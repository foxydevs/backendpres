<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCercaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cerca', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo')->nullable()->default(null);
            $table->double('distance')->nullable()->default(null);
            $table->double('latitude',15,8)->nullable()->default(null);
            $table->double('longitude',15,8)->nullable()->default(null);
            $table->integer('tipo')->nullable()->default(1);
            $table->integer('state')->nullable()->default(1);

            $table->integer('user_owner')->nullable()->default(null)->unsigned();
            $table->foreign('user_owner')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cerca');
    }
}
