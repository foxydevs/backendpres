<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Users;
use App\Events;
use Response;
use Validator;
use Storage;
use DB;
use PDF;

class ReportsController extends Controller
{
    public function getUsersPerMonth(Request $request)
    {
        $begin = $request->get('begin');
        $end = $request->get('end');
        

        $reportData = Users::select(['created_at as Fecha_De_Registro', 'id', 'firstname as Nombre', 'lastname as Apellido', 'email as Correo', 'age as Edad', 'birthday as Cumpleaños', 'phone as Telefono'])->where('created_at','>',[$begin.' 00:00:00'])->where('created_at','<',[$end.' 00:00:00'])->orderBy('Nombre')->get();
        if($request->get('type') == 'JSON'){
        return Response::json($reportData, 200);
        }else{
           try{
                $viewPDF = view('pdf.UsersPerMonth', ["user" => $reportData, "begin" => $begin, "end" => $end]);
                $pdf = PDF::loadHTML($viewPDF);
                return $pdf->stream();
           } catch(Exception $e){
               $returnData = array (
                   'status' => 500,
                   
                   'message' => $e->getMessage()
               );
               return Response::json($returnData, 500);
           }
        }
        
    }

    public function getEventsPerMonth(Request $request)
    {
        $begin = $request->get('begin');
        $end = $request->get('end');
        
        $reportData = DB::table('events')->join('users', 'users.id', '=', 'events.user_created')->join('user_events', 'users.id', '=', 'user_events.user')->select(DB::raw('events.created_at as Fecha_De_Registro, events.id, date as Fecha, time as Hora, users.firstname as Nombre, users.lastname as Apellido, (select count(*) from user_events where events.id=user_events.event and state=1) as Asistentes, (select count(*) from user_events where events.id=user_events.event and state=0) as Interesados'))->where('events.created_at','>',[$begin.' 00:00:00'])->where('events.created_at','<',[$end.' 00:00:00'])->orderBy('date','desc')->orderBy('time','desc')->get();
        
        if($request->get('type') == 'JSON'){
        return Response::json($reportData, 200);
        }else{
           try{
                $viewPDF = view('pdf.EventsPerMonth', ["user" => $reportData, "begin" => $begin, "end" => $end]);
                $pdf = PDF::loadHTML($viewPDF);
                return $pdf->stream();
           } catch(Exception $e){
               $returnData = array (
                   'status' => 500,
                   
                   'message' => $e->getMessage()
               );
               return Response::json($returnData, 500);
           }
        }
    }

    public function eventsByYear(Request $request) {
        $year = $request->get('year');
        $events = collect();
        for ($month=1; $month < 13; $month++) { 
            $countEvents = DB::table('events')->select('id')->whereRaw('YEAR(created_at) = ? AND MONTH(created_at) = ? ', [$year, $month])->count();
            $events->push($countEvents);
        }
        $assistans = collect();
        for ($month=1; $month < 13; $month++) { 
            $countAssistans = DB::table('user_events')->select('id')->whereRaw('YEAR(created_at) = ? AND MONTH(created_at) = ? ', [$year, $month])->count();
            $assistans->push($countAssistans);
        }

        $returnData = array (
            'events' => $events,
            'assistans' => $assistans
        );

        return Response::json($returnData, 200);
    }
    
    public function assistantsVsInterests(Request $request) {
        $year = $request->get('year');
        $countAssistans = DB::table('user_events')->select('id')->whereRaw('YEAR(created_at) = ? AND state = 0 ', [$year])->count();
        $countInterests = DB::table('user_events')->select('id')->whereRaw('YEAR(created_at) = ? AND state = 1 ', [$year])->count();
        
        $returnData = array (
            'interests' => $countInterests,
            'assistans' => $countAssistans
        );

        return Response::json($returnData, 200);
    }

    public function advertisingsActive(Request $request) {
        $year = $request->get('year');
        $countAdvertissings = DB::table('advertising')->select('id')->whereRaw('YEAR(created_at) = ? AND state = 1 ', [$year])->count();
        
        $returnData = array (
            'advertisings' => $countAdvertissings
        );

        return Response::json($returnData, 200);
    }

    public function totalUsers() {
        $countUsers = DB::table('users')->select('id')->where('admin', '=', '0')->count();
        
        $returnData = array (
            'users' => $countUsers
        );

        return Response::json($returnData, 200);
    }
}
