<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tips3 extends Model
{
    protected $table = 'tips3';

    public function products() {
        return $this->hasOne('App\Products', 'id', 'product');
    }

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function events() {
        return $this->hasOne('App\Events', 'id', 'event');
    }
}
