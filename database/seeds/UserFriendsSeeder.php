<?php

use Illuminate\Database\Seeder;

class UserFriendsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 17,
            "user_receipt"      => 2,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 2,
            "user_receipt"      => 7,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 2,
            "user_receipt"      => 16,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 22,
            "user_receipt"      => 19,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 18,
            "user_receipt"      => 17,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 10,
            "user_receipt"      => 22,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 1,
            "user_receipt"      => 14,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 2,
            "user_receipt"      => 12,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 21,
            "user_receipt"      => 20,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 6,
            "user_receipt"      => 5,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 5,
            "user_receipt"      => 3,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 18,
            "user_receipt"      => 14,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 21,
            "user_receipt"      => 13,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 12,
            "user_receipt"      => 17,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 14,
            "user_receipt"      => 16,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 1,
            "user_receipt"      => 13,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 17,
            "user_receipt"      => 7,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 9,
            "user_receipt"      => 15,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 18,
            "user_receipt"      => 22,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 12,
            "user_receipt"      => 22,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 19,
            "user_receipt"      => 10,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 7,
            "user_receipt"      => 4,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 3,
            "user_receipt"      => 18,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 14,
            "user_receipt"      => 5,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 19,
            "user_receipt"      => 20,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 21,
            "user_receipt"      => 8,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 17,
            "user_receipt"      => 9,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 15,
            "user_receipt"      => 19,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 5,
            "user_receipt"      => 11,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 3,
            "user_receipt"      => 8,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 2,
            "user_receipt"      => 9,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 10,
            "user_receipt"      => 11,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 2,
            "user_receipt"      => 15,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 2,
            "user_receipt"      => 19,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 12,
            "user_receipt"      => 10,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 21,
            "user_receipt"      => 4,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 1,
            "user_receipt"      => 4,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 9,
            "user_receipt"      => 16,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 3,
            "user_receipt"      => 9,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 1,
            "user_receipt"      => 17,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 20,
            "user_receipt"      => 22,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 13,
            "user_receipt"      => 12,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 19,
            "user_receipt"      => 5,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 21,
            "user_receipt"      => 9,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 6,
            "user_receipt"      => 21,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 12,
            "user_receipt"      => 3,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 10,
            "user_receipt"      => 3,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 6,
            "user_receipt"      => 2,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 11,
            "user_receipt"      => 15,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 15,
            "user_receipt"      => 18,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 16,
            "user_receipt"      => 17,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 9,
            "user_receipt"      => 18,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 4,
            "user_receipt"      => 19,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 8,
            "user_receipt"      => 11,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 11,
            "user_receipt"      => 13,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 15,
            "user_receipt"      => 5,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 12,
            "user_receipt"      => 14,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 5,
            "user_receipt"      => 21,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 9,
            "user_receipt"      => 4,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 2,
            "user_receipt"      => 5,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 11,
            "user_receipt"      => 16,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 3,
            "user_receipt"      => 11,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 20,
            "user_receipt"      => 18,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 2,
            "user_receipt"      => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 11,
            "user_receipt"      => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 20,
            "user_receipt"      => 9,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 22,
            "user_receipt"      => 21,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 9,
            "user_receipt"      => 11,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 7,
            "user_receipt"      => 22,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 6,
            "user_receipt"      => 11,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 6,
            "user_receipt"      => 20,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 9,
            "user_receipt"      => 19,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 7,
            "user_receipt"      => 6,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 5,
            "user_receipt"      => 18,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 12,
            "user_receipt"      => 15,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 13,
            "user_receipt"      => 2,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 17,
            "user_receipt"      => 15,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 14,
            "user_receipt"      => 21,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 0,
            "user_send"         => 8,
            "user_receipt"      => 6,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        DB::table('friends')->insert([
            "state"             => 1,
            "user_send"         => 2,
            "user_receipt"      => 11,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
    }
}
