<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citas extends Model
{
    protected $table = 'citas';
    
    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }
    
    public function clients() {
        return $this->hasOne('App\Users', 'id', 'client');
    }
    
    public function products() {
        return $this->hasOne('App\Products', 'id', 'product');
    }

    
}
