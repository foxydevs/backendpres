<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PausaActivaComments extends Model
{
    protected $table = 'pausa_activa_comments';

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function comments() {
        return $this->hasMany('App\RetosComments', 'reto');
    }
}
