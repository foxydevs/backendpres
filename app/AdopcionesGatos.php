<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdopcionesGatos extends Model
{
    protected $table = 'adopciones';
    
    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }
}
