<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friends', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('state')->nullable()->default(0);

            $table->integer('user_send')->nullable()->default(null)->unsigned();
            $table->foreign('user_send')->references('id')->on('users')->onDelete('cascade');

            $table->integer('user_receipt')->nullable()->default(null)->unsigned();
            $table->foreign('user_receipt')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('friends');
    }
}
