<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorys extends Model
{
    protected $table = 'categorys';
    
    public function subcategorys() {
        return $this->hasMany('App\Categorys', 'parent', 'id')->with('subcategorys');
    }
}
