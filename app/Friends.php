<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friends extends Model
{
    protected $table = 'friends';

    public function send() {
        return $this->hasOne('App\Users', 'id', 'user_send');
    }

    public function receipt() {
        return $this->hasOne('App\Users', 'id', 'user_receipt');
    }
}
