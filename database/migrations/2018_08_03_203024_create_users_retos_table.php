<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersRetosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_retos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable()->default(null);
            $table->integer('calificacion')->default(0);
            $table->integer('puesto')->default(0);
            $table->integer('state')->default(1);
            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');

            $table->integer('reto')->nullable()->default(null)->unsigned();
            $table->foreign('reto')->references('id')->on('retos')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_retos');
    }
}
