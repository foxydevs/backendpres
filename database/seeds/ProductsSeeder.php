<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            "id"                => 1,
            "name"              => "ABC",
            "description"       => "Aplicacion ERP encargada para el control de compras y ventas con estadisticas incluidas",
            "price"             => 160,
            "quantity"          => 1,
            "cost"              => 0,
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/products/L7R9vCo6TyPNMBxQ1dafDEvKQvNznGJvLXImoDs5.jpeg",
            "state"             => 1,
            "user_created"      => 2,
            "category"          => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('products')->insert([
            "id"                => 2,
            "name"              => "ME",
            "description"       => "Aplicación para presentación de profesionales permite mostrar los productos y servicios de cada uno y mantener un registro de sus clientes",
            "price"             => 17,
            "quantity"          => 0,
            "cost"              => 0,
            "picture"           => "http://resinfer.com/wp-content/uploads/2015/09/productos.png",
            "state"             => 1,
            "user_created"      => 3,
            "category"          => 2,
            "created_at"        => "2018-03-13 15:03:35",
            "updated_at"        => "2018-03-13 15:03:35"
        ]);

        DB::table('products')->insert([
            "id"                => 3,
            "name"              => "Casa familiar de 3 cuartos",
            "description"       => "3 cuartos, 2 baños, sala, cocina y comedor",
            "price"             => 2000,
            "quantity"          => 0,
            "cost"              => 0,
            "picture"           => "http://resinfer.com/wp-content/uploads/2015/09/productos.png",
            "state"             => 1,
            "user_created"      => 9,
            "category"          => 5,
            "created_at"        => "2018-04-19 13:33:05",
            "updated_at"        => "2018-04-19 13:33:05"
        ]);

        DB::table('products')->insert([
            "id"                => 4,
            "name"              => "Pagina Web",
            "description"       => "Elaboracion de pagina web",
            "price"             => 104.12,
            "quantity"          => 0,
            "cost"              => 0,
            "picture"           => "http://resinfer.com/wp-content/uploads/2015/09/productos.png",
            "state"             => 1,
            "user_created"      => 2,
            "category"          => 1,
            "created_at"        => "2018-04-19 15:38:26",
            "updated_at"        => "2018-04-19 15:38:26"
        ]);

        DB::table('products')->insert([
            "id"                => 5,
            "name"              => "Soporte por mantenimiento",
            "description"       => "Soporte para paginas y softwares",
            "price"             => 23.79,
            "quantity"          => 0,
            "cost"              => 0,
            "picture"           => "http://resinfer.com/wp-content/uploads/2015/09/productos.png",
            "state"             => 1,
            "user_created"      => 2,
            "category"          => 1,
            "created_at"        => "2018-04-19 15:39:01",
            "updated_at"        => "2018-04-19 15:39:01"
        ]);

        DB::table('products')->insert([
            "id"                => 6,
            "name"              => "Aplicacion Mobile",
            "description"       => "Diferentes tipos de aplicaciones moviles",
            "price"             => 99.91,
            "quantity"          => 0,
            "cost"              => 0,
            "picture"           => "http://resinfer.com/wp-content/uploads/2015/09/productos.png",
            "state"             => 1,
            "user_created"      => 2,
            "category"          => 1,
            "created_at"        => "2018-04-19 15:39:26",
            "updated_at"        => "2018-04-19 15:39:26"
        ]);

        DB::table('products')->insert([
            "id"                => 18,
            "name"              => "Cadena de plata",
            "description"       => "48 gramos de plata pura 925",
            "price"             => 1500,
            "quantity"          => 0,
            "cost"              => 0,
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/products/6YQpQXI2cOcAUwFbTfo1oKp8Ze4UWO1W9VOiwhHZ.jpeg",
            "state"             => 1,
            "user_created"      => 19,
            "category"          => 42,
            "created_at"        => "2018-06-05 00:03:16",
            "updated_at"        => "2018-06-05 00:05:57"
        ]);

        DB::table('products')->insert([
            "id"                => 19,
            "name"              => "Cadena de plata",
            "description"       => "48 gramos de plata pura 925",
            "price"             => 1500,
            "quantity"          => 0,
            "cost"              => 0,
            "picture"           => "http://resinfer.com/wp-content/uploads/2015/09/productos.png",
            "state"             => 1,
            "user_created"      => 19,
            "category"          => 42,
            "created_at"        => "2018-06-05 00:03:19",
            "updated_at"        => "2018-06-05 00:03:19"
        ]);

        DB::table('products')->insert([
            "id"                => 24,
            "name"              => "Bocina Avanti",
            "description"       => "Activa de 15', 500 watts, Bluetooth, Usb, cajon de plástico.",
            "price"             => 1800,
            "quantity"          => null,
            "cost"              => 50,
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/products/lidukuM4OlaUn3jUzVI401JBnWXPLDeDVJFANOzr.jpeg",
            "state"             => 1,
            "user_created"      => 24,
            "category"          => 65,
            "created_at"        => "2018-06-20 16:08:59",
            "updated_at"        => "2018-07-09 18:21:12"
        ]);

        DB::table('products')->insert([
            "id"                => 28,
            "name"              => "Café huehuetenango",
            "description"       => "Cafe de alta calidad",
            "price"             => 45,
            "quantity"          => 10,
            "cost"              => 12,
            "picture"           => "http://resinfer.com/wp-content/uploads/2015/09/productos.png",
            "state"             => 1,
            "user_created"      => 25,
            "category"          => 80,
            "created_at"        => "2018-06-22 10:05:43",
            "updated_at"        => "2018-06-22 10:12:02"
        ]);

        DB::table('products')->insert([
            "id"                => 31,
            "name"              => "Me",
            "description"       => "Aplicacion para profesionales y empresas",
            "price"             => 0.99,
            "quantity"          => 0,
            "cost"              => 0,
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/products/M7Xjg9Ih4fCEhK5SQ2y2r3Go70pv9YJ2jgx7CcQt.png",
            "state"             => 1,
            "user_created"      => 22,
            "category"          => 55,
            "created_at"        => "2018-06-27 11:06:07",
            "updated_at"        => "2018-06-27 11:06:52"
        ]);

        DB::table('products')->insert([
            "id"                => 32,
            "name"              => "Bocina KSCS15a",
            "description"       => "bocina de 15', 500 watts, cajon de madera.",
            "price"             => 5500,
            "quantity"          => 0,
            "cost"              => 0,
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/products/MyT6ah7tzHFgjQKkwe7X1IX3h3b9kX3kzJH255p4.jpeg",
            "state"             => 1,
            "user_created"      => 24,
            "category"          => 65,
            "created_at"        => "2018-07-09 17:13:44",
            "updated_at"        => "2018-07-09 17:17:14"
        ]);

        DB::table('products')->insert([
            "id"                => 33,
            "name"              => "Cable Pro Lok",
            "description"       => "Xlr a xlr de 7 metros de largo.",
            "price"             => 100,
            "quantity"          => 0,
            "cost"              => 0,
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/products/aaBhtZGeSo0rPbu559V5I5ze8engLWd5YJxaLTbA.jpeg",
            "state"             => 1,
            "user_created"      => 24,
            "category"          => 83,
            "created_at"        => "2018-07-09 17:23:15",
            "updated_at"        => "2018-07-09 17:23:59"
        ]);

        DB::table('products')->insert([
            "id"                => 34,
            "name"              => "Cable para instrumento",
            "description"       => "7 metros de largo.",
            "price"             => 85,
            "quantity"          => 0,
            "cost"              => 0,
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/products/aVo8f946JHY484bRndxmWJLHe3EedmZj4nqXBBqG.jpeg",
            "state"             => 1,
            "user_created"      => 24,
            "category"          => 83,
            "created_at"        => "2018-07-09 17:28:37",
            "updated_at"        => "2018-07-09 17:28:50"
        ]);

        DB::table('products')->insert([
            "id"                => 35,
            "name"              => "AKG",
            "description"       => "Inalámbricos, 50 metros de alcance, señal UHF, incluye baterías.",
            "price"             => 2000,
            "quantity"          => 0,
            "cost"              => 0,
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/products/lrdbU4oiBjNRcBmeqOMoJeugXPIXxm3Iw7XmeuJG.jpeg",
            "state"             => 1,
            "user_created"      => 24,
            "category"          => 84,
            "created_at"        => "2018-07-09 17:32:45",
            "updated_at"        => "2018-07-09 17:33:16"
        ]);

        DB::table('products')->insert([
            "id"                => 36,
            "name"              => "Guitarra palmer",
            "description"       => "Guitarra acústica, Americana, colores negro, azul y corinto.",
            "price"             => 500,
            "quantity"          => 0,
            "cost"              => 0,
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/products/jsNRdM4CrsLVQ9Cq3R212teEU0TpT6plq7y5A4Kd.jpeg",
            "state"             => 1,
            "user_created"      => 24,
            "category"          => 85,
            "created_at"        => "2018-07-09 18:07:12",
            "updated_at"        => "2018-07-09 18:07:44"
        ]);

        DB::table('products')->insert([
            "id"                => 37,
            "name"              => "Pedestal p/ partituras",
            "description"       => "Incluye estuche",
            "price"             => 100,
            "quantity"          => 0,
            "cost"              => 40,
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/products/js05FXwbab35qIe3O29v2ZdCnC9DIT1cp2THPXrg.jpeg",
            "state"             => 1,
            "user_created"      => 24,
            "category"          => 83,
            "created_at"        => "2018-07-09 23:22:03",
            "updated_at"        => "2018-07-09 23:22:48"
        ]);


    }
}
