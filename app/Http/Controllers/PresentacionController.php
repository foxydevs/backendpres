<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Presentacion;
use App\PicturesProducts;
use Response;
use Validator;
use Storage;
use DB;

class PresentacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Presentacion::get(), 200);
    }
    
    public function getThisByUser($id)
    {
        $objectSee = Presentacion::where('app','=',$id)->with('apps','clients','products','presentaciones')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = Presentacion::where('client','=',$id)->with('apps','clients','products','presentaciones')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'          => 'required',
            'description'          => 'required',
            'user_created'          => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Presentacion();
                $newObject->name            = $request->get('name');
                $newObject->description     = $request->get('description');
                $newObject->user_created    = $request->get('user_created');
                $newObject->price           = $request->get('price');
                $newObject->priceEspecial        = $request->get('priceEspecial');
                $newObject->priceFinal        = $request->get('priceFinal');
                $newObject->tiempo        = $request->get('tiempo');
                $newObject->periodo        = $request->get('periodo');
                $newObject->membresia        = $request->get('membresia');
                $newObject->nivel        = $request->get('nivel');
                $newObject->tipo        = $request->get('tipo');
                $newObject->opcion1        = $request->get('opcion1');
                $newObject->pos        = $request->get('pos');
                $newObject->link        = $request->get('link',null);
                $newObject->archivo        = $request->get('archivo',null);
                $newObject->duracion        = $request->get('duracion',null);
                $newObject->app        = $request->get('app',null);
                $newObject->save();
                return Response::json($newObject, 200);
            
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Presentacion::whereRaw('id=?',$id)->first();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getProductsByUser($id)
    {
        $objectSee = Presentacion::where('user_created','=',$id)->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Presentacion::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->name            = $request->get('name', $objectUpdate->name);
                $objectUpdate->description     = $request->get('description', $objectUpdate->description);
                $objectUpdate->user_created    = $request->get('user_created', $objectUpdate->user_created);
                $objectUpdate->quantity        = $request->get('quantity', $objectUpdate->quantity);
                $objectUpdate->price           = $request->get('price', $objectUpdate->price);
                $objectUpdate->cost            = $request->get('cost', $objectUpdate->cost);
                $objectUpdate->state           = $request->get('state', $objectUpdate->state);
                $objectUpdate->priceEspecial           = $request->get('priceEspecial', $objectUpdate->priceEspecial);
                $objectUpdate->priceFinal           = $request->get('priceFinal', $objectUpdate->priceFinal);
                $objectUpdate->tiempo           = $request->get('tiempo', $objectUpdate->tiempo);
                $objectUpdate->periodo           = $request->get('periodo', $objectUpdate->periodo);
                $objectUpdate->membresia           = $request->get('membresia', $objectUpdate->membresia);
                $objectUpdate->nivel           = $request->get('nivel', $objectUpdate->nivel);
                $objectUpdate->tipo           = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->opcion1           = $request->get('opcion1', $objectUpdate->opcion1);
                $objectUpdate->pos           = $request->get('pos', $objectUpdate->pos);
                $objectUpdate->state           = $request->get('state', $objectUpdate->state);
                $objectUpdate->link           = $request->get('link', $objectUpdate->link);
                $objectUpdate->archivo           = $request->get('archivo', $objectUpdate->archivo);
                $objectUpdate->duracion           = $request->get('duracion', $objectUpdate->duracion);
                $objectUpdate->periodo_duracion           = $request->get('periodo_duracion', $objectUpdate->periodo_duracion);
                $objectUpdate->app           = $request->get('app', $objectUpdate->app);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    public function uploadAvatar(Request $request, $id) {
        $objectUpdate = Presentacion::find($id);
        if ($objectUpdate) {

            $validator = Validator::make($request->all(), [
                'avatar'      => 'required|image|mimes:jpeg,png,jpg'
            ]);

            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $path = Storage::disk('s3')->put('Presentacion', $request->avatar);

                    $objectUpdate->picture = Storage::disk('s3')->url($path);
                    $objectUpdate->save();

                    return Response::json($objectUpdate, 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }

            }

            return Response::json($objectUpdate, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    public function uploadAvatars(Request $request, $id) {
            $validator = Validator::make($request->all(), [
                'avatar'      => 'required|image|mimes:jpeg,png,jpg'
            ]);

            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $path = Storage::disk('s3')->put('Presentacion', $request->avatar);
                    if($request->get('idPic')){
                        $objectUpdate = PicturesProducts::find($request->get('idPic'));
                        if ($objectUpdate) {
                            try {
                                $objectUpdate->picture = Storage::disk('s3')->url($path);
                                $objectUpdate->save();
                                return Response::json($objectUpdate, 200);
                            } catch (Exception $e) {
                                $returnData = array (
                                    'status' => 500,
                                    'message' => $e->getMessage()
                                );
                                return Response::json($returnData, 500);
                            }
                        }
                        else {
                            $returnData = array (
                                'status' => 404,
                                'message' => 'No record found'
                            );
                            return Response::json($returnData, 404);
                        }
                    }else{
                        $newObject = new PicturesProducts();
                        $newObject->user           = $request->get('user');
                        $newObject->product        = $id;
                        $newObject->picture = Storage::disk('s3')->url($path);
                        $newObject->save();
    
                        return Response::json($newObject, 200);
                    }
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }

            }

            return Response::json($newObject, 200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Presentacion::find($id);
        if ($objectDelete) {
            try {
                Presentacion::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}