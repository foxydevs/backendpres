<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDireccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direcciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('direccion')->nullable()->default(null);
            $table->string('ciudad')->nullable()->default(null);
            $table->string('calle')->nullable()->default(null);
            $table->string('numero')->nullable()->default(null);
            $table->string('observacion')->nullable()->default(null);
            $table->integer('tipo')->default(1);
            $table->integer('state')->default(1);

            $table->integer('app')->nullable()->default(null)->unsigned();
            $table->foreign('app')->references('id')->on('users')->onDelete('cascade');

            $table->integer('client')->nullable()->default(null)->unsigned();
            $table->foreign('client')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direcciones');
    }
}
