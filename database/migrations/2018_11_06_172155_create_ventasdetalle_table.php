<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasdetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventasdetalle', function (Blueprint $table) {
            $table->increments('id');
            $table->double('subtotal',15,2)->nullable()->default(null);
            $table->double('cantidad',15,2)->nullable()->default(null);
            $table->double('precio',17,2)->nullable()->default(null);
            $table->double('precioE',17,2)->nullable()->default(null);
            $table->double('precioM',17,2)->nullable()->default(null);
            $table->double('descuento',17,2)->nullable()->default(null);
            $table->date('vencimiento')->nullable()->default(null);
            $table->string('garantia')->nullable()->default(null);
            $table->string('comentario')->nullable()->default(null);
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->integer('venta')->unsigned()->nullable()->default(null);
            $table->foreign('venta')->references('id')->on('ventas')->onDelete('cascade');

            $table->integer('producto')->unsigned()->nullable()->default(null);
            $table->foreign('producto')->references('id')->on('products')->onDelete('cascade');
            
            $table->integer('presentacion')->unsigned()->nullable()->default(null);
            $table->foreign('presentacion')->references('id')->on('presentacion_producto')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventasdetalle');
    }
}
