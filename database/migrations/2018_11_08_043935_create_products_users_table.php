<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable()->default(null);
            $table->integer('calificacion')->default(0);
            $table->integer('tipo')->default(0);
            $table->integer('state')->default(1);

            $table->integer('app')->nullable()->default(null)->unsigned();
            $table->foreign('app')->references('id')->on('users')->onDelete('cascade');

            $table->integer('client')->nullable()->default(null)->unsigned();
            $table->foreign('client')->references('id')->on('users')->onDelete('cascade');

            $table->integer('product')->nullable()->default(null)->unsigned();
            $table->foreign('product')->references('id')->on('products')->onDelete('cascade');

            $table->integer('documento')->nullable()->default(null)->unsigned();
            $table->foreign('documento')->references('id')->on('documentos')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_users');
    }
}
