<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MotivacionComments extends Model
{
    protected $table = 'motivación_comments';
}
