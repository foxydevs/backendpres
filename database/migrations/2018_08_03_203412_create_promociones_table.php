<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromocionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promociones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable()->default(null);
            $table->string('place')->nullable()->default(null);
            $table->string('address')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->date('inicio')->nullable()->default(null);
            $table->date('fin')->nullable()->default(null);
            $table->double('descuento')->nullable()->default(0);
            $table->string('description')->nullable()->default(null);
            $table->double('latitude',15,8)->nullable()->default(null);
            $table->double('longitude',15,8)->nullable()->default(null);
            $table->integer('state')->default(1);

            $table->integer('user_created')->nullable()->default(null)->unsigned();
            $table->foreign('user_created')->references('id')->on('users')->onDelete('cascade');

            $table->integer('product')->nullable()->default(null)->unsigned();
            $table->foreign('product')->references('id')->on('products')->onDelete('cascade');
            $table->integer('event')->nullable()->default(null)->unsigned();
            $table->foreign('event')->references('id')->on('events')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promociones');
    }
}
