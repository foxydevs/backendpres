<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Sliders;
use Response;
use Validator;
class SlidersController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(Sliders::whereRaw('activo = 1')->get(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Sliders::whereRaw('app=? and state=?',[$id,$state])->with('apps','clients')->get();
                    break;
                }
                case 'activo':{
                    $objectSee = Sliders::whereRaw('app=? and activo=?',[$id,$state])->with('apps','clients')->get();
                    break;
                }
                case 'fechaInicio':{
                    $objectSee = Sliders::whereRaw('app=? and fechaInicio<=?',[$id,$state])->with('apps','clients')->get();
                    break;
                }
                case 'fechaFin':{
                    $objectSee = Sliders::whereRaw('app=? and fechaFin>=?',[$id,$state])->with('apps','clients')->get();
                    break;
                }
                default:{
                    $objectSee = Sliders::whereRaw('app=? and activo=?',[$id,$state])->with('apps','clients')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Sliders::whereRaw('activo=?',[$state])->with('apps','clients')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'app'          => 'required',
            'url'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Sliders();
                $newObject->titulo            = $request->get('titulo');
                $newObject->descripcion            = $request->get('descripcion');
                $newObject->url            = $request->get('url');
                $newObject->fechaInicio            = $request->get('fechaInicio');
                $newObject->fechaFin            = $request->get('fechaFin');
                $newObject->activo            = $request->get('activo');
                $newObject->state            = $request->get('state');
                $newObject->app            = $request->get('app');
                $newObject->client            = $request->get('client');
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = Sliders::find($id);
        if ($objectSee) {
            $objectSee->apps;
            $objectSee->clients;
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = Sliders::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->titulo = $request->get('titulo', $objectUpdate->titulo);
                $objectUpdate->descripcion = $request->get('descripcion', $objectUpdate->descripcion);
                $objectUpdate->url = $request->get('url', $objectUpdate->url);
                $objectUpdate->fechaInicio = $request->get('fechaInicio', $objectUpdate->fechaInicio);
                $objectUpdate->fechaFin = $request->get('fechaFin', $objectUpdate->fechaFin);
                $objectUpdate->activo = $request->get('activo', $objectUpdate->activo);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->app = $request->get('app', $objectUpdate->app);
                $objectUpdate->client = $request->get('client', $objectUpdate->client);
    
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = Sliders::find($id);
        if ($objectDelete) {
            try {
                Sliders::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
