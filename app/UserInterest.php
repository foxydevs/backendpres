<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInterest extends Model
{
    protected $table = 'interest';

    public function userInterest(){
        return $this->hasOne('App\Users', 'id', 'user');
    }
}
