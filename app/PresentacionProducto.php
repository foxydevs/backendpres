<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PresentacionProducto extends Model
{
    protected $table = 'presentacion_producto';

    public function clients() {
        return $this->hasOne('App\Users', 'id', 'client');
    }

    public function apps() {
        return $this->hasOne('App\Users', 'id', 'app');
    }

    public function presentaciones() {
        return $this->hasOne('App\Presentacion', 'id', 'presentacion');
    }

    public function products() {
        return $this->hasOne('App\Products', 'id', 'product');
    }
}
