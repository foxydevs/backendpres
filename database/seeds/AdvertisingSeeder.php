<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class AdvertisingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        DB::table('advertising')->insert([
            'place'             => 'Tillman',
            'state'             => 1,
            'client'            => 'Tillman',
            'picture'           => 'http://d2ydh70d4b5xgv.cloudfront.net/images/a/5/john-tillman-co-25bxl-4-leather-cuff-split-deerskin-kevlar-sewn-tig-gloves-fd3629efa3b9f8d5ac348ee06f1d80f5.jpg',
            'description'       => 'The best corporation in the west cost.',
            'begin'             => "2017-05-01 08:00:00",
            'end'               => "2017-12-01 08:00:00",
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
        DB::table('advertising')->insert([
            'place'             => 'Kreiger',
            'state'             => 1,
            'client'            => "Kreiger",
            'picture'           => 'https://rose-krieger.co.uk/wp-content/uploads/profile_technology_image.jpg',
            'description'       => 'Wearning the word',
            'begin'             => "2017-05-01 08:00:00",
            'end'               => "2017-12-01 08:00:00",
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
        DB::table('advertising')->insert([
            'place'             => 'Hilpert Group',
            'state'             => 1,
            'client'            => "Hilpert Group",
            'picture'           => 'https://pbs.twimg.com/media/CucjIjvXEAAwFsR.jpg',
            'description'       => 'Helper the word',
            'begin'             => "2017-05-01 08:00:00",
            'end'               => "2017-12-01 08:00:00",
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
        DB::table('advertising')->insert([
            'place'             => 'Crooks, Lubowitz and Bruen',
            'state'             => 1,
            'client'            => "Crooks, Lubowitz and Bruen",
            'picture'           => 'https://getgil.com/wp-content/uploads/2016/08/M.O.A.B._2017_029-300x200.jpg',
            'description'       => 'You and us can be doing',
            'begin'             => "2017-05-01 08:00:00",
            'end'               => "2017-12-01 08:00:00",
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
        DB::table('advertising')->insert([
            'place'             => 'Beatty-Kautzer',
            'state'             => 1,
            'client'            => "Beatty-Kautzer",
            'picture'           => 'https://cdn.slidesharecdn.com/ss_thumbnails/galaxytradefacilitators-111110005717-phpapp01-thumbnail-3.jpg',
            'description'       => 'Be beatty',
            'begin'             => "2017-05-01 08:00:00",
            'end'               => "2017-12-01 08:00:00",
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
        DB::table('advertising')->insert([
            'place'             => 'Keeling-Bauch',
            'state'             => 1,
            'client'            => "Keeling-Bauch",
            'picture'           => 'https://ae01.alicdn.com/kf/HTB1rOB4QXXXXXcFXpXXq6xXFXXX1/Athletic-Girdle-Postpartum-Recovery-Shapewear-font-b-Support-b-font-Corset-Prenatal-Care-Women-Maternity-font.jpg',
            'description'       => 'Eat something enjoy everything',
            'begin'             => "2017-05-01 08:00:00",
            'end'               => "2017-12-01 08:00:00",
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
        DB::table('advertising')->insert([
            'place'             => 'Rolfson, Homenick and Cassin',
            'state'             => 1,
            'client'            => "Rolfson, Homenick and Cassin",
            'picture'           => 'http://i.ytimg.com/vi/8oA8FhHulSY/hqdefault.jpg',
            'description'       => 'You are a Winner',
            'begin'             => "2017-05-01 08:00:00",
            'end'               => "2017-12-01 08:00:00",
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
        DB::table('advertising')->insert([
            'place'             => 'Denesik PLC',
            'state'             => 1,
            'client'            => "Denesik PLC",
            'picture'           => 'http://i.imgur.com/ya9nZ6U.jpg',
            'description'       => 'The best of the best',
            'begin'             => "2017-05-01 08:00:00",
            'end'               => "2017-12-01 08:00:00",
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
        DB::table('advertising')->insert([
            'place'             => 'Okuneva and Sons',
            'state'             => 1,
            'client'            => "Okuneva and Sons",
            'picture'           => 'http://broadwaydancemagazine.com/wp-content/uploads/2016/11/78fc1864a50448478e4ab6bd4ac58b6a-3.jpg',
            'description'       => 'In Family is better',
            'begin'             => "2017-05-01 08:00:00",
            'end'               => "2017-12-01 08:00:00",
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
        DB::table('advertising')->insert([
            'place'             => 'Gottlieb, Schaefer and Shanahan',
            'state'             => 1,
            'client'            => "Gottlieb, Schaefer and Shanahan",
            'picture'           => 'http://kwaas.org/healthstudio/pluginfile.php/383/mod_glossary/entry/145/650B5426-1018-4B65-A54B58D4E91BFBB7.jpg',
            'description'       => 'Because the world can be better',
            'begin'             => "2017-05-01 08:00:00",
            'end'               => "2017-12-01 08:00:00",
            'created_at'        => date('Y-m-d H:m:s'),
            'updated_at'        => date('Y-m-d H:m:s')
        ]);
        
    }
}
