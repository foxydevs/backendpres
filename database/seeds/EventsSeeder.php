<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class EventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([
            "id"                => 13,
            "date"              => "2018-06-09",
            "time"              => "01:00:00",
            "place_id"          => "0",
            "place"             => "Foxylabs",
            "address"           => "3ra avenida 20-06 zona 2",
            "picture"           => "http://www.powerlight.es/img/p/es-default-large.jpg",
            "description"       => "Fiesta de celebracion",
            "latitude"          => 14.66462473,
            "longitude"         => -90.51446576,
            "user_created"      => 22,
            "created_at"        => "2018-06-07 21:40:54",
            "updated_at"        => "2018-06-27 11:08:42"
        ]);

        DB::table('user_events')->insert([
            "id"                => 1,
            "state"             => 0,
            "event"             => 13,
            "user"              => 7,
            "created_at"        => "2018-05-15 17:14:14",
            "updated_at"        => "2018-05-24 13:55:17"
        ]);

    }
}
