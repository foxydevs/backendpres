<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoBeneficios extends Model
{
    protected $table = 'tipo_beneficios';

    public function beneficios() {
        return $this->hasMany('App\Beneficios', 'tipo', 'id');
    }
    
    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function apps() {
        return $this->hasOne('App\Users', 'id', 'app');
    }

}
