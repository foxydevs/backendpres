<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Descuentos;
use Response;
use Validator;
class DescuentosController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(Descuentos::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Descuentos::whereRaw('app=? and state=?',[$id,$state])->with('apps','clientes')->get();
                    break;
                }
                case 'type':{
                    $objectSee = Descuentos::whereRaw('app=? and tipo=?',[$id,$state])->with('apps','clientes')->get();
                    break;
                }
                case 'codigo':{
                    $objectSee = Descuentos::whereRaw('app=? and codigo=?',[$id,$state])->with('apps','clientes')->get();
                    break;
                }
                default:{
                    $objectSee = Descuentos::whereRaw('app=? and state=?',[$id,$state])->with('apps','clientes')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Descuentos::whereRaw('app=? and state=?',[$id,$state])->with('apps','clientes')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = Descuentos::where('app','=',$id)->with('apps','clientes')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = Descuentos::where('app','=',$id)->with('apps','clientes')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cantidad'          => 'required',
            'app'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Descuentos();
                $newObject->codigo            = $request->get('codigo');
                $newObject->cantidad            = $request->get('cantidad');
                $newObject->picture            = $request->get('picture');
                $newObject->description            = $request->get('description');
                $newObject->latitude            = $request->get('latitude');
                $newObject->longitude            = $request->get('longitude');
                $newObject->usado            = $request->get('usado');
                $newObject->tipo            = $request->get('tipo');
                $newObject->state            = $request->get('state');
                $newObject->cliente            = $request->get('cliente');
                $newObject->app            = $request->get('app');
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = Descuentos::find($id);
        if ($objectSee) {
            $objectSee->clientes;
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = Descuentos::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->cantidad = $request->get('cantidad', $objectUpdate->cantidad);
                $objectUpdate->picture = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->codigo = $request->get('codigo', $objectUpdate->codigo);
                $objectUpdate->description = $request->get('description', $objectUpdate->description);
                $objectUpdate->latitude = $request->get('latitude', $objectUpdate->latitude);
                $objectUpdate->longitude = $request->get('longitude', $objectUpdate->longitude);
                $objectUpdate->usado = $request->get('usado', $objectUpdate->usado);
                $objectUpdate->tipo = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->cliente = $request->get('cliente', $objectUpdate->cliente);
                $objectUpdate->app = $request->get('app', $objectUpdate->app);
    
                $objectUpdate->save();
                $objectUpdate->clientes;
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = Descuentos::find($id);
        if ($objectDelete) {
            try {
                Descuentos::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
