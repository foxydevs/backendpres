<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comment')->nullable()->default(null);
            $table->double('unit_price')->nullable()->default(null);
            $table->double('precioInicial')->nullable()->default(null);
            $table->double('quantity')->nullable()->default(null);
            $table->double('total')->nullable()->default(null);
            $table->timestamp('created')->useCurrent();
            $table->date('entrega')->nullable()->default(null);
            $table->string('ern')->nullable()->default(null);
            $table->string('token')->nullable()->default(null);
            $table->string('aprobacion')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->string('fechaapro')->nullable()->default(null);
            $table->string('deposito')->nullable()->default(null);
            $table->string('opcion1')->nullable()->default(null);
            $table->string('opcion2')->nullable()->default(null);
            $table->string('opcion3')->nullable()->default(null);
            $table->string('opcion4')->nullable()->default(null);
            $table->string('opcion5')->nullable()->default(null);
            $table->string('opcion6')->nullable()->default(null);
            $table->string('libro')->nullable()->default(null);
            $table->integer('state')->default(1);

            $table->integer('venta')->nullable()->default(null)->unsigned();
            $table->foreign('venta')->references('id')->on('ventas')->onDelete('cascade');

            $table->integer('client')->nullable()->default(null)->unsigned();
            $table->foreign('client')->references('id')->on('users')->onDelete('cascade');
            
            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');

            $table->integer('product')->nullable()->default(null)->unsigned();
            $table->foreign('product')->references('id')->on('products')->onDelete('cascade');

            $table->integer('presentacion')->nullable()->default(null)->unsigned();
            $table->foreign('presentacion')->references('id')->on('presentacion_producto')->onDelete('cascade');

            $table->integer('direccion')->unsigned()->nullable()->default(null);
            $table->foreign('direccion')->references('id')->on('direcciones')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
