<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProducts extends Model
{
    protected $table = 'users_products';
}
