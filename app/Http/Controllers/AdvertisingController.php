<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Advertising;
use App\Events;
use Response;
use Validator;
use Storage;
use DB;

class AdvertisingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Advertising::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'place'          => 'required',
            'begin'          => 'required',
            'end'          => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Advertising();
                $newObject->place            = $request->get('place');
                $newObject->description      = $request->get('description');
                $newObject->picture          = $request->get('picture');
                $newObject->client           = $request->get('client');
                $newObject->begin            = $request->get('begin');
                $newObject->end              = $request->get('end');
                $newObject->state            = $request->get('state');
                $newObject->save();
                return Response::json($newObject, 200);
            
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
           
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Advertising::find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Advertising::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->place            = $request->get('place', $objectUpdate->place);
                $objectUpdate->description      = $request->get('description', $objectUpdate->description);
                $objectUpdate->picture          = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->client           = $request->get('client', $objectUpdate->client);
                $objectUpdate->begin            = $request->get('begin', $objectUpdate->begin);
                $objectUpdate->end              = $request->get('end', $objectUpdate->end);
                $objectUpdate->state            = $request->get('state', $objectUpdate->state);
                $objectUpdate->save();

                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Advertising::find($id);
        if ($objectDelete) {
            try {
                Advertising::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
