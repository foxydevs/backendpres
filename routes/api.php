<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::resource('accesos', 'AccesosController');
Route::resource('adopcionesperros', 'AdopcionesPerrosController');
Route::resource('adopcionesgatos', 'AdopcionesGatosController');
Route::resource('advertising', 'AdvertisingController');
Route::resource('beneficios', 'BeneficiosController');
Route::resource('blog', 'BlogController');
Route::resource('blogcomments', 'BlogCommentsController');
Route::resource('cerca', 'CercaController');
Route::resource('categorys', 'CategorysController');
Route::resource('citas', 'CitasController');
Route::resource('citasnotas', 'CitasNotasController');
Route::resource('clients', 'UserClientsController');
Route::resource('commentsevents', 'CommentsEventsController');
Route::resource('commentsnews', 'CommentsNewsController');
Route::resource('commentsposts', 'CommentsPostsController');
Route::resource('commentsproducts', 'CommentsProductsController');
Route::resource('commetsusers', 'CommentsUsersController');
Route::resource('comercios', 'ComerciosController');
Route::resource('descuentos', 'DescuentosController');
Route::resource('documentos', 'DocumentosController');
Route::resource('direcciones', 'DireccionesController');
Route::resource('enterprise', 'UserEnterpriseController');
Route::resource('events', 'EventsController');
Route::resource('eventstype', 'EventsTypeController');
Route::resource('friends', 'FriendsController');
Route::resource('horarios', 'HorariosController');
Route::resource('informacioncontacto', 'InformacionContactoController');
Route::resource('interest', 'UserInterestController');
Route::resource('modulos', 'ModulosController');
Route::resource('motivacion', 'MotivacionController');
Route::resource('motivacioncomments', 'MotivacionCommentsController');
Route::resource('news', 'NewsController');
Route::resource('orders', 'OrdersController');
Route::resource('pausaactiva', "PausaActivaController");
Route::resource('periodos', 'PeriodosController');
Route::resource('picturesevents', 'PicturesEventsController');
Route::resource('picturesproducts', 'PicturesProductsController');
Route::resource('posts', 'PostsController');
Route::resource('products', 'ProductsController');
Route::resource('presentacion', 'PresentacionController');
Route::resource('presentacionproducto', 'PresentacionProductoController');
Route::resource('progreso', 'ProgresoController');
Route::resource('promociones', 'PromocionesController');
Route::resource('promocionescomments', 'PromocionesCommentsController');
Route::resource('retos', 'RetosController');
Route::resource('retoscomments', 'RetosCommentsController');
Route::resource('rutinas', 'RutinasController');
Route::resource('sliders', 'SlidersController');
Route::resource('staff', 'StaffController');
Route::resource('social', 'SocialController');
Route::resource('socialcomments', 'SocialCommentsController');
Route::resource('tips2', 'Tips2Controller');
Route::resource('tips3', 'Tips3Controller');
Route::resource('tips4', 'Tips4Controller');
Route::resource('tips5', 'Tips5Controller');
Route::resource('ubicaciones', 'UbicacionesController');
Route::resource('userevents', 'UserEventsController');
Route::resource('userproducts', 'UserProductsController');
Route::resource('userstypes', 'Users_TypesController');
Route::resource('users', 'UsersController');
Route::resource('usersretos', 'UsersRetosController');
Route::resource('workouts', 'WorkoutsController');
Route::resource('workoutscomments', 'WorkoutsCommentsController');

Route::resource('tiposventa', 'TiposVentaController');
Route::resource('ventas', 'VentasController');
Route::resource('ventasdetalle', 'VentasDetalleController');


Route::get('orders/find/{ern}', 'OrdersController@findErnOrders');
Route::get('orders/redireccion/{token}/{ern}', 'OrdersController@decided');

Route::get('users/{friend}/friends/{id}', 'UsersController@showAllUserInformation');
Route::get('users/{id}/friends', "FriendsController@getFriends");
Route::get('users/{id}/cerca', "CercaController@getThisByUser");
Route::get('users/{id}/events', "EventsController@getEventsByUser");
Route::get('users/{id}/categorys', "CategorysController@getCategorysByUser");
Route::get('users/{id}/nofriends', "FriendsController@getNoFriends");
Route::get('users/{id}/products', "ProductsController@getProductsByUser");
Route::get('users/{id}/descuentos', "DescuentosController@getThisByUser");
Route::get('users/{id}/horarios', "HorariosController@getThisByUser");
Route::get('users/{id}/informacioncontacto', "InformacionContactoController@getThisByUser");
Route::get('users/{id}/staff', "StaffController@getThisByUser");
Route::get('user/{id}/staff/{id2}', "StaffController@getThisByStaff");
Route::get('users/{id}/progreso', "ProgresoController@getThisByUser");
Route::get('users/{id}/presentacion', "PresentacionController@getThisByUser");
Route::get('users/{id}/presentacionproducto', "PresentacionProductoController@getThisByUser");
Route::get('users/{id}/presentacionProducto/{estado}', "PresentacionProductoController@getThisByFilter");
Route::get('users/{id}/sliders/{estado}', "SlidersController@getThisByFilter");
Route::get('users/{id}/presentacion/{estado}', "PresentacionController@getThisByFilter");
Route::get('users/{id}/progreso/{estado}', "ProgresoController@getThisByFilter");
Route::get('users/{id}/orders/{estado}', "OrdersController@getThisByFilter");
Route::get('users/{id}/ubicaciones/{estado}', "UbicacionesController@getThisByFilter");
Route::get('filter/{id}/citasnotas/{state}', "CitasNotasController@getThisByFilter");

Route::get('users/{id}/posts', "PostsController@getPostsByUser");
Route::get('users/{id}/documentos', "DocumentosController@getProductsByUser");
Route::get('users/{id}/tips2', "Tips2Controller@getThisByUser");
Route::get('users/{id}/tips3', "Tips3Controller@getThisByUser");
Route::get('users/{id}/tips4', "Tips4Controller@getThisByUser");
Route::get('users/{id}/tips5', "Tips5Controller@getThisByUser");
Route::get('users/{id}/beneficios', "BeneficiosController@getBeneficiosByUser");
Route::get('users/{id}/comercios', "ComerciosController@getComerciosByUser");
Route::get('users/{id}/promociones', "PromocionesController@getPromocionesByUser");
Route::get('users/{id}/workout', "WorkoutsController@getWorkoutByUser");
Route::get('users/{id}/motivacion', "MotivacionController@getMotivacionByUser");
Route::get('users/{id}/blog', "BlogController@getBlogByUser");
Route::get('users/{id}/retos', "RetosController@getRetosByUser");
Route::get('users/{id}/pausaactiva', "PausaActivaController@getThisByUser");
Route::get('users/{id}/social', "SocialController@getThisByUser");

Route::get('users/{id}/periodos', "PeriodosController@getThisByUser");
Route::get('users/{id}/rutinas', "RutinasController@getThisByUser");

Route::get('ubicaciones/{id}/users', 'UbicacionesController@getThisByUser');
Route::get('ubicaciones/{id}/clients', 'UbicacionesController@getThisByClients');
Route::get('citas/{id}/users', 'CitasController@getThisByUserDate');
Route::get('citas/{id}/clients', 'CitasController@getThisByClientsDate');
Route::get('users/{id}/adopcionesperros', 'AdopcionesPerrosController@getThisByUser');
Route::get('users/{id}/print/adopcionesperros', 'AdopcionesPerrosController@printAdopcionesPerros');
Route::get('users/{id}/adopcionesgatos', 'AdopcionesGatosController@getThisByUser');
Route::get('users/{id}/print/adopcionesgatos', 'AdopcionesGatosController@printAdopcionesGatos');
Route::get('clients/{id}/adopcionesperros', 'AdopcionesPerrosController@getThisByClient');
Route::get('clients/{id}/adopcionesgatos', 'AdopcionesGatosController@getThisByClient');
Route::get('clients/{id}/descuentos', 'DescuentosController@getThisByClient');
Route::get('clients/{id}/adopcionesperros/{estado}', 'AdopcionesPerrosController@getThisByStateClients');
Route::get('clients/{id}/adopcionesgatos/{estado}', 'AdopcionesGatosController@getThisByStateClients');
Route::get('users/{id}/citas', 'CitasController@getThisByUser');
Route::get('clients/{id}/citas', 'CitasController@getThisByClient');
Route::get('clients/{id}/workout', "WorkoutsController@getWorkoutByClients");
Route::get('clients/{id}/horarios', "HorariosController@getThisByClients");
Route::get('clients/{id}/informacioncontacto', "InformacionContactoController@getThisByClients");
Route::get('clients/{id}/presentacion', "PresentacionController@getThisByClients");
Route::get('clients/{id}/presentacionproducto', "PresentacionProductoController@getThisByClients");
Route::get('clients/{id}/direcciones', "DireccionesController@getThisByClients");

Route::get('users/{id}/direcciones', "DireccionesController@getThisByUser");
Route::get('users/{id}/orders', "OrdersController@getOrdersByUser");
Route::get('users/{id}/orders/{estado}', "OrdersController@getOrdersByState");
Route::get('users/{id}/adopcionesgatos/{estado}', "AdopcionesGatosController@getThisByState");
Route::get('users/{id}/adopcionesperros/{estado}', "AdopcionesPerrosController@getThisByState");
Route::get('users/{id}/events/{estado}', "EventsController@getThisByState");
Route::get('users/{id}/events/type/{estado}', "EventsController@getThisByType");
Route::get('users/{id}/events/tipo/{estado}', "EventsController@getThisByTipo");
Route::get('users/{id}/users/{estado}', "UsersController@getThisByState");
Route::get('filter/{id}/decuentos/{state}', "DescuentosController@getThisByFilter");
Route::get('users/{id}/products/orders', "OrdersController@getOrdersByUser");
Route::get('users/{id}/receipt', "CommentsUsersController@getMyReceipt");
Route::get('users/{id}/send', "CommentsUsersController@getMySend");

Route::get('clients/{id}/orders', "OrdersController@getOrdersByClients");
Route::get('clients/{id}/progreso/{estado}', "ProgresoController@getThisByClient");

Route::get('clients/{id}/progreso', "ProgresoController@getThisByClients");
Route::get('clients/{id}/periodos', "PeriodosController@getThisByPeriodos");
Route::get('clients/{id}/rutina', "RutinasController@getThisByClients");

Route::get('comments/{id}/news', "CommentsNewsController@getCommentsByNews");
Route::get('comments/{id}/retos', "RetosCommentsController@getCommentsByRetos");
Route::get('comments/{id}/products', "CommentsProductsController@getCommentsByProducts");
Route::get('comments/{id}/events', "CommentsEventsController@getCommentsByEvents");
Route::get('comments/{id}/posts', "CommentsPostsController@getCommentsByPosts");
Route::get('comments/{id}/social', "SocialCommentsController@getCommentsByPosts");

Route::get('find/users/{id}', 'UsersController@getThisByCode');

Route::get('user/only', "UsersController@getUsersOnly");
Route::get('user/only/{id}', "UsersController@getUsersOnlyMine");
Route::get('user/clients/{id}', "UsersController@getClientsOnlyMine");
Route::get('pending/users', "UsersController@getUsersPendingOnly");
Route::get('client/only', "UsersController@getClientsOnly");
Route::get('enterprise/only', "UsersController@getEnterpriseOnly");
Route::get('products/{id}/orders', "OrdersController@getOrdersByProducts");
Route::get('users/{id}/invitations', "FriendsController@getInvitations");
Route::get('users/find/name', 'UsersController@findUserByName');
Route::get('orders/{id}/comprobante', 'OrdersController@comprobanteCompra');
Route::get('interest/filter/{id}', "UserInterestController@interestsByUser");
Route::get('events/filter/friends', 'EventsController@eventsByFriends');
Route::get('commentsusers/{id}/filter/{state}', 'CommentsUsersController@getThisByFilter');
Route::get('near/ubicaciones', 'UbicacionesController@pinNear');
Route::get('near/ubicaciones/{id}', 'UbicacionesController@pinNearMine');
Route::get('near/events', 'EventsController@eventsNear');
Route::get('near/nofriends', 'FriendsController@getNearNoFriends');
Route::get('near/friends', 'FriendsController@getNearFriends');
Route::get('near/people', 'FriendsController@getNearPeople');
Route::get('reports/userspermonth', 'ReportsController@getUsersPerMonth');
Route::get('reports/eventspermonth', 'ReportsController@getEventsPerMonth');
Route::get('reports/event/{id}', 'EventsController@reportEvent');    
Route::get('reports/byyearevent', 'ReportsController@eventsByYear');
Route::get('reports/assistantsvsinterests', 'ReportsController@assistantsVsInterests');
Route::get('reports/advertisingsactive', 'ReportsController@advertisingsActive');
Route::get('reports/totalusers', 'ReportsController@totalusers');

Route::delete('friends/{user}/{friend}', 'FriendsController@removeFriend');

Route::put('users/{id}/deleteavatar', "UsersController@deleteAvatar");

Route::post('users/new/facebook', 'UsersController@storeFcebook');
Route::post('users/password/reset', 'UsersController@recoveryPassword');
Route::post('users/{id}/changepassword', "UsersController@changePassword");
Route::post('orders/{id}/pagar', 'OrdersController@pagar');
Route::post('orders/{id}/pagarqpp', 'OrdersController@pagarQPP');

Route::post('categorys/upload/{id}', 'CategorysController@uploadAvatar');
Route::post('product/upload/{id}', 'ProductsController@uploadAvatar');
Route::post('product/upload/multiple/{id}', 'ProductsController@uploadAvatars');
Route::post('users/upload/{id}', 'UsersController@uploadAvatar');
Route::post('users/upload/pic1/{id}', 'UsersController@uploadAvatarPic1');
Route::post('users/upload/pic2/{id}', 'UsersController@uploadAvatarPic2');
Route::post('users/upload/pic3/{id}', 'UsersController@uploadAvatarPic3');
Route::post('event/upload/{id}', 'EventsController@uploadAvatar');
Route::post('orders/upload/{id}', 'Ordersontroller@uploadAvatar');
Route::post('commentsusers/upload/{id}', 'CommentsUsersController@uploadAvatar');
Route::post('pictures/upload', 'CommentsUsersController@uploadAvatarOnly');
Route::post('orders/upload/{id}', 'OrdersController@uploadAvatar');
Route::post('posts/upload/{id}', 'PostsController@uploadAvatar');


Route::get('users/{id}/modulos', 'AccesosController@getAccesos');
Route::get('user/{id}/modulos', 'AccesosController@getAccesosByUserDefault');
Route::get('user/{id}/all/modulos', 'AccesosController@getAccesosByUser');
Route::get('user/{id}/especial/modulos', 'AccesosController@getAccesosByUserEspecial');
Route::get('users/{id}/modulos/{id2}', 'AccesosController@getAcceso');

Route::get('users/{id}/top', 'RetosCommentsController@getTopUsersByRetos');
Route::get('top/users', 'RetosCommentsController@getTopUsers');

Route::post('users', 'UsersController@store');
Route::post('users/new/create', 'UsersController@store');
Route::post('users/new/facebook', 'UsersController@storeFcebook');
Route::post('login/{id}', 'AuthenticateController@loginID');
Route::post('login', 'AuthenticateController@login');
Route::post('login/admin', 'AuthenticateController@loginAdmin');

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');