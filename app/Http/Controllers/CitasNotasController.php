<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\CitasNotas;
use Response;
use Validator;
class CitasNotasController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(CitasNotas::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = CitasNotas::whereRaw('state=?',[$state])->with('users','clients','citas')->get();
                    break;
                }
                case 'cita':{
                    $objectSee = CitasNotas::whereRaw('cita=?',[$state])->with('users','clients','citas')->get();
                    break;
                }
                case 'client':{
                    $objectSee = CitasNotas::whereRaw('client=?',[$state])->with('users','clients','citas')->get();
                    break;
                }
                default:{
                    $objectSee = CitasNotas::whereRaw('state=?',[$state])->with('users','clients','citas')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = CitasNotas::with('users','clients','citas')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'client'          => 'required',
            'cita'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new CitasNotas();
                $newObject->titulo            = $request->get('titulo');
                $newObject->descripcion            = $request->get('descripcion');
                $newObject->comentario            = $request->get('comentario');
                $newObject->fechaInicio            = $request->get('fechaInicio');
                $newObject->fechaFin            = $request->get('fechaFin');
                $newObject->leido            = $request->get('leido');
                $newObject->state            = $request->get('state');
                $newObject->cita            = $request->get('cita');
                $newObject->client            = $request->get('client');
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = CitasNotas::find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = CitasNotas::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->titulo = $request->get('titulo', $objectUpdate->titulo);
                $objectUpdate->descripcion = $request->get('descripcion', $objectUpdate->descripcion);
                $objectUpdate->comentario = $request->get('comentario', $objectUpdate->comentario);
                $objectUpdate->fechaInicio = $request->get('fechaInicio', $objectUpdate->fechaInicio);
                $objectUpdate->fechaFin = $request->get('fechaFin', $objectUpdate->fechaFin);
                $objectUpdate->leido = $request->get('leido', $objectUpdate->leido);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->cita = $request->get('cita', $objectUpdate->cita);
                $objectUpdate->client = $request->get('client', $objectUpdate->client);
    
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = CitasNotas::find($id);
        if ($objectDelete) {
            try {
                CitasNotas::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
