<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PicturesProducts extends Model
{
    protected $table = 'pictures_products';
}
