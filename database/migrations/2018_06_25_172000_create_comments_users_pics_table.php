<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsUsersPicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments_users_pics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comment')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->integer('state')->default(1);

            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');

            $table->integer('comment_user')->nullable()->default(null)->unsigned();
            $table->foreign('comment_user')->references('id')->on('comments_users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments_users_pics');
    }
}
