<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ubicaciones extends Model
{
    protected $table = 'ubicaciones';

    public function clientes(){
        return $this->hasOne('App\Users','id','cliente');
    }

    public function apps(){
        return $this->hasOne('App\Users','id','app');
    }
}
