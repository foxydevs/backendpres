<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comercios extends Model
{
    protected $table = 'comercios';

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

}
