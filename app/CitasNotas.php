<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CitasNotas extends Model
{
    protected $table = 'citas_notas';
    
    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }
    
    public function clients() {
        return $this->hasOne('App\Users', 'id', 'client');
    }
    
    public function citas() {
        return $this->hasOne('App\Citas', 'id', 'cita');
    }
}
