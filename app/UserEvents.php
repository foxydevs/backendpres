<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEvents extends Model
{
    protected $table = 'user_events';

    public function user(){
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function users(){
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function events(){
        return $this->hasOne('App\Events', 'id', 'event');
    }
}
