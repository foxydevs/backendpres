<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersClients extends Model
{
    protected $table = 'users_clients';

    public function clients(){
        return $this->hasOne('App\Users','id','client');
    }

    public function users(){
        return $this->hasOne('App\Users','id','user');
    }
}
