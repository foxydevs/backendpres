<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $table = 'events';

    public function user() {
        return $this->hasOne('App\Users', 'id', 'user_created');
    }

    public function types() {
        return $this->hasOne('App\EventsType', 'id', 'type');
    }

    public function assistants() {
        return $this->hasMany('App\UserEvents', 'event')->where('state','1')->with('user');
    }

    public function interested() {
        return $this->hasMany('App\UserEvents', 'event')->where('state','0')->with('user');
    }

    public function pictures() {
        return $this->hasMany('App\PicturesEvents', 'event');
    }

    public function comments() {
        return $this->hasMany('App\CommentsEvents', 'event');
    }
}
