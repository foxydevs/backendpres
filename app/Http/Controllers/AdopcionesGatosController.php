<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\AdopcionesGatos;
use Response;
use Validator;
use Storage;
use DB;
use PDF;

class AdopcionesGatosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(AdopcionesGatos::whereRaw('tipo=2')->get(), 200);
    }

    public function getThisByClient($id)
    {
        $objectSee = AdopcionesGatos::whereRaw('user=? and tipo=2',$id)->with('users')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getThisByUser($id)
    {
        $objectSee = AdopcionesGatos::where('autoriza=? and tipo=2',$id)->with('users')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function printAdopcionesGatos($id,Request $request)
    {
        $objectSee = AdopcionesGatos::whereRaw('id=?',$id)->with('users')->first();
        if ($objectSee) {
            
            if($request->get('type') == 'JSON'){
                return Response::json($objectSee, 200);
                }else{
                   try{
                        $viewPDF = view('pdf.adopcionesGatos', ["data" => $objectSee]);
                        $pdf = PDF::loadHTML($viewPDF);
                        return $pdf->stream();
                   } catch(Exception $e){
                       $returnData = array (
                           'status' => 500,
                           
                           'message' => $e->getMessage()
                       );
                       return Response::json($returnData, 500);
                   }
                }
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
        
    }

    public function getThisByState($id,$state)
    {
        $objectSee = AdopcionesGatos::whereRaw('autoriza=? and state=? and tipo=2',[$id,$state])->with('users')->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }



    public function getThisByStateClients($id,$state)
    {
        $objectSee = AdopcionesGatos::whereRaw('user=? and state=? and tipo=2',[$id,$state])->with('users')->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user'          => 'required',
            'dpiAdoptante'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new AdopcionesGatos();
                $newObject->nombreAdoptante   = $request->get('nombreAdoptante');
                $newObject->edadAdoptante     = $request->get('edadAdoptante');
                $newObject->dpiAdoptante      = $request->get('dpiAdoptante');
                $newObject->nombreMascota     = $request->get('nombreMascota');
                $newObject->fechaSolicita     = $request->get('fechaSolicita');
                $newObject->fechaAutoriza     = $request->get('fechaAutoriza');
                $newObject->compromiso1       = $request->get('compromiso1');
                $newObject->compromiso2       = $request->get('compromiso2');
                $newObject->compromiso3       = $request->get('compromiso3');
                $newObject->compromiso4       = $request->get('compromiso4');
                $newObject->compromiso5       = $request->get('compromiso5');
                $newObject->compromiso6       = $request->get('compromiso6');
                $newObject->compromiso7       = $request->get('compromiso7');
                $newObject->compromiso8       = $request->get('compromiso8');
                $newObject->compromiso9       = $request->get('compromiso9');
                $newObject->firma             = $request->get('firma');
                $newObject->picture           = $request->get('picture');
                $newObject->pregunta1         = $request->get('pregunta1');
                $newObject->pregunta2         = $request->get('pregunta2');
                $newObject->pregunta3         = $request->get('pregunta3');
                $newObject->pregunta4         = $request->get('pregunta4');
                $newObject->pregunta5         = $request->get('pregunta5');
                $newObject->pregunta6         = $request->get('pregunta6');
                $newObject->pregunta7         = $request->get('pregunta7');
                $newObject->pregunta8         = $request->get('pregunta8');
                $newObject->pregunta9         = $request->get('pregunta9');
                $newObject->pregunta10        = $request->get('pregunta10');
                $newObject->pregunta11        = $request->get('pregunta11');
                $newObject->pregunta12        = $request->get('pregunta12');
                $newObject->pregunta13        = $request->get('pregunta13');
                $newObject->pregunta14        = $request->get('pregunta14');
                $newObject->pregunta15        = $request->get('pregunta15');
                $newObject->pregunta16        = $request->get('pregunta16');
                $newObject->pregunta17        = $request->get('pregunta17');
                $newObject->pregunta18        = $request->get('pregunta18');
                $newObject->pregunta19        = $request->get('pregunta19');
                $newObject->pregunta20        = $request->get('pregunta20');
                $newObject->pregunta21        = $request->get('pregunta21');
                $newObject->pregunta22        = $request->get('pregunta22');
                $newObject->pregunta23        = $request->get('pregunta23');
                $newObject->pregunta24        = $request->get('pregunta24');
                $newObject->pregunta25        = $request->get('pregunta25');
                $newObject->nombreAdulto1     = $request->get('nombreAdulto1');
                $newObject->edadAdulto1       = $request->get('edadAdulto1');
                $newObject->telefonoAdulto1   = $request->get('telefonoAdulto1');
                $newObject->nombreAdulto2     = $request->get('nombreAdulto2');
                $newObject->edadAdulto2       = $request->get('edadAdulto2');
                $newObject->telefonoAdulto2   = $request->get('telefonoAdulto2');
                $newObject->nombreAdulto3     = $request->get('nombreAdulto3');
                $newObject->edadAdulto3       = $request->get('edadAdulto3');
                $newObject->telefonoAdulto3   = $request->get('telefonoAdulto3');
                $newObject->nombreAdulto4     = $request->get('nombreAdulto4');
                $newObject->edadAdulto4       = $request->get('edadAdulto4');
                $newObject->telefonoAdulto4   = $request->get('telefonoAdulto4');
                $newObject->telefonoCasa      = $request->get('telefonoCasa');
                $newObject->telefonoCelular   = $request->get('telefonoCelular');
                $newObject->telefonoTrabajo   = $request->get('telefonoTrabajo');
                $newObject->correo            = $request->get('correo');
                $newObject->direccionCasa     = $request->get('direccionCasa');
                $newObject->direccionTrabajo  = $request->get('direccionTrabajo');
                $newObject->noFamilia         = $request->get('noFamilia');
                $newObject->noNino            = $request->get('noNino');
                $newObject->edadesNino        = $request->get('edadesNino');
                $newObject->aprobacion        = $request->get('aprobacion');
                $newObject->state             = $request->get('state');
                $newObject->user              = $request->get('user');
                $newObject->autoriza          = $request->get('autoriza');
                $newObject->tipo              = 2;
                $newObject->save();
                return Response::json($newObject, 200);
            
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = AdopcionesGatos::find($id);
        if ($objectSee) {
            $objectSee->column;
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = AdopcionesGatos::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->nombreAdoptante   = $request->get('nombreAdoptante', $objectUpdate->nombreAdoptante);
                $objectUpdate->edadAdoptante     = $request->get('edadAdoptante', $objectUpdate->edadAdoptante);
                $objectUpdate->dpiAdoptante      = $request->get('dpiAdoptante', $objectUpdate->dpiAdoptante);
                $objectUpdate->nombreMascota     = $request->get('nombreMascota', $objectUpdate->nombreMascota);
                $objectUpdate->fechaSolicita     = $request->get('fechaSolicita', $objectUpdate->fechaSolicita);
                $objectUpdate->fechaAutoriza     = $request->get('fechaAutoriza', $objectUpdate->fechaAutoriza);
                $objectUpdate->compromiso1       = $request->get('compromiso1', $objectUpdate->compromiso1);
                $objectUpdate->compromiso2       = $request->get('compromiso2', $objectUpdate->compromiso2);
                $objectUpdate->compromiso3       = $request->get('compromiso3', $objectUpdate->compromiso3);
                $objectUpdate->compromiso4       = $request->get('compromiso4', $objectUpdate->compromiso4);
                $objectUpdate->compromiso5       = $request->get('compromiso5', $objectUpdate->compromiso5);
                $objectUpdate->compromiso6       = $request->get('compromiso6', $objectUpdate->compromiso6);
                $objectUpdate->compromiso7       = $request->get('compromiso7', $objectUpdate->compromiso7);
                $objectUpdate->picture           = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->compromiso8       = $request->get('compromiso8', $objectUpdate->compromiso8);
                $objectUpdate->compromiso9       = $request->get('compromiso9', $objectUpdate->compromiso9);
                $objectUpdate->firma             = $request->get('firma', $objectUpdate->firma);
                $objectUpdate->pregunta1         = $request->get('pregunta1', $objectUpdate->pregunta1);
                $objectUpdate->pregunta2         = $request->get('pregunta2', $objectUpdate->pregunta2);
                $objectUpdate->pregunta3         = $request->get('pregunta3', $objectUpdate->pregunta3);
                $objectUpdate->pregunta4         = $request->get('pregunta4', $objectUpdate->pregunta4);
                $objectUpdate->pregunta5         = $request->get('pregunta5', $objectUpdate->pregunta5);
                $objectUpdate->pregunta6         = $request->get('pregunta6', $objectUpdate->pregunta6);
                $objectUpdate->pregunta7         = $request->get('pregunta7', $objectUpdate->pregunta7);
                $objectUpdate->pregunta8         = $request->get('pregunta8', $objectUpdate->pregunta8);
                $objectUpdate->pregunta9         = $request->get('pregunta9', $objectUpdate->pregunta9);
                $objectUpdate->pregunta10        = $request->get('pregunta10', $objectUpdate->pregunta10);
                $objectUpdate->pregunta11        = $request->get('pregunta11', $objectUpdate->pregunta11);
                $objectUpdate->pregunta12        = $request->get('pregunta12', $objectUpdate->pregunta12);
                $objectUpdate->pregunta13        = $request->get('pregunta13', $objectUpdate->pregunta13);
                $objectUpdate->pregunta14        = $request->get('pregunta14', $objectUpdate->pregunta14);
                $objectUpdate->pregunta15        = $request->get('pregunta15', $objectUpdate->pregunta15);
                $objectUpdate->pregunta16        = $request->get('pregunta16', $objectUpdate->pregunta16);
                $objectUpdate->pregunta17        = $request->get('pregunta17', $objectUpdate->pregunta17);
                $objectUpdate->pregunta18        = $request->get('pregunta18', $objectUpdate->pregunta18);
                $objectUpdate->pregunta19        = $request->get('pregunta19', $objectUpdate->pregunta19);
                $objectUpdate->pregunta20        = $request->get('pregunta20', $objectUpdate->pregunta20);
                $objectUpdate->pregunta21        = $request->get('pregunta21', $objectUpdate->pregunta21);
                $objectUpdate->pregunta22        = $request->get('pregunta22', $objectUpdate->pregunta22);
                $objectUpdate->pregunta23        = $request->get('pregunta23', $objectUpdate->pregunta23);
                $objectUpdate->pregunta24        = $request->get('pregunta24', $objectUpdate->pregunta24);
                $objectUpdate->pregunta25        = $request->get('pregunta25', $objectUpdate->pregunta25);
                $objectUpdate->nombreAdulto1     = $request->get('nombreAdulto1', $objectUpdate->nombreAdulto1);
                $objectUpdate->edadAdulto1       = $request->get('edadAdulto1', $objectUpdate->edadAdulto1);
                $objectUpdate->telefonoAdulto1   = $request->get('telefonoAdulto1', $objectUpdate->telefonoAdulto1);
                $objectUpdate->nombreAdulto2     = $request->get('nombreAdulto2', $objectUpdate->nombreAdulto2);
                $objectUpdate->edadAdulto2       = $request->get('edadAdulto2', $objectUpdate->edadAdulto2);
                $objectUpdate->telefonoAdulto2   = $request->get('telefonoAdulto2', $objectUpdate->telefonoAdulto2);
                $objectUpdate->nombreAdulto3     = $request->get('nombreAdulto3', $objectUpdate->nombreAdulto3);
                $objectUpdate->edadAdulto3       = $request->get('edadAdulto3', $objectUpdate->edadAdulto3);
                $objectUpdate->telefonoAdulto3   = $request->get('telefonoAdulto3', $objectUpdate->telefonoAdulto3);
                $objectUpdate->nombreAdulto4     = $request->get('nombreAdulto4', $objectUpdate->nombreAdulto4);
                $objectUpdate->telefonoAdulto4   = $request->get('telefonoAdulto4', $objectUpdate->telefonoAdulto4);
                $objectUpdate->edadAdulto4       = $request->get('edadAdulto4', $objectUpdate->edadAdulto4);
                $objectUpdate->telefonoCasa      = $request->get('telefonoCasa', $objectUpdate->telefonoCasa);
                $objectUpdate->telefonoCelular   = $request->get('telefonoCelular', $objectUpdate->telefonoCelular);
                $objectUpdate->telefonoTrabajo   = $request->get('telefonoTrabajo', $objectUpdate->telefonoTrabajo);
                $objectUpdate->correo            = $request->get('correo', $objectUpdate->correo);
                $objectUpdate->direccionCasa     = $request->get('direccionCasa', $objectUpdate->direccionCasa);
                $objectUpdate->direccionTrabajo  = $request->get('direccionTrabajo', $objectUpdate->direccionTrabajo);
                $objectUpdate->noFamilia         = $request->get('noFamilia', $objectUpdate->noFamilia);
                $objectUpdate->noNino            = $request->get('noNino', $objectUpdate->noNino);
                $objectUpdate->edadesNino        = $request->get('edadesNino', $objectUpdate->edadesNino);
                $objectUpdate->aprobacion        = $request->get('aprobacion', $objectUpdate->aprobacion);
                $objectUpdate->state             = $request->get('state', $objectUpdate->state);
                $objectUpdate->user              = $request->get('user', $objectUpdate->user);
                $objectUpdate->autoriza          = $request->get('autoriza', $objectUpdate->autoriza);
                $objectUpdate->tipo              = 2;
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = AdopcionesGatos::find($id);
        if ($objectDelete) {
            try {
                AdopcionesGatos::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
