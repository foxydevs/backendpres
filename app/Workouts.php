<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workouts extends Model
{
    protected $table = 'workouts';

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function comments() {
        return $this->hasMany('App\WorkoutsComments', 'workout');
    }
}
