<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Citas;
use DateTime;
use Response;
use Validator;

class CitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Citas::all(), 200);
    }

    public function getThisByUser($id)
    {
        $objectSee = Citas::where('user','=',$id)->with('users','clients','products')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getThisByUserDate(Request $request, $id)
    {
        $nowD = date('Y-m-d');
        $nowT = date('H:i:s');
        // $nowD = "2018-09-25";
        // $nowT = "21:08:21";
        $fecha1 = new DateTime($request->get('fechaInicio'));//fecha inicial
        $fecha2 = new DateTime($request->get('fechaFin'));//fecha de cierre

        $diff = $fecha1->diff($fecha2);
        
        $fecha = $request->get('fechaInicio');
        $nuevafechaMas = strtotime ( '+0 year' , strtotime ( $fecha ) ) ;
        $nuevafechaMas = date ( 'Y-m-d H:i:s' , $nuevafechaMas );
        if($diff->y>0){
            $nuevafechaMas = strtotime ( '+'.$diff->y.' year' , strtotime ( $fecha ) ) ;
            $nuevafechaMas = date ( 'Y-m-d H:i:s' , $nuevafechaMas );
        }
        if($diff->m>0){
            $nuevafechaMas = strtotime ( '+'.$diff->m.' month' , strtotime ( $fecha ) ) ;
            $nuevafechaMas = date ( 'Y-m-d H:i:s' , $nuevafechaMas );
        }
        if($diff->d>0){
            $nuevafechaMas = strtotime ( '+'.$diff->d.' day' , strtotime ( $fecha ) ) ;
            $nuevafechaMas = date ( 'Y-m-d H:i:s' , $nuevafechaMas );
        }
        if($diff->h>0){
            $nuevafechaMas = strtotime ( '+'.$diff->h.' hour' , strtotime ( $fecha ) ) ;
            $nuevafechaMas = date ( 'Y-m-d H:i:s' , $nuevafechaMas );
        }
        if($diff->i>0){
            $nuevafechaMas = strtotime ( '+'.$diff->i.' minute' , strtotime ( $fecha ) ) ;
            $nuevafechaMas = date ( 'Y-m-d H:i:s' , $nuevafechaMas );
        }
        if($diff->s>0){
            $nuevafechaMas = strtotime ( '+'.$diff->s.' second' , strtotime ( $fecha ) ) ;
            $nuevafechaMas = date ( 'Y-m-d H:i:s' , $nuevafechaMas );
        }

        $nuevafechaNeg = strtotime ( '+0 year' , strtotime ( $fecha ) ) ;
        $nuevafechaNeg = date ( 'Y-m-d H:i:s' , $nuevafechaNeg );
        if($diff->y>0){
            $nuevafechaNeg = strtotime ( '-'.$diff->y.' year' , strtotime ( $fecha ) ) ;
            $nuevafechaNeg = date ( 'Y-m-d H:i:s' , $nuevafechaNeg );
        }
        if($diff->m>0){
            $nuevafechaNeg = strtotime ( '-'.$diff->m.' month' , strtotime ( $fecha ) ) ;
            $nuevafechaNeg = date ( 'Y-m-d H:i:s' , $nuevafechaNeg );
        }
        if($diff->d>0){
            $nuevafechaNeg = strtotime ( '-'.$diff->d.' day' , strtotime ( $fecha ) ) ;
            $nuevafechaNeg = date ( 'Y-m-d H:i:s' , $nuevafechaNeg );
        }
        if($diff->h>0){
            $nuevafechaNeg = strtotime ( '-'.$diff->h.' hour' , strtotime ( $fecha ) ) ;
            $nuevafechaNeg = date ( 'Y-m-d H:i:s' , $nuevafechaNeg );
        }
        if($diff->i>0){
            $nuevafechaNeg = strtotime ( '-'.$diff->i.' minute' , strtotime ( $fecha ) ) ;
            $nuevafechaNeg = date ( 'Y-m-d H:i:s' , $nuevafechaNeg );
        }
        if($diff->s>0){
            $nuevafechaNeg = strtotime ( '-'.$diff->s.' second' , strtotime ( $fecha ) ) ;
            $nuevafechaNeg = date ( 'Y-m-d H:i:s' , $nuevafechaNeg );
        }

        $fecha = $request->get('fechaFin');
        $nuevafechaFinMas = strtotime ( '+0 year' , strtotime ( $fecha ) ) ;
        $nuevafechaFinMas = date ( 'Y-m-d H:i:s' , $nuevafechaFinMas );
        if($diff->y>0){
            $nuevafechaFinMas = strtotime ( '+'.$diff->y.' year' , strtotime ( $fecha ) ) ;
            $nuevafechaFinMas = date ( 'Y-m-d H:i:s' , $nuevafechaFinMas );
        }
        if($diff->m>0){
            $nuevafechaFinMas = strtotime ( '+'.$diff->m.' month' , strtotime ( $fecha ) ) ;
            $nuevafechaFinMas = date ( 'Y-m-d H:i:s' , $nuevafechaFinMas );
        }
        if($diff->d>0){
            $nuevafechaFinMas = strtotime ( '+'.$diff->d.' day' , strtotime ( $fecha ) ) ;
            $nuevafechaFinMas = date ( 'Y-m-d H:i:s' , $nuevafechaFinMas );
        }
        if($diff->h>0){
            $nuevafechaFinMas = strtotime ( '+'.$diff->h.' hour' , strtotime ( $fecha ) ) ;
            $nuevafechaFinMas = date ( 'Y-m-d H:i:s' , $nuevafechaFinMas );
        }
        if($diff->i>0){
            $nuevafechaFinMas = strtotime ( '+'.$diff->i.' minute' , strtotime ( $fecha ) ) ;
            $nuevafechaFinMas = date ( 'Y-m-d H:i:s' , $nuevafechaFinMas );
        }
        if($diff->s>0){
            $nuevafechaFinMas = strtotime ( '+'.$diff->s.' second' , strtotime ( $fecha ) ) ;
            $nuevafechaFinMas = date ( 'Y-m-d H:i:s' , $nuevafechaFinMas );
        }
        $nuevafechaFinNeg = strtotime ( '+0 year' , strtotime ( $fecha ) ) ;
        $nuevafechaFinNeg = date ( 'Y-m-d H:i:s' , $nuevafechaFinNeg );
        if($diff->y>0){
            $nuevafechaFinNeg = strtotime ( '-'.$diff->y.' year' , strtotime ( $fecha ) ) ;
            $nuevafechaFinNeg = date ( 'Y-m-d H:i:s' , $nuevafechaFinNeg );
        }
        if($diff->m>0){
            $nuevafechaFinNeg = strtotime ( '-'.$diff->m.' month' , strtotime ( $fecha ) ) ;
            $nuevafechaFinNeg = date ( 'Y-m-d H:i:s' , $nuevafechaFinNeg );
        }
        if($diff->d>0){
            $nuevafechaFinNeg = strtotime ( '-'.$diff->d.' day' , strtotime ( $fecha ) ) ;
            $nuevafechaFinNeg = date ( 'Y-m-d H:i:s' , $nuevafechaFinNeg );
        }
        if($diff->h>0){
            $nuevafechaFinNeg = strtotime ( '-'.$diff->h.' hour' , strtotime ( $fecha ) ) ;
            $nuevafechaFinNeg = date ( 'Y-m-d H:i:s' , $nuevafechaFinNeg );
        }
        if($diff->i>0){
            $nuevafechaFinNeg = strtotime ( '-'.$diff->i.' minute' , strtotime ( $fecha ) ) ;
            $nuevafechaFinNeg = date ( 'Y-m-d H:i:s' , $nuevafechaFinNeg );
        }
        if($diff->s>0){
            $nuevafechaFinNeg = strtotime ( '-'.$diff->s.' second' , strtotime ( $fecha ) ) ;
            $nuevafechaFinNeg = date ( 'Y-m-d H:i:s' , $nuevafechaFinNeg );
        }

        $objectSee = Citas::whereRaw('(fechaInicio<=? && fechaInicio>=?) or (fechaFin<=? && fechaFin>=?)',[$nuevafechaMas,$nuevafechaNeg,$nuevafechaFinMas,$nuevafechaFinNeg])->where('user','=',$id)->with('users','clients','products')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }



    public function getThisByClient($id)
    {
        $objectSee = Citas::where('client','=',$id)->with('users','clients','products')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getThisByClientsDate(Request $request, $id)
    {
        $nowD = date('Y-m-d');
        $nowT = date('H:i:s');
        // $nowD = "2018-09-25";
        // $nowT = "21:08:21";
        $fecha1 = new DateTime($request->get('fechaInicio'));//fecha inicial
        $fecha2 = new DateTime($request->get('fechaFin'));//fecha de cierre

        $diff = $fecha1->diff($fecha2);
        
        $fecha = $request->get('fechaInicio');
        $nuevafechaMas = strtotime ( '+0 year' , strtotime ( $fecha ) ) ;
        $nuevafechaMas = date ( 'Y-m-d H:i:s' , $nuevafechaMas );
        if($diff->y>0){
            $nuevafechaMas = strtotime ( '+'.$diff->y.' year' , strtotime ( $fecha ) ) ;
            $nuevafechaMas = date ( 'Y-m-d H:i:s' , $nuevafechaMas );
        }
        if($diff->m>0){
            $nuevafechaMas = strtotime ( '+'.$diff->m.' month' , strtotime ( $fecha ) ) ;
            $nuevafechaMas = date ( 'Y-m-d H:i:s' , $nuevafechaMas );
        }
        if($diff->d>0){
            $nuevafechaMas = strtotime ( '+'.$diff->d.' day' , strtotime ( $fecha ) ) ;
            $nuevafechaMas = date ( 'Y-m-d H:i:s' , $nuevafechaMas );
        }
        if($diff->h>0){
            $nuevafechaMas = strtotime ( '+'.$diff->h.' hour' , strtotime ( $fecha ) ) ;
            $nuevafechaMas = date ( 'Y-m-d H:i:s' , $nuevafechaMas );
        }
        if($diff->i>0){
            $nuevafechaMas = strtotime ( '+'.$diff->i.' minute' , strtotime ( $fecha ) ) ;
            $nuevafechaMas = date ( 'Y-m-d H:i:s' , $nuevafechaMas );
        }
        if($diff->s>0){
            $nuevafechaMas = strtotime ( '+'.$diff->s.' second' , strtotime ( $fecha ) ) ;
            $nuevafechaMas = date ( 'Y-m-d H:i:s' , $nuevafechaMas );
        }

        $nuevafechaNeg = strtotime ( '+0 year' , strtotime ( $fecha ) ) ;
        $nuevafechaNeg = date ( 'Y-m-d H:i:s' , $nuevafechaNeg );
        if($diff->y>0){
            $nuevafechaNeg = strtotime ( '-'.$diff->y.' year' , strtotime ( $fecha ) ) ;
            $nuevafechaNeg = date ( 'Y-m-d H:i:s' , $nuevafechaNeg );
        }
        if($diff->m>0){
            $nuevafechaNeg = strtotime ( '-'.$diff->m.' month' , strtotime ( $fecha ) ) ;
            $nuevafechaNeg = date ( 'Y-m-d H:i:s' , $nuevafechaNeg );
        }
        if($diff->d>0){
            $nuevafechaNeg = strtotime ( '-'.$diff->d.' day' , strtotime ( $fecha ) ) ;
            $nuevafechaNeg = date ( 'Y-m-d H:i:s' , $nuevafechaNeg );
        }
        if($diff->h>0){
            $nuevafechaNeg = strtotime ( '-'.$diff->h.' hour' , strtotime ( $fecha ) ) ;
            $nuevafechaNeg = date ( 'Y-m-d H:i:s' , $nuevafechaNeg );
        }
        if($diff->i>0){
            $nuevafechaNeg = strtotime ( '-'.$diff->i.' minute' , strtotime ( $fecha ) ) ;
            $nuevafechaNeg = date ( 'Y-m-d H:i:s' , $nuevafechaNeg );
        }
        if($diff->s>0){
            $nuevafechaNeg = strtotime ( '-'.$diff->s.' second' , strtotime ( $fecha ) ) ;
            $nuevafechaNeg = date ( 'Y-m-d H:i:s' , $nuevafechaNeg );
        }

        $fecha = $request->get('fechaFin');
        $nuevafechaFinMas = strtotime ( '+0 year' , strtotime ( $fecha ) ) ;
        $nuevafechaFinMas = date ( 'Y-m-d H:i:s' , $nuevafechaFinMas );
        if($diff->y>0){
            $nuevafechaFinMas = strtotime ( '+'.$diff->y.' year' , strtotime ( $fecha ) ) ;
            $nuevafechaFinMas = date ( 'Y-m-d H:i:s' , $nuevafechaFinMas );
        }
        if($diff->m>0){
            $nuevafechaFinMas = strtotime ( '+'.$diff->m.' month' , strtotime ( $fecha ) ) ;
            $nuevafechaFinMas = date ( 'Y-m-d H:i:s' , $nuevafechaFinMas );
        }
        if($diff->d>0){
            $nuevafechaFinMas = strtotime ( '+'.$diff->d.' day' , strtotime ( $fecha ) ) ;
            $nuevafechaFinMas = date ( 'Y-m-d H:i:s' , $nuevafechaFinMas );
        }
        if($diff->h>0){
            $nuevafechaFinMas = strtotime ( '+'.$diff->h.' hour' , strtotime ( $fecha ) ) ;
            $nuevafechaFinMas = date ( 'Y-m-d H:i:s' , $nuevafechaFinMas );
        }
        if($diff->i>0){
            $nuevafechaFinMas = strtotime ( '+'.$diff->i.' minute' , strtotime ( $fecha ) ) ;
            $nuevafechaFinMas = date ( 'Y-m-d H:i:s' , $nuevafechaFinMas );
        }
        if($diff->s>0){
            $nuevafechaFinMas = strtotime ( '+'.$diff->s.' second' , strtotime ( $fecha ) ) ;
            $nuevafechaFinMas = date ( 'Y-m-d H:i:s' , $nuevafechaFinMas );
        }
        $nuevafechaFinNeg = strtotime ( '+0 year' , strtotime ( $fecha ) ) ;
        $nuevafechaFinNeg = date ( 'Y-m-d H:i:s' , $nuevafechaFinNeg );
        if($diff->y>0){
            $nuevafechaFinNeg = strtotime ( '-'.$diff->y.' year' , strtotime ( $fecha ) ) ;
            $nuevafechaFinNeg = date ( 'Y-m-d H:i:s' , $nuevafechaFinNeg );
        }
        if($diff->m>0){
            $nuevafechaFinNeg = strtotime ( '-'.$diff->m.' month' , strtotime ( $fecha ) ) ;
            $nuevafechaFinNeg = date ( 'Y-m-d H:i:s' , $nuevafechaFinNeg );
        }
        if($diff->d>0){
            $nuevafechaFinNeg = strtotime ( '-'.$diff->d.' day' , strtotime ( $fecha ) ) ;
            $nuevafechaFinNeg = date ( 'Y-m-d H:i:s' , $nuevafechaFinNeg );
        }
        if($diff->h>0){
            $nuevafechaFinNeg = strtotime ( '-'.$diff->h.' hour' , strtotime ( $fecha ) ) ;
            $nuevafechaFinNeg = date ( 'Y-m-d H:i:s' , $nuevafechaFinNeg );
        }
        if($diff->i>0){
            $nuevafechaFinNeg = strtotime ( '-'.$diff->i.' minute' , strtotime ( $fecha ) ) ;
            $nuevafechaFinNeg = date ( 'Y-m-d H:i:s' , $nuevafechaFinNeg );
        }
        if($diff->s>0){
            $nuevafechaFinNeg = strtotime ( '-'.$diff->s.' second' , strtotime ( $fecha ) ) ;
            $nuevafechaFinNeg = date ( 'Y-m-d H:i:s' , $nuevafechaFinNeg );
        }

        $objectSee = Citas::whereRaw('(fechaInicio<=? && fechaInicio>=?) or (fechaFin<=? && fechaFin>=?)',[$nuevafechaMas,$nuevafechaNeg,$nuevafechaFinMas,$nuevafechaFinNeg])->where('client','=',$id)->with('users','clients','products')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'client'          => 'required',
            'user'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Citas();
                $newObject->comentario       = $request->get('comentario');
                $newObject->fechaInicio      = $request->get('fechaInicio');
                $newObject->fechaFin         = $request->get('fechaFin');
                $newObject->aprobacion       = $request->get('aprobacion');
                $newObject->state            = $request->get('state');
                $newObject->user             = $request->get('user');
                $newObject->client           = $request->get('client');
                $newObject->titulo           = $request->get('titulo');
                $newObject->descripcion      = $request->get('descripcion');
                $newObject->product          = $request->get('product');
                $newObject->save();
                return Response::json($newObject, 200);
            
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Citas::whereRaw('id=?',[$id])->with('users','clients','products')->first();
        if ($objectSee) {
            $objectSee->column;
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Citas::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->comentario       = $request->get('comentario', $objectUpdate->comentario);
                $objectUpdate->fechaInicio      = $request->get('fechaInicio', $objectUpdate->fechaInicio);
                $objectUpdate->fechaFin         = $request->get('fechaFin', $objectUpdate->fechaFin);
                $objectUpdate->aprobacion       = $request->get('aprobacion', $objectUpdate->aprobacion);
                $objectUpdate->state            = $request->get('state', $objectUpdate->state);
                $objectUpdate->user             = $request->get('user', $objectUpdate->user);
                $objectUpdate->client           = $request->get('client', $objectUpdate->client);
                $objectUpdate->product          = $request->get('product', $objectUpdate->product);
                $objectUpdate->titulo           = $request->get('titulo', $objectUpdate->titulo);
                $objectUpdate->descripcion          = $request->get('descripcion', $objectUpdate->descripcion);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Citas::find($id);
        if ($objectDelete) {
            try {
                Citas::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
