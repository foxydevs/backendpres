<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTips3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tips3', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('link')->nullable()->default(null);
            $table->string('link2')->nullable()->default(null);
            $table->string('video')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->string('picture1')->nullable()->default(null);
            $table->string('picture2')->nullable()->default(null);
            $table->string('picture3')->nullable()->default(null);
            $table->integer('state')->default(1);

            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');

            $table->integer('product')->nullable()->default(null)->unsigned();
            $table->foreign('product')->references('id')->on('products')->onDelete('cascade');

            $table->integer('event')->nullable()->default(null)->unsigned();
            $table->foreign('event')->references('id')->on('events')->onDelete('cascade');

            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tips3');
    }
}
