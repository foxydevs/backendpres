<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromocionesComments extends Model
{
    protected $table = 'promociones_comments';
}
