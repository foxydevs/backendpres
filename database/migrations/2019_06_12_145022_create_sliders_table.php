<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo')->nullable()->default(null);
            $table->string('descripcion')->nullable()->default(null);
            $table->string('url')->nullable()->default(null);
            $table->timestamp('fechaInicio')->useCurrent();
            $table->timestamp('fechaFin')->nullable()->default(null);

            $table->integer('activo')->default(1);
            $table->integer('state')->default(1);
            
            $table->integer('app')->nullable()->default(null)->unsigned();
            $table->foreign('app')->references('id')->on('users')->onDelete('cascade');
            
            $table->integer('client')->nullable()->default(null)->unsigned();
            $table->foreign('client')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
