<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Progreso;
use Response;
use Validator;

class ProgresoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Progreso::with('users')->get(), 200);
    }

    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state': {
                    $objectSee = Progreso::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
                    break;
                }
                case 'type': {
                    $objectSee = Progreso::whereRaw('user=? and tipo=?',[$id,$state])->with('user')->get();
                    break;
                }
                default:{
                    $objectSee = Progreso::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
                    break;
                }
                    
            }
        }else{
            $objectSee = Progreso::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
        }
        
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getThisByUser($id)
    {
        $objectSee = Progreso::where('user','=',$id)->with('users','clients')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getThisByClient($id,$estado)
    {
        $objectSee = Progreso::whereRaw('client=? and tipo=?',[$id,$estado])->with('users','clients')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getThisByClients($id)
    {
        $objectSee = Progreso::whereRaw('client=?',[$id])->with('users','clients')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'client'          => 'required',
            'user'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Progreso();
                $newObject->foto_espalda            = $request->get('foto_espalda');
                $newObject->foto_frente             = $request->get('foto_frente');
                $newObject->comentario              = $request->get('comentario');
                $newObject->complexion              = $request->get('complexion');
                $newObject->imc                     = $request->get('imc');
                $newObject->opcion1                 = $request->get('opcion1');
                $newObject->opcion2                 = $request->get('opcion2');
                $newObject->peso                    = $request->get('peso');
                $newObject->grasa                   = $request->get('grasa');
                $newObject->masa_muscular           = $request->get('masa_muscular');
                $newObject->imc_num                 = $request->get('imc_num');
                $newObject->opcionNum1              = $request->get('opcionNum1');
                $newObject->opcionNum2              = $request->get('opcionNum2');
                $newObject->state                   = $request->get('state');
                $newObject->tipo                    = $request->get('tipo');
                $newObject->client                  = $request->get('client');
                $newObject->user                    = $request->get('user');
                $newObject->save();
                return Response::json($newObject, 200);
            
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    public function uploadAvatar(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'avatar'      => 'required|image|mimes:jpeg,png,jpg'
        ]);
        
        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $path = Storage::disk('s3')->put($request->carpeta, $request->avatar);
        
                $objectUpdate->picture = Storage::disk('s3')->url($path);
                $objectUpdate->save();
        
                return Response::json($objectUpdate, 200);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
            }
        
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Progreso::find($id);
        if ($objectSee) {

            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Progreso::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->foto_espalda            = $request->get('foto_espalda', $objectUpdate->foto_espalda);
                $objectUpdate->foto_frente             = $request->get('foto_frente', $objectUpdate->foto_frente);
                $objectUpdate->comentario              = $request->get('comentario', $objectUpdate->comentario);
                $objectUpdate->complexion              = $request->get('complexion', $objectUpdate->complexion);
                $objectUpdate->imc                     = $request->get('imc', $objectUpdate->imc);
                $objectUpdate->opcion1                 = $request->get('opcion1', $objectUpdate->opcion1);
                $objectUpdate->opcion2                 = $request->get('opcion2', $objectUpdate->opcion2);
                $objectUpdate->peso                    = $request->get('peso', $objectUpdate->peso);
                $objectUpdate->grasa                   = $request->get('grasa', $objectUpdate->grasa);
                $objectUpdate->masa_muscular           = $request->get('masa_muscular', $objectUpdate->masa_muscular);
                $objectUpdate->imc_num                 = $request->get('imc_num', $objectUpdate->imc_num);
                $objectUpdate->opcionNum1              = $request->get('opcionNum1', $objectUpdate->opcionNum1);
                $objectUpdate->opcionNum2              = $request->get('opcionNum2', $objectUpdate->opcionNum2);
                $objectUpdate->state                   = $request->get('state', $objectUpdate->state);
                $objectUpdate->tipo                    = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->client                  = $request->get('client', $objectUpdate->client);
                $objectUpdate->user                    = $request->get('user', $objectUpdate->user);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Progreso::find($id);
        if ($objectDelete) {
            try {
                Progreso::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
