<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beneficios extends Model
{
    protected $table = 'beneficios';
    
    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }
}
