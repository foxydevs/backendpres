<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->string('video')->nullable()->default(null);
            $table->string('link')->nullable()->default(null);
            $table->date('fechaIni')->nullable()->default(null);
            $table->date('fechaFin')->nullable()->default(null);
            $table->time('horaIni')->nullable()->default(null);
            $table->time('horaFin')->nullable()->default(null);
            $table->string('dia')->nullable()->default(null);
            $table->string('semana')->nullable()->default(null);
            $table->integer('abierto')->nullable()->default(null);
            $table->integer('state')->default(1);
            $table->integer('type')->default(1);

            $table->string('icono')->nullable()->default(null);
            $table->string('color')->nullable()->default(null);
            $table->string('colorBg')->nullable()->default(null);
            $table->string('round')->nullable()->default(null);
            
            $table->integer('app')->nullable()->default(null)->unsigned();
            $table->foreign('app')->references('id')->on('users')->onDelete('cascade');

            $table->integer('client')->nullable()->default(null)->unsigned();
            $table->foreign('client')->references('id')->on('users')->onDelete('cascade');

            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios');
    }
}
