<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Events;
use App\Friends;
use App\Users;
use App\UsersClients;
use App\Advertising;
use App\UserEvents;
use Response;
use Validator;
use Storage;
use DB;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nowD = date('Y-m-d');
        $nowT = date('H:i:s');
        // $nowD = "2018-09-25";
        // $nowT = "21:08:21";
        return Response::json(Events::whereRaw('date>=? or (date>=? and time>=?)',[$nowD,$nowD,$nowT])->with('user','assistants','interested','pictures','comments')->orderby('date')->orderby('time')->get(), 200);
    }

    public function getThisByState($id,$state)
    {
        $nowD = date('Y-m-d');
        $nowT = date('H:i:s');
        // $nowD = "2018-09-25";
        // $nowT = "21:08:21";
        $objectSee = Events::whereRaw('(user_owner=? and state=?) and (date>=? or (date>=? and time>=?))',[$id,$state,$nowD,$nowD,$nowT])->with('user')->orderby('date')->orderby('time')->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getThisByType($id,$state)
    {
        $nowD = date('Y-m-d');
        $nowT = date('H:i:s');
        // $nowD = "2018-09-25";
        // $nowT = "21:08:21";
        $objectSee = Events::whereRaw('(user_owner=? and type=?) and (date>=? or (date>=? and time>=?))',[$id,$state,$nowD,$nowD,$nowT])->with('user')->orderby('date')->orderby('time')->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }



    public function getThisByTipo($id,$state)
    {
        $nowD = date('Y-m-d');
        $nowT = date('H:i:s');
        // $nowD = "2018-09-25";
        // $nowT = "21:08:21";
        $objectSee = Events::whereRaw('(user_owner=? and tipo=?) and (date>=? or (date>=? and time>=?))',[$id,$state,$nowD,$nowD,$nowT])->with('user')->orderby('date')->orderby('time')->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date'          => 'required',
            'time'          => 'required',
            'user_created'  => 'required',
        ]);

        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            $lat = $request->get('latitude');
            $lon = $request->get('longitude');
            if($lon != 0 && $lat != 0)
            {
                try {
                    $date = $request->get('date');
                    
                    if(strlen($date)==strlen('EEE, dd MMM yyyy')){
                        $day=substr($date,5,2);
                        $mon=substr($date,8,3);
                        $mon=$this->parseNumDate($mon);
                        $yea=substr($date,12,4);
                        $date = $yea."-".$mon."-".$day;
                    }
                    
                    $newObject = new Events();
                    $newObject->date            = $date;
                    $newObject->time            = $request->get('time');
                    $newObject->picture         = $request->get('picture', 'http://www.powerlight.es/img/p/es-default-large.jpg');
                    $newObject->description     = $request->get('description');
                    $newObject->latitude        = $lat;
                    $newObject->longitude       = $lon;
                    $newObject->user_owner      = $request->get('user_owner');
                    $newObject->user_created    = $request->get('user_created');
                    $newObject->place           = $request->get('place','este');
                    $newObject->place_id        = $request->get('place_id','1');
                    $newObject->address         = $request->get('address','null');
                    $newObject->tipo            = $request->get('tipo',null);
                    $newObject->state           = $request->get('state',1);
                    $newObject->type            = $request->get('type');
                    $newObject->save();
                    $newObject->advertising = Advertising::where('place','like',$newObject->place)->get();

                    $userEvent = new UserEvents();
                    $userEvent->state = 1;
                    $userEvent->event = $newObject->id;
                    $userEvent->user  = $request->get('user_created');
                    $userEvent->save();

                    if($request->get('user_created')==$request->get('user_owner')){
                        $user = Users::find($request->get('user_owner'));
                        $this->sendNotificationToFriends($user, $newObject);
                    }
                    
                    return Response::json($newObject, 200);

                } catch (Exception $e) {
                    $returnData = array (
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                    
                    return Response::json($returnData, 500);
                }
            }else{
                $returnData = array (
                    'status' => 400,
                    'message' => 'Invalid Parameters latitude or longitud',
                    'validator' => $validator
                );
                
                return Response::json($returnData, 500);
            }
        }
    }

    function sendNotificationToFriends($user, $event) {
        $clients = UsersClients::select('client')->whereRaw('user=?',[$user->id])->get();
        if ($clients) {
            $objectSee = Users::whereIn('id',$clients)->where('state','=','1')->with('types')->get();
        }
        $data = array();
        
        foreach ($objectSee as $key => $value) {
            if ($value->one_signal_id != "") {
                array_push($data, $value->one_signal_id);
            }
        }

        $fullname = $user->firstname." ".$user->lastname;
        $content = array('en' => $fullname.' create a new event in zfit', 
                         'es' => $fullname.' creo un nuevo evento zfit');

        $fields = array(
            'app_id' => '884209b4-840e-426d-9dd6-62e3b83d8d5c',
            'include_player_ids' => $data,
            'data' => array('view' => 'EventsView', 'event' => $event->id),
            'contents' => $content
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Authorization: Basic MzczNWVmNmYtNzY0NC00MDY5LWJmZTktM2IzZjc0MDMyZjlj'));
    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    
        $response = curl_exec($ch);
        curl_close($ch);
    }

    function parseNumDate($date)
    {
        switch($date)
        {
            case 'JAN':{
                return '01';
            }
            case 'FEB':{
                return '02';
            }
            case 'MAR':{
                return '03';
            }
            case 'APH':{
                return '04';
            }
            case 'MAY':{
                return '05';
            }
            case 'JUN':{
                return '06';
            }
            case 'JUL':{
                return '07';
            }
            case 'AUG':{
                return '08';
            }
            case 'SEP':{
                return '09';
            }
            case 'OCT':{
                return '10';
            }
            case 'NOV':{
                return '11';
            }
            case 'DIC':{
                return '12';
            }
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Events::with('user','assistants','interested','pictures','comments','types')->find($id);
        if ($objectSee) {
            $objectSee->user;
            $objectSee->assistants;
            $objectSee->interested;
            $objectSee->pictures;
            $objectSee->comments;
            $objectSee->advertising = Advertising::where('place','like',$objectSee->place)->get();
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getEventsByUser($id)
    {
        $nowD = date('Y-m-d');
        $nowT = date('H:i:s');
        // $nowD = "2018-09-25";
        // $nowT = "21:08:21";
        $objectSee = Events::whereRaw('(user_created=?) and (date>=? or (date>=? and time>=?))',[$id,$nowD,$nowD,$nowT])->with('user','assistants','interested','pictures','comments')->orderby('date')->orderby('time')->get();
        if ($objectSee) {
           
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Events::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->date = $request->get('date', $objectUpdate->date);
                $objectUpdate->time = $request->get('time', $objectUpdate->time);
                $objectUpdate->picture = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->description = $request->get('description', $objectUpdate->description);
                $objectUpdate->latitude = $request->get('latitude', $objectUpdate->latitude);
                $objectUpdate->longitude = $request->get('longitude', $objectUpdate->longitude);
                $objectUpdate->user_created = $request->get('user_created', $objectUpdate->user_created);
                $objectUpdate->user_owner = $request->get('user_owner', $objectUpdate->user_owner);
                $objectUpdate->place = $request->get('place', $objectUpdate->place);
                $objectUpdate->place_id = $request->get('place_id', $objectUpdate->place_id);
                $objectUpdate->address = $request->get('address', $objectUpdate->address);
                $objectUpdate->tipo = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->type = $request->get('type', $objectUpdate->type);
                $objectUpdate->save();

                $objectUpdate->user;
                $objectUpdate->assistants;
                $objectUpdate->interested;
                $objectUpdate->pictures;
                $objectUpdate->comments;
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function uploadAvatar(Request $request, $id) {
        $objectUpdate = Events::find($id);
        if ($objectUpdate) {

            $validator = Validator::make($request->all(), [
                'avatar'      => 'required|image|mimes:jpeg,png,jpg'
            ]);

            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $path = Storage::disk('s3')->put('events', $request->avatar);

                    $objectUpdate->picture = Storage::disk('s3')->url($path);
                    $objectUpdate->save();

                    return Response::json($objectUpdate, 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }

            }

            return Response::json($objectUpdate, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    public function eventsByFriends(Request $request)
    {
        $user = Users::find($request->get('id'));
        if ($user) {
            try {
                $senderUser  = Friends::whereRaw("user_send = ? AND state = 1", [$user->id])->with('receipt')->get();
                $receiveUSer = Friends::whereRaw("user_receipt = ? AND state = 1", [$user->id])->with('send')->get();

                $data = collect();

                foreach ($senderUser as $key => $value) {
                    $data->push($value->receipt->id);
                }

                foreach ($receiveUSer as $key => $value) {
                    $data->push($value->send->id);
                }

                //$data->push($user->id);

                $events = Events::whereIn("user_created", $data)->orderby('date')->orderby('time')->get();
                
                $showEventsByFriends = collect();
                foreach ($events as $key => $value) {
                    if(($value->date." ".$value->time)>date('Y-m-d H:m:s')){
                        $objectSee = Events::find($value['id']);
                        if ($objectSee) {
                            $objectSee->user;
                            $showEventsByFriends->push($objectSee);
                        }
                    }
                }
                return Response::json($showEventsByFriends, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    function getBoundaries($lat, $lng, $distance = 1, $earthRadius = 6371)
    {
        $return = array();
        
        // Los angulos para cada dirección
        $cardinalCoords = array('north' => '0',
                                'south' => '180',
                                'east' => '90',
                                'west' => '270');
        $rLat = deg2rad($lat);
        $rLng = deg2rad($lng);
        $rAngDist = $distance/$earthRadius;
        foreach ($cardinalCoords as $name => $angle)
        {
            $rAngle = deg2rad($angle);
            $rLatB = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
            $rLonB = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));
            $return[$name] = array('lat' => (float) rad2deg($rLatB), 
                                    'lng' => (float) rad2deg($rLonB));
        }
        return array('min_lat'  => $return['south']['lat'],
                    'max_lat' => $return['north']['lat'],
                    'min_lng' => $return['west']['lng'],
                    'max_lng' => $return['east']['lng']);
    }
    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
            } else {
                return $miles;
            }
    }
    public function eventsNear(Request $request){
        try {
            $lat = $request->get('latitude');
            $lon = $request->get('longitude');
            $distance = $request->get('distance');
            $box = $this->getBoundaries($lat, $lon, $distance);

            $eventsNear = DB::table('events')->select(DB::raw('id,date,time, (6371 * ACOS( 
                                            SIN(RADIANS(latitude)) 
                                            * SIN(RADIANS(' . $lat . ')) 
                                            + COS(RADIANS(longitude - ' . $lon . ')) 
                                            * COS(RADIANS(latitude)) 
                                            * COS(RADIANS(' . $lat . '))
                                            )
                               ) AS distance'))->whereRaw('((latitude BETWEEN ? AND ?) AND (longitude BETWEEN ? AND ?)) HAVING distance < ? ', 
                               [$box['min_lat'], $box['max_lat'], $box['min_lng'], $box['max_lng'], $distance])->get();
            /*$showEventsNear = collect();
            foreach ($eventsNear as $key => $value) {
                if(strtotime($value->date." ".$value->time) > date('Y-m-d H:m:s')){
                    $objectSee = Events::find($value->id);
                    if ($objectSee) {
                        $objectSee->user;
                        $objectSee->distance = $this->distance($objectSee->latitude,$objectSee->longitude,$lat,$lon,"K");
                        $showEventsNear->push($objectSee);
                    }
                }
            }*/
            
            $eventsId = collect();
            foreach ($eventsNear as $key => $value) {
                $eventsId->push($value->id);
            }

            $eventsOnDate = Events::whereIn('id', $eventsId)->whereRaw('CONCAT(date, " ", time) > ?', [date('Y-m-d H:m:s')] )->orderby('date','asc')->orderby('time','asc')->with('user')->get();

            return Response::json($eventsOnDate, 200);
        } catch (Exception $e) {
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Events::find($id);
        if ($objectDelete) {
            try {
                Events::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    function reportEvent($id) {
        $objectReport = Events::find($id);
        if ($objectReport) {
            try {
                return Response::json($objectReport, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}