<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RetosComments extends Model
{
    protected $table = 'retos_comments';

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function comments() {
        return $this->hasMany('App\RetosComments', 'parent','id')->with('users');
    }
}
