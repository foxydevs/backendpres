<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $table = 'social';

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function comments() {
        return $this->hasMany('App\SocialComments', 'social')->with('comments');
    }
}
