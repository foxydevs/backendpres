<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests;
use App\Orders;
use App\Products;
use App\Users;
use Response;
use Validator;
class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Orders::whereRaw('venta IS NULL')->with('products','clients','users')->get(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Orders::whereRaw('user=? and state=? and venta IS NULL',[$id,$state])->with('products','clients','users')->get();
                    break;
                }
                case 'ern':{
                    $objectSee = Orders::whereRaw('user=? and ern=? and venta IS NULL',[$id,$state])->with('products','clients','users')->get();
                    break;
                }
                case 'token':{
                    $objectSee = Orders::whereRaw('user=? and token=? and venta IS NULL',[$id,$state])->with('products','clients','users')->get();
                    break;
                }
                case 'aprobacion':{
                    $objectSee = Orders::whereRaw('user=? and aprobacion=? and venta IS NULL',[$id,$state])->with('products','clients','users')->get();
                    break;
                }
                case 'fechaapro':{
                    $objectSee = Orders::whereRaw('user=? and fechaapro=? and venta IS NULL',[$id,$state])->with('products','clients','users')->get();
                    break;
                }
                case 'deposito':{
                    $objectSee = Orders::whereRaw('user=? and deposito=? and venta IS NULL',[$id,$state])->with('products','clients','users')->get();
                    break;
                }
                case 'opcion1':{
                    $objectSee = Orders::whereRaw('user=? and opcion1=? and venta IS NULL',[$id,$state])->with('products','clients','users')->get();
                    break;
                }
                case 'opcion2':{
                    $objectSee = Orders::whereRaw('user=? and opcion2=? and venta IS NULL',[$id,$state])->with('products','clients','users')->get();
                    break;
                }
                case 'opcion3':{
                    $objectSee = Orders::whereRaw('user=? and opcion3=? and venta IS NULL',[$id,$state])->with('products','clients','users')->get();
                    break;
                }
                case 'opcion4':{
                    $objectSee = Orders::whereRaw('user=? and opcion4=? and venta IS NULL',[$id,$state])->with('products','clients','users')->get();
                    break;
                }
                case 'opcion5':{
                    $objectSee = Orders::whereRaw('user=? and opcion5=? and venta IS NULL',[$id,$state])->with('products','clients','users')->get();
                    break;
                }
                default:{
                    $objectSee = Orders::whereRaw('user=? and state=? and venta IS NULL',[$id,$state])->with('products','clients','users')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Orders::whereRaw('user=? and state=? and venta IS NULL',[$id,$state])->with('products','clients','users')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function pagarQPP(Request $request)
    {
        $base_api = "https://sandbox.qpaypro.com/payment/api_v1";

        $testMode = true;
        $sessionID = uniqid();
        $orgID = $testMode ? '1snn5n9w' : 'k8vif92e';
        if($request->id){
            switch($request->id){
                case '144':{
                    $testMode = false;
                    $base_api = "https://payments.qpaypro.com/checkout/api_v1";
                    $orgID = $testMode ? '1snn5n9w' : 'k8vif92e';
                    $mechantID = '5Y6UcxDxkDl6LFPffowfnStz';
                    $x_login = '5Y6UcxDxkDl6LFPffowfnStz';
                    $x_private_key = 'zqKrXUupSePfeXvW2Rok03E5';
                    $x_api_secret = 'wieOwJeETb5i0kNxPWSKOPYb';
                    break;
                }
                default:{
                    $x_login = 'visanetgt_qpay';
                    $x_private_key = '88888888888';
                    $x_api_secret = '99999999999';
                    break;
                }
            }
        } else {
            $x_login = 'visanetgt_qpay';
            $x_private_key = '88888888888';
            $x_api_secret = '99999999999';
        }
        $x_fp_timestamp = time();
        $x_relay_response = 'none';
        $x_relay_url = 'none';
        $x_type = 'AUTH_ONLY';
        $x_method = 'CC';
        $x_invoice_num = rand(1,999999);
        $x_fp_sequence = 1988679099;
        $x_audit_number = rand(1,999999);
        
        $array = array(
        "x_login"=> $x_login, 
        "x_private_key"=> $x_private_key,
        "x_api_secret"=> $x_api_secret,
        "x_product_id"=> $request->x_product_id,
        "x_audit_number"=> $x_audit_number,
        "x_fp_sequence"=> $x_fp_sequence,
        "x_fp_timestamp"=> $x_fp_timestamp,
        "x_invoice_num"=> $x_invoice_num,
        "x_currency_code"=> $request->x_currency_code,
        "x_amount"=> $request->x_amount,
        "x_line_item"=> $request->x_line_item,
        "x_freight"=> $request->x_freight,
        "x_email"=> $request->x_email,
        "cc_number"=> $request->cc_number,
        "cc_exp"=> $request->cc_exp,
        "cc_cvv2"=> $request->cc_cvv2,
        "cc_name"=> $request->cc_name,
        "x_first_name"=> $request->x_first_name,
        "x_last_name"=> $request->x_last_name,
        "x_company"=> $request->x_company,
        "x_address"=> $request->x_address,
        "x_city"=> $request->x_city,
        "x_state"=> $request->x_state,
        "x_country"=> $request->x_country,
        "x_zip"=> $request->x_zip,
        "x_relay_response"=> $x_relay_response,
        "x_relay_url"=> $x_relay_url,
        "x_type"=> $x_type,
        "x_method"=> $x_method,
        "http_origin"=> $request->http_origin,
        "cc_type"=> $request->cc_type,
        "visaencuotas"=> $request->visaencuotas,
        "device_fingerprint_id"=> $sessionID
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $base_api);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $array);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $resp = curl_exec($ch);
        $info = curl_getinfo($ch);
        $json = json_decode($resp);

        // var_dump($json);
        //var_dump($array);
        //echo '<br/><br/>';
        //var_dump($base_api);
        //echo '<br/><br/>';
        //var_dump($resp);
        
        return Response::json($json, 200);
    }

    public function pagar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cantidad'          => 'required',
            'precio'            => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'No ha llenado los campos adecuadamente.',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            /*
             * Lo primero es crear el objeto nusoap_client, al que se le pasa como
             * parámetro la URL de Conexión definida en la constante WSPG
             */

            // define("UID", "00a44a36adaab6310e6bf306b9d5969b");
            // define("WSK", "c6ef3680408284ca1addc87b64c48421");
            // define("SANDBOX", true);
            if($request->get("applicacion")){
                switch ($request->get("applicacion")) {
                    case '49':{
                        define("UID", "f000d4c73c37258c9e32d0ad1f26ef05");
                        define("WSK", "0aa5e92f54d6c0ceb0f058715abbd8c7");
                        define("SANDBOX", false);
                        break;
                    }
                    
                    default:{
                        define("UID", "00a44a36adaab6310e6bf306b9d5969b");
                        define("WSK", "c6ef3680408284ca1addc87b64c48421");
                        define("SANDBOX", true);
                        break;
                    }
                }
            }else{
                define("UID", "00a44a36adaab6310e6bf306b9d5969b");
                define("WSK", "c6ef3680408284ca1addc87b64c48421");
                define("SANDBOX", true);
            }
            
            $Pagadito = new Pagadito(UID, WSK);
            /*
             * Si se está realizando pruebas, necesita conectarse con Pagadito SandBox. Para ello llamamos
             * a la función mode_sandbox_on(). De lo contrario omitir la siguiente linea.
             */
            if (SANDBOX) {
                $Pagadito->mode_sandbox_on();
            }
            /*
             * Validamos la conexión llamando a la función connect(). Retorna
             * true si la conexión es exitosa. De lo contrario retorna false
             */
            if ($Pagadito->connect()) {
                /*
                 * Luego pasamos a agregar los detalles
                 */
                if ($request->get("cantidad") > 0) {
                    $Pagadito->add_detail($request->get("cantidad"), $request->get("descripcion"), $request->get("precio"), $request->get("url"));
                }
                
                
                //Agregando campos personalizados de la transacción
                $Pagadito->set_custom_param("param1", "Valor de param1");
                $Pagadito->set_custom_param("param2", "Valor de param2");
                $Pagadito->set_custom_param("param3", "Valor de param3");
                $Pagadito->set_custom_param("param4", "Valor de param4");
                $Pagadito->set_custom_param("param5", "Valor de param5");
        
                //Habilita la recepción de pagos preautorizados para la orden de cobro.
                $Pagadito->enable_pending_payments();
        
                /*
                 * Lo siguiente es ejecutar la transacción, enviandole el ern.
                 *
                 * A manera de ejemplo el ern es generado como un número
                 * aleatorio entre 1000 y 2000. Lo ideal es que sea una
                 * referencia almacenada por el Pagadito Comercio.
                 */
                if($request->get("ern"))
                {
                    $ern = $request->get("ern");
                }
                else{
                    $ern = rand(1000, 2000);
                }
                $eeror = $Pagadito->exec_trans($ern);
                if (!$eeror) {
                    /*
                     * En caso de fallar la transacción, verificamos el error devuelto.
                     * Debido a que la API nos puede devolver diversos mensajes de
                     * respuesta, validamos el tipo de mensaje que nos devuelve.
                     */
                    switch($Pagadito->get_rs_code())
                    {
                        case "PG2001":
                            {
                                $returnData = array (
                                    'status' => 400,
                                    'message' => 'Data incompleta'
                                );
                                return Response::json($returnData, 400);
                                break;
                            /*Incomplete data*/
                        }
                        case "PG3002":
                            {
                                $returnData = array (
                                    'status' => 400,
                                    'message' => 'Error General'
                                );
                                return Response::json($returnData, 400);
                                break;
                        }
                            /*Error*/
                        case "PG3003":
                            {
                                $returnData = array (
                                    'status' => 400,
                                    'message' => 'transaccion sin registrar'
                                );
                                return Response::json($returnData, 400);
                                break;
                        }
                            /*Unregistered transaction*/
                        case "PG3004":
                            {
                                $returnData = array (
                                    'status' => 400,
                                    'message' => 'Error matematico'
                                );
                                return Response::json($returnData, 400);
                                break;
                        }
                            /*Match error*/
                        case "PG3005":
                            {
                                $returnData = array (
                                    'status' => 400,
                                    'message' => 'Error de conexion'
                                );
                                return Response::json($returnData, 400);
                                break;
                        }
                            /*Disabled connection*/
                        default:
                            $returnData = array (
                                'status' => 400,
                                'message' => 'No ha podido funcionar 1 '.$Pagadito->get_rs_code()
                            );
                            return Response::json($returnData, 400);
                            break;
                    }
                }else{
                    $returnData = array (
                        'status' => 200,
                        'message' => 'Funcionando genial',
                        'token' => $eeror
                    );
                    return Response::json($returnData, 200);
                }
            } else {
                /*
                 * En caso de fallar la conexión, verificamos el error devuelto.
                 * Debido a que la API nos puede devolver diversos mensajes de
                 * respuesta, validamos el tipo de mensaje que nos devuelve.
                 */
                switch($Pagadito->get_rs_code())
                {
                    case "PG2001":
                        /*Incomplete data*/
                    case "PG3001":
                        /*Problem connection*/
                    case "PG3002":
                        /*Error*/
                    case "PG3003":
                        /*Unregistered transaction*/
                    case "PG3005":
                        /*Disabled connection*/
                    case "PG3006":
                        /*Exceeded*/
                    default:
                        $returnData = array (
                            'status' => 400,
                            'message' => 'No ha podido funcionar 2'
                        );
                        return Response::json($returnData, 400);
                        break;
                }
            }
        
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'unit_price'       => 'required',
            'quantity'         => 'required',
            'product'          => 'required',
            'user'             => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Orders();
                $newObject->comment            = $request->get('comment');
                $newObject->unit_price         = $request->get('unit_price');
                $newObject->quantity           = $request->get('quantity');
                $newObject->product            = $request->get('product');
                $newObject->presentacion            = $request->get('presentacion');
                $newObject->user               = $request->get('user');
                $newObject->picture            = $request->get('picture','');
                $newObject->ern                = $request->get('ern');
                $newObject->state              = $request->get('state',3);
                $newObject->client             = $request->get('client');
                $newObject->token              = $request->get('token',null);
                $newObject->total              = $request->get('unit_price')*$request->get('quantity');
                $newObject->deposito              = $request->get('deposito');
                $newObject->opcion1              = $request->get('opcion1');
                $newObject->opcion2              = $request->get('opcion2');
                $newObject->opcion3              = $request->get('opcion3');
                $newObject->opcion4              = $request->get('opcion4');
                $newObject->opcion5              = $request->get('opcion5');
                $newObject->opcion6              = $request->get('opcion6');
                $newObject->direccion              = $request->get('direccion');
                $newObject->libro              = $request->get('libro');
                $newObject->state              = $request->get('state');
                $newObject->save();
                if($request->get('membresia') && $request->get('membresia')=='1'){
                    $objectUpdate = Users::find($request->get('client'));
                    if ($objectUpdate) {
                        try {
                            $objectUpdate->finMembresia = $request->get('finMembresia', $objectUpdate->finMembresia);
                            $objectUpdate->tipoNivel = $request->get('tipoNivel', $objectUpdate->tipoNivel);
                            $objectUpdate->inicioMembresia = $request->get('inicioMembresia', $objectUpdate->inicioMembresia);
                            $objectUpdate->nivelMembresia = $request->get('nivelMembresia', $objectUpdate->nivelMembresia);
                            $objectUpdate->membresia = $request->get('membresia', $objectUpdate->membresia);
                    
                            $objectUpdate->save();
                            return Response::json($newObject, 200);
                        } catch (Exception $e) {
                            $returnData = array (
                                'status' => 500,
                                'message' => $e->getMessage()
                            );
                            return Response::json($returnData, 500);
                        }
                    }
                    else {
                        $returnData = array (
                            'status' => 404,
                            'message' => 'No record found'
                        );
                        return Response::json($returnData, 404);
                    }
                }

                if($request->get('libro') && $request->get('libro')=='1'){
                    try {
                        $newObject = new ProductsUsers();
                        $newObject->description            = $request->get('description', null);
                        $newObject->calificacion            = $request->get('calificacion', null);
                        $newObject->tipo            = $request->get('tipo', null);
                        $newObject->state            = $request->get('state', null);
                        $newObject->app            = $request->get('user',null);
                        $newObject->client            = $request->get('client', null);
                        $newObject->product            = $request->get('product', null);
                        $newObject->documento            = $request->get('documento', null);
                        $newObject->save();
                        return Response::json($newObject, 200);
                    
                    } catch (Exception $e) {
                        $returnData = array (
                            'status' => 500,
                            'message' => $e->getMessage()
                        );
                        return Response::json($returnData, 500);
                    }
                }
                return Response::json($newObject, 200);
                $userSend = Users::find($newObject->user);
                $userReceipt = Users::find($newObject->user_receipt); 
                $producto = Products::find($newObject->product); 
                $this->sendPushInvitate($userSend, $userReceipt, $producto);
            
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    public function findErnOrders($ern){
        $objectSee = Orders::with('products','clients','users')->whereRaw('ern=?',$ern)->first();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    public function decided($token, $ern){
        $des = substr($ern,0,2);
        $len = strlen($ern);
        if($des == '02' && $len == 6){
            echo "<html><head></head><body><script>window.close();</script></body></html>";
        }else{
            echo "<html><head></head><body><script>location.href = 'http://me.gtechnology.gt/home/cliente/pedidos/".$token."/".$ern." ';</script></body></html>";

        }
    }
    public function comprobanteCompra($id,Request $request)
    {
        if ($request->get('token')) {
            /*
             * Lo primero es crear el objeto Pagadito, al que se le pasa como
             * parámetros el UID y el WSK definidos en config.php
             */
            if($request->get("applicacion")){
                switch ($request->get("applicacion")) {
                    case '49':{
                        define("UID", "f000d4c73c37258c9e32d0ad1f26ef05");
                        define("WSK", "0aa5e92f54d6c0ceb0f058715abbd8c7");
                        define("SANDBOX", false);
                        break;
                    }
                    
                    default:{
                        define("UID", "00a44a36adaab6310e6bf306b9d5969b");
                        define("WSK", "c6ef3680408284ca1addc87b64c48421");
                        define("SANDBOX", true);
                        break;
                    }
                }
            }else{
                define("UID", "00a44a36adaab6310e6bf306b9d5969b");
                define("WSK", "c6ef3680408284ca1addc87b64c48421");
                define("SANDBOX", true);
            }
            $Pagadito = new Pagadito(UID, WSK);
            /*
             * Si se está realizando pruebas, necesita conectarse con Pagadito SandBox. Para ello llamamos
             * a la función mode_sandbox_on(). De lo contrario omitir la siguiente linea.
             */
            if (SANDBOX) {
                $Pagadito->mode_sandbox_on();
            }
            /*
             * Validamos la conexión llamando a la función connect(). Retorna
             * true si la conexión es exitosa. De lo contrario retorna false
             */
            if ($Pagadito->connect()) {
                /*
                 * Solicitamos el estado de la transacción llamando a la función
                 * get_status(). Le pasamos como parámetro el token recibido vía
                 * GET en nuestra URL de retorno.
                 */
                if ($Pagadito->get_status($request->get('token'))) {

                    /*
                     * Luego validamos el estado de la transacción, consultando el
                     * estado devuelto por la API.
                     */
                    switch($Pagadito->get_rs_status())
                    {
                        case "COMPLETED":{
                            /*
                             * Tratamiento para una transacción exitosa.
                             */ ///////////////////////////////////////////////////////////////////////////////////////////////////////
                             $objectUpdate = Orders::find($id);
                                if ($objectUpdate) {
                                    if(!$objectUpdate->token && !$objectUpdate->aprobacion && !$objectUpdate->fechaapro){
                                        $returnData11 = array (
                                            'status' => 200,
                                            'token' => $request->get('token'),
                                            'aprobacion' => $Pagadito->get_rs_reference(),
                                            'fecha' => $Pagadito->get_rs_date_trans(),
                                            'message' => 'Compra Exitosa'
                                        );
                                        try {
                                            if($objectUpdate->token==NULL){
                                                $objectUpdate->token = $request->get('token', $objectUpdate->token);
                                                $objectUpdate->aprobacion = $Pagadito->get_rs_reference();
                                                $objectUpdate->fechaapro = $Pagadito->get_rs_date_trans();
                                                $objectUpdate->state              = 1;
                                                $objectUpdate->save();
                                                $objectSee = Users::find($objectUpdate->user);
                                                if ($objectSee) {
                                                    $objectSeeProducts = Products::find($objectUpdate->product);
                                                    if($objectSeeProducts){
                                                        if($objectSeeProducts->membresia=="1"){
                                                            try {
                                                                    $fecha = date('Y-m-j');
                                                                    $perioro = "";
                                                                    switch ($objectSeeProducts->periodo) {
                                                                        case '1':{
                                                                            $perioro="day";
                                                                            break;
                                                                        }
                                                                        case '2':{
                                                                            $perioro="month";
                                                                            break;
                                                                        }
                                                                        case '3':{
                                                                            $perioro="year";
                                                                            break;
                                                                        }
                                                                        default:{
                                                                            $perioro="day";
                                                                            break;
                                                                        }
                                                                    }
                                                                    $nuevafecha = strtotime ( '+'.$objectSeeProducts->tiempo.' '.$perioro , strtotime ( $fecha ) ) ;
                                                                    $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
                                                                    $objectSee->membresia = $objectSeeProducts->id;
                                                                    $objectSee->inicioMembresia = $fecha;
                                                                    $objectSee->finMembresia = $nuevafecha;
                                                                    $objectSee->tipoNivel = $objectSeeProducts->tipo;
                                                                    $objectSee->nivelMembresia = $objectSeeProducts->nivel;
                                                                    $objectSee->save();
                                                                
                                                            } catch (Exception $e) {
                                                                $returnData = array (
                                                                    'status' => 500,
                                                                    'message' => $e->getMessage()
                                                                );
                                                                return Response::json($returnData, 500);
                                                            }
                                                        }else{
                                                            if($objectSeeProducts->quantity!='null'){
                                                                if($objectSeeProducts->quantity>0){
                                                                    $objectSeeProducts->quantity = $objectSeeProducts->quantity - $objectUpdate->quantity;
                                                                    $objectSeeProducts->save();
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    Mail::send('emails.pago', ['empresa' => 'GTechnology', 'url' => 'http://gtechnology.gt', 'app' => 'http://me.gtechnology.gt', 'autorizacion' => $Pagadito->get_rs_reference(), 'fecha' => $Pagadito->get_rs_date_trans(), 'username' => $objectSee->username, 'email' => $objectSee->email, 'name' => $objectSee->firstname.' '.$objectSee->lastname,], function (Message $message) use ($objectSee){
                                                        $message->from('info@foxylabs.gt', 'Info GTechnology')
                                                                ->sender('info@foxylabs.gt', 'Info GTechnology')
                                                                ->to($objectSee->email, $objectSee->firstname.' '.$objectSee->lastname)
                                                                ->replyTo('info@foxylabs.gt', 'Info GTechnology')
                                                                ->subject('Comprobante de Pago');
                                                    });
                                                
                                                }
                                                else {
                                                    $returnData = array (
                                                        'status' => 404,
                                                        'message' => 'No record found'
                                                    );
                                                    return Response::json($returnData, 404);
                                                }
                                            
                                            }
                                        } catch (Exception $e) {
                                            $returnData = array (
                                                'status' => 500,
                                                'message' => $e->getMessage()
                                            );
                                            return Response::json($returnData, 500);
                                        }
                                        return Response::json($returnData11, 200);
                                    }else{
                                        $returnData11 = array (
                                            'status' => 200,
                                            'token' => $objectUpdate->token,
                                            'aprobacion' => $objectUpdate->aprobacion,
                                            'fecha' => $objectUpdate->fechaapro,
                                            'message' => 'Compra Exitosa'
                                        );
                                        return Response::json($returnData11, 200);
                                    }
                                    
                                }
                                else {
                                    $returnData = array (
                                        'status' => 404,
                                        'message' => 'No record found'
                                    );
                                    return Response::json($returnData, 404);
                                }
                            }
                        
                        case "REGISTERED":{
                            
                            /*
                             * Tratamiento para una transacción aún en
                             * proceso.
                             */ ///////////////////////////////////////////////////////////////////////////////////////////////////////
                             $returnData = array (
                                'status' => 400,
                                'token' => $request->get('token'),
                                'message' => "operacion Cancelada"
                            );
                            return Response::json($returnData, 400);
                            break;
                        }
                        
                        case "VERIFYING":{
                            
                            /*
                             * La transacción ha sido procesada en Pagadito, pero ha quedado en verificación.
                             * En este punto el cobro xha quedado en validación administrativa.
                             * Posteriormente, la transacción puede marcarse como válida o denegada;
                             * por lo que se debe monitorear mediante esta función hasta que su estado cambie a COMPLETED o REVOKED.
                             */ ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            $returnData = array (
                                 'status' => 200,
                                 'token' => $request->get('token'),
                                 'aprobacion' => $Pagadito->get_rs_reference(),
                                 'fecha' => $Pagadito->get_rs_date_trans(),
                                 'message' => 'Compra en Validacion'
                             );
                             return Response::json($returnData, 200);
                            break;}
                        
                        case "REVOKED":{
                            
                            /*
                             * La transacción en estado VERIFYING ha sido denegada por Pagadito.
                             * En este punto el cobro ya ha sido cancelado.
                             */ ///////////////////////////////////////////////////////////////////////////////////////////////////////
                             $returnData = array (
                                'status' => 400,
                                'token' => $request->get('token'),
                                'message' => "Compra Denegada"
                            );
                            return Response::json($returnData, 400);
                            break;}
                        
                        case "FAILED":
                            /*
                             * Tratamiento para una transacción fallida.
                             */
                            {
                                $returnData = array (
                                    'status' => 500,
                                    'token' => $request->get('token'),
                                    'message' => "Compra Denegada"
                                );
                                return Response::json($returnData, 500);
                            }
                        default:
                            {
                            /*
                             * Por ser un ejemplo, se muestra un mensaje
                             * de error fijo.
                             */ ///////////////////////////////////////////////////////////////////////////////////////////////////////
                             $returnData = array (
                                'status' => 500,
                                'token' => $request->get('token'),
                                'message' => "Compra Denegada"
                            );
                            return Response::json($returnData, 500);
                            break;}
                    }
                } else {
                    /*
                     * En caso de fallar la petición, verificamos el error devuelto.
                     * Debido a que la API nos puede devolver diversos mensajes de
                     * respuesta, validamos el tipo de mensaje que nos devuelve.
                     */
                    switch($Pagadito->get_rs_code())
                    {
                        case "PG2001":
                            /*Incomplete data*/
                        case "PG3002":
                            /*Error*/
                        case "PG3003":
                            /*Unregistered transaction*/
                        default:
                            /*
                             * Por ser un ejemplo, se muestra un mensaje
                             * de error fijo.
                             */ ///////////////////////////////////////////////////////////////////////////////////////////////////////
                             $returnData = array (
                                'status' => 500,
                                'token' => $request->get('token'),
                                'message' => "error de transaccion Denegada"
                            );
                            return Response::json($returnData, 500);
                            break;
                    }
                }
            } else {
                /*
                 * En caso de fallar la conexión, verificamos el error devuelto.
                 * Debido a que la API nos puede devolver diversos mensajes de
                 * respuesta, validamos el tipo de mensaje que nos devuelve.
                 */
                switch($Pagadito->get_rs_code())
                {
                    case "PG2001":
                        /*Incomplete data*/
                    case "PG3001":
                        /*Problem connection*/
                    case "PG3002":
                        /*Error*/
                    case "PG3003":
                        /*Unregistered transaction*/
                    case "PG3005":
                        /*Disabled connection*/
                    case "PG3006":
                        /*Exceeded*/
                    default:
                        /*
                         * Aqui se muestra el código y mensaje de la respuesta del WSPG
                         */
                        $returnData = array (
                            'status' => 400,
                            'token' => $request->get('token'),
                            'message' => "Compra Denegada",
                            'COD' => $Pagadito->get_rs_code(),
                            'MSG' => $Pagadito->get_rs_message()
                        );
                        return Response::json($returnData, 400);
                        
                        break;
                }
            }
        } else {
            /*
             * Aqui se muestra el mensaje de error al no haber recibido el token por medio de la URL.
             */
            $returnData = array (
                'status' => 400,
                'token' => $request->get('token'),
                'message' => "no se recibieron los datos",
            );
            return Response::json($returnData, 400);
        }
    }
    public function sendPushInvitate($userSend, $userReceipt, $producto)
    {   
        $fullname = $userSend->firstname." ".$userSend->lastname;
        $content = array('en' => 'You have a Order from '.$producto->name.' did for '.$fullname, 
                         'es' => 'Tienes una Orden de '.$producto->name.' hecha por '.$fullname);

        $fields = array(
            'app_id' => '884209b4-840e-426d-9dd6-62e3b83d8d5c',
            'include_player_ids' => array($userReceipt->one_signal_id),
            'data' => array('view' => 'InvitationView'),
            'contents' => $content
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Authorization: Basic MWZiMWRhZTUtODU1NC00ODIyLTlkNTYtN2Q4ZDgxN2Q5ZDQ5'));
    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    
        $response = curl_exec($ch);
        curl_close($ch);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Orders::with('products','clients','users')->find($id);
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getOrdersByUser($id)
    {
        $objectSee = Products::select('id')->whereRaw('user_created=? and state=1',[$id])->get();
        if ($objectSee) {
            $objectSeeOrders = Orders::whereIn('product',$objectSee)->with('products','clients','users')->orderby('created')->get();
            return Response::json($objectSeeOrders, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getOrdersByState($id,$state)
    {
        $objectSee = Orders::whereRaw('user=? and state=?',[$id,$state])->with('products','clients','users')->orderby('created')->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }



    public function getOrdersByClients($id)
    {
        $objectSee = Orders::whereRaw('client=?',[$id])->with('products','clients','users')->orderby('created')->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }



    public function getThisByUsers($id)
    {
        $objectSee = Orders::whereRaw('user=?',[$id])->with('products','clients','users')->orderby('created')->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getOrdersByProducts($id)
    {
        $objectSee = Orders::where('product',$id)->with('products','clients','users')->orderby('created')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Orders::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->comment            = $request->get('comment', $objectUpdate->comment);
                $objectUpdate->unit_price         = $request->get('unit_price', $objectUpdate->unit_price);
                $objectUpdate->quantity           = $request->get('quantity', $objectUpdate->quantity);
                $objectUpdate->presentacion       = $request->get('presentacion', $objectUpdate->presentacion);
                $objectUpdate->product            = $request->get('product', $objectUpdate->product);
                $objectUpdate->user               = $request->get('user', $objectUpdate->user);
                $objectUpdate->state              = $request->get('state', $objectUpdate->state);
                $objectUpdate->picture            = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->fechaapro          = $request->get('fechaapro', $objectUpdate->fechaapro);
                $objectUpdate->aprobacion         = $request->get('aprobacion', $objectUpdate->aprobacion);
                $objectUpdate->token              = $request->get('token', $objectUpdate->token);
                $objectUpdate->ern                = $request->get('ern', $objectUpdate->ern);
                $objectUpdate->created            = $request->get('created', $objectUpdate->created);
                $objectUpdate->client             = $request->get('client', $objectUpdate->client);
                $objectUpdate->total              = $objectUpdate->unit_price*$objectUpdate->quantity;
                $objectUpdate->deposito             = $request->get('deposito', $objectUpdate->deposito);
                $objectUpdate->opcion1             = $request->get('opcion1', $objectUpdate->opcion1);
                $objectUpdate->opcion2             = $request->get('opcion2', $objectUpdate->opcion2);
                $objectUpdate->opcion3             = $request->get('opcion3', $objectUpdate->opcion3);
                $objectUpdate->opcion4             = $request->get('opcion4', $objectUpdate->opcion4);
                $objectUpdate->opcion5             = $request->get('opcion5', $objectUpdate->opcion5);
                $objectUpdate->opcion6             = $request->get('opcion6', $objectUpdate->opcion6);
                $objectUpdate->save();
            if($objectUpdate->state=='2'){
                $objectUpdate->state              = 1;
                $objectUpdate->save();
                
                $objectSee = Users::whereRaw('id=?',$objectUpdate->client)->first();
                if ($objectSee) {
                    $objectSeeProducts = Products::find($objectUpdate->product);
                    if($objectSeeProducts){
                        if($objectSeeProducts->membresia=="1"){
                            try {
                                    $fecha = date('Y-m-j');
                                    $perioro = "";
                                    switch ($objectSeeProducts->periodo) {
                                        case '1':{
                                            $perioro="day";
                                            break;
                                        }
                                        case '2':{
                                            $perioro="month";
                                            break;
                                        }
                                        case '3':{
                                            $perioro="year";
                                            break;
                                        }
                                        default:{
                                            $perioro="day";
                                            break;
                                        }
                                    }
                                    $nuevafecha = strtotime ( '+'.$objectSeeProducts->tiempo.' '.$perioro , strtotime ( $fecha ) ) ;
                                    $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
                                    $objectSee->membresia = $objectSeeProducts->id;
                                    $objectSee->inicioMembresia = $fecha;
                                    $objectSee->finMembresia = $nuevafecha;
                                    $objectSee->tipoNivel = $objectSeeProducts->nivel;
                                    $objectSee->nivelMembresia = $objectSeeProducts->tipo;
                                    $objectSee->save();
                                
                            } catch (Exception $e) {
                                $returnData = array (
                                    'status' => 500,
                                    'message' => $e->getMessage()
                                );
                                return Response::json($returnData, 500);
                            }
                        }
                        
                    }
                }
                else {
                    $returnData = array (
                        'status' => 404,
                        'message' => 'No record found'
                    );
                    return Response::json($returnData, 404);
                }
            }
                return Response::json($objectUpdate, 200);
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function uploadAvatar(Request $request, $id) {
        $objectUpdate = Orders::find($id);
        if ($objectUpdate) {

            $validator = Validator::make($request->all(), [
                'avatar'      => 'required|image|mimes:jpeg,png,jpg'
            ]);

            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $path = Storage::disk('s3')->put('orders', $request->avatar);

                    $objectUpdate->picture = Storage::disk('s3')->url($path);
                    $objectUpdate->save();

                    return Response::json($objectUpdate, 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }

            }

            return Response::json($objectUpdate, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Orders::find($id);
        if ($objectDelete) {
            try {
                Orders::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
