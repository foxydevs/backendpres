<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->nullable()->default(null);
            $table->time('time')->nullable()->default(null);
            $table->string('place_id')->nullable()->default(null);
            $table->string('place')->nullable()->default(null);
            $table->string('address')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->double('latitude',15,8)->nullable()->default(null);
            $table->double('longitude',15,8)->nullable()->default(null);
            $table->integer('tipo')->nullable()->default(null);
            $table->integer('state')->nullable()->default(null);

            $table->integer('user_created')->nullable()->default(null)->unsigned();
            $table->foreign('user_created')->references('id')->on('users')->onDelete('cascade');

            $table->integer('type')->nullable()->default(null)->unsigned();
            $table->foreign('type')->references('id')->on('events_type')->onDelete('cascade');

            $table->integer('user_owner')->nullable()->default(null)->unsigned();
            $table->foreign('user_owner')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
