<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accesos', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('agregar')->nullable()->default(0);
            $table->tinyInteger('modificar')->nullable()->default(0);
            $table->tinyInteger('mostrar')->nullable()->default(0);
            $table->tinyInteger('eliminar')->nullable()->default(0);
            $table->integer('orden')->nullable()->default(0);
            $table->integer('membresia')->nullable()->default(0);
            $table->string('apodo')->nullable()->default(null);

            $table->integer('usuario')->unsigned()->nullable()->default(null);
            $table->foreign('usuario')->references('id')->on('users')->onDelete('cascade');
            $table->integer('modulo')->unsigned();
            $table->foreign('modulo')->references('id')->on('modulos')->onDelete('cascade');
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accesos');
    }
}
