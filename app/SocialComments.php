<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialComments extends Model
{
    protected $table = 'social_comments';

    public function user() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function comments() {
        return $this->hasMany('App\SocialComments', 'social');
    }
}
