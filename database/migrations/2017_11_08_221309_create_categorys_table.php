<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->integer('orden')->nullable()->default(0);
            $table->integer('state')->default(1);

            $table->integer('user_created')->nullable()->default(null)->unsigned();
            $table->foreign('user_created')->references('id')->on('users')->onDelete('cascade');

            $table->integer('parent')->nullable()->default(null)->unsigned();
            $table->foreign('parent')->references('id')->on('categorys')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorys');
    }
}
