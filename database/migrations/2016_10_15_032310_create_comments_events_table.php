<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments_events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comment')->nullable()->default(null);
            $table->integer('state')->default(1);
            
            $table->integer('event')->nullable()->default(null)->unsigned();
            $table->foreign('event')->references('id')->on('events')->onDelete('cascade');

            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments_events');
    }
}
