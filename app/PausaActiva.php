<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PausaActiva extends Model
{
    protected $table = 'pausa_activa';

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function comments() {
        return $this->hasMany('App\PausaActivaComments', 'pausa_activa')->with('comments');
    }
}
