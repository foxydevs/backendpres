<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Promociones;
use Response;
use Validator;
class PromocionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Promociones::all(), 200);
    }

    public function getPromocionesByUser($id)
    {
        $objectSee = Promociones::where('user_created','=',$id)->with('users','events','comments')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'          => 'required',
            'user_created'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Promociones();
                $newObject->title            = $request->get('title');
                $newObject->description      = $request->get('description');
                $newObject->picture          = $request->get('picture');
                $newObject->state            = $request->get('state',1);
                $newObject->user_created     = $request->get('user_created');
                $newObject->inicio           = $request->get('inicio');
                $newObject->fin              = $request->get('fin');
                $newObject->descuento        = $request->get('descuento');
                $newObject->product          = $request->get('product');
                $newObject->event            = $request->get('event');
                $newObject->address          = $request->get('address');
                $newObject->latitude         = $request->get('latitude');
                $newObject->longitude        = $request->get('longitude');
                $newObject->place            = $request->get('place');
                $newObject->save();
                return Response::json($newObject, 200);
            
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Promociones::find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Promociones::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->title            = $request->get('title', $objectUpdate->title);
                $objectUpdate->description      = $request->get('description', $objectUpdate->description);
                $objectUpdate->picture          = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->state            = $request->get('state', $objectUpdate->state);
                $objectUpdate->user_created     = $request->get('user_created', $objectUpdate->user_created);
                $objectUpdate->place            = $request->get('place', $objectUpdate->place);
                $objectUpdate->inicio           = $request->get('inicio', $objectUpdate->inicio);
                $objectUpdate->fin              = $request->get('fin', $objectUpdate->fin);
                $objectUpdate->descuento        = $request->get('descuento', $objectUpdate->descuento);
                $objectUpdate->product          = $request->get('product', $objectUpdate->product);
                $objectUpdate->address          = $request->get('address', $objectUpdate->address);
                $objectUpdate->event            = $request->get('event', $objectUpdate->event);
                $objectUpdate->address          = $request->get('address', $objectUpdate->address);
                $objectUpdate->latitude         = $request->get('latitude', $objectUpdate->latitude);
                $objectUpdate->longitude        = $request->get('longitude', $objectUpdate->longitude);
                $objectUpdate->place            = $request->get('place', $objectUpdate->place);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Promociones::find($id);
        if ($objectDelete) {
            try {
                Promociones::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
