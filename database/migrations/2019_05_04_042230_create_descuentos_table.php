<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDescuentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('descuentos', function (Blueprint $table) {
            $table->increments('id');
            $table->double('cantidad')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('codigo')->nullable()->default(null);
            $table->double('latitude',15,8)->nullable()->default(null);
            $table->double('longitude',15,8)->nullable()->default(null);
            $table->integer('usado')->nullable()->default(0);
            $table->integer('tipo')->nullable()->default(1);
            $table->integer('state')->nullable()->default(1);

            $table->integer('cliente')->nullable()->default(null)->unsigned();
            $table->foreign('cliente')->references('id')->on('users')->onDelete('cascade');

            $table->integer('app')->nullable()->default(null)->unsigned();
            $table->foreign('app')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('descuentos');
    }
}
