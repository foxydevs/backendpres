<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\CommentsUsers;
use Response;
use Validator;
use Storage;
class CommentsUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(CommentsUsers::all(), 200);
    }
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = CommentsUsers::whereRaw('state=?',[$id,$state])->get();
                    break;
                }
                case 'tipo':{
                    $objectSee = CommentsUsers::whereRaw('tipo=?',[$id,$state])->get();
                    break;
                }
                default:{
                    $objectSee = CommentsUsers::whereRaw('state=?',[$id,$state])->get();
                    break;
                }
    
            }
        }else{
            $objectSee = CommentsUsers::whereRaw('state=?',[$id,$state])->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subject'          => 'required',
            'message'          => 'required',
            'user_send'        => 'required',
            'user_receipt'     => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new CommentsUsers();
                $newObject->subject            = $request->get('subject');
                $newObject->message            = $request->get('message');
                $newObject->picture            = $request->get('picture');
                $newObject->link1            = $request->get('link1');
                $newObject->link2            = $request->get('link2');
                $newObject->link3            = $request->get('link3');
                $newObject->user_send          = $request->get('user_send');
                $newObject->user_receipt       = $request->get('user_receipt');
                $newObject->tipo       = $request->get('tipo');
                $newObject->save();
                return Response::json($newObject, 200);
            
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = CommentsUsers::find($id);
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getMyReceipt(Request $request, $id)
    {
        if($request->get('tipo')){
            $objectSee = CommentsUsers::whereRaw('user_receipt=? and tipo=?',[$id,$request->get('tipo')])->get();
        }else{
            $objectSee = CommentsUsers::whereRaw('user_receipt=?',[$id])->get();
        }
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getMySend(Request $request, $id)
    {
        if($request->get('tipo')){
            $objectSee = CommentsUsers::whereRaw('user_send=? and tipo=?',[$id,$request->get('tipo')])->get();
        }else{
            $objectSee = CommentsUsers::whereRaw('user_send=?',[$id])->get();
        }
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = CommentsUsers::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->subject            = $request->get('subject', $objectUpdate->subject);
                $objectUpdate->message            = $request->get('message', $objectUpdate->message);
                $objectUpdate->picture            = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->user_send          = $request->get('user_send', $objectUpdate->user_send);
                $objectUpdate->user_receipt       = $request->get('user_receipt', $objectUpdate->user_receipt);
                $objectUpdate->state              = $request->get('state', $objectUpdate->state);
                $objectUpdate->link1              = $request->get('link1', $objectUpdate->link1);
                $objectUpdate->link2              = $request->get('link2', $objectUpdate->link2);
                $objectUpdate->link3              = $request->get('link3', $objectUpdate->link3);
                $objectUpdate->tipo              = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    
    public function uploadAvatar(Request $request, $id) {
        $objectUpdate = CommentsUsers::find($id);
        if ($objectUpdate) {

            $validator = Validator::make($request->all(), [
                'avatar'      => 'required|image|mimes:jpeg,png,jpg'
            ]);

            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $path = Storage::disk('s3')->put('messages', $request->avatar);

                    $objectUpdate->picture = Storage::disk('s3')->url($path);
                    $objectUpdate->save();

                    return Response::json($objectUpdate, 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }

            }

            return Response::json($objectUpdate, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }



    
    public function uploadAvatarOnly(Request $request) {
            $validator = Validator::make($request->all(), [
                'avatar'      => 'required'
            ]);

            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                try {

                    $path = Storage::disk('s3')->put($request->get('carpeta'), $request->avatar);
                    $object = (object) array("url" => Storage::disk('s3')->url($path),
                                             "path" => $path,
                                             "carpeta"=>$request->get('carpeta'));

                    return Response::json($object, 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }

            }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = CommentsUsers::find($id);
        if ($objectDelete) {
            try {
                CommentsUsers::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
