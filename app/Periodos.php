<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periodos extends Model
{
    protected $table = 'periodos';

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user_created');
    }

    public function parents() {
        return $this->hasOne('App\Periodos', 'id', 'parent');
    }

    public function childs() {
        return $this->hasMany('App\Periodos', 'parent', 'id');
    }
}
