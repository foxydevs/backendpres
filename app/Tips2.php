<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tips2 extends Model
{
    protected $table = 'tips2';

    public function products() {
        return $this->hasOne('App\Products', 'id', 'product');
    }

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function events() {
        return $this->hasOne('App\Events', 'id', 'event');
    }
}
