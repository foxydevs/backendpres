<?php

use Illuminate\Database\Seeder;

class ModulosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modulos')->insert([
            'nombre'           => "Categorias",
            'dir'              => "",
            'refId'            => "",
            'icono'            => "tachometer",
            'link'             => "categorias",
            'tipo'             => 1,
            'estado'           => 1,
            'orden'            => 1,
            'deleted_at'       => null,
            'default'          => 0,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('modulos')->insert([
            'nombre'           => "Productos",
            'dir'              => "",
            'refId'            => "",
            'icono'            => "tachometer",
            'link'             => "productos",
            'tipo'             => 1,
            'estado'           => 1,
            'orden'            => 2,
            'deleted_at'       => null,
            'default'          => 0,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('modulos')->insert([
            'nombre'           => "Pedidos",
            'dir'              => "",
            'refId'            => "",
            'icono'            => "tachometer",
            'link'             => "pedidos",
            'tipo'             => 1,
            'estado'           => 1,
            'orden'            => 3,
            'deleted_at'       => null,
            'default'          => 0,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('modulos')->insert([
            'nombre'           => "Eventos",
            'dir'              => "",
            'refId'            => "",
            'icono'            => "tachometer",
            'link'             => "eventos",
            'tipo'             => 1,
            'estado'           => 1,
            'orden'            => 4,
            'deleted_at'       => null,
            'default'          => 0,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('modulos')->insert([
            'nombre'           => "Youtube",
            'dir'              => "",
            'refId'            => "",
            'icono'            => "tachometer",
            'link'             => "youtube",
            'tipo'             => 1,
            'estado'           => 1,
            'orden'            => 5,
            'deleted_at'       => null,
            'default'          => 0,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('modulos')->insert([
            'nombre'           => "En vivo",
            'dir'              => "",
            'refId'            => "",
            'icono'            => "tachometer",
            'link'             => "news",
            'tipo'             => 1,
            'estado'           => 1,
            'orden'            => 6,
            'deleted_at'       => null,
            'default'          => 0,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('modulos')->insert([
            'nombre'           => "Tips",
            'dir'              => "",
            'refId'            => "",
            'icono'            => "tachometer",
            'link'             => "tips",
            'tipo'             => 1,
            'estado'           => 1,
            'orden'            => 7,
            'deleted_at'       => null,
            'default'          => 0,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('modulos')->insert([
            'nombre'           => "Mensajeria",
            'dir'              => "",
            'refId'            => "",
            'icono'            => "tachometer",
            'link'             => "mensajeria",
            'tipo'             => 1,
            'estado'           => 1,
            'orden'            => 8,
            'deleted_at'       => null,
            'default'          => 0,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('modulos')->insert([
            'nombre'           => "Configuracion",
            'dir'              => "",
            'refId'            => "",
            'icono'            => "tachometer",
            'link'             => "configuracion",
            'tipo'             => 1,
            'estado'           => 1,
            'orden'            => 9,
            'deleted_at'       => null,
            'default'          => 0,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
    }
}
