<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentsEvents extends Model
{
    protected $table = 'comments_events';

    public function thiss() {
        return $this->hasOne('App\Events', 'id', 'event');
    }

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }
}
