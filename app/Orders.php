<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';

    public function products() {
        return $this->hasOne('App\Products', 'id', 'product')->with('categorys');
    }

    public function presentaciones(){
        return $this->hasOne('App\PresentacionProducto','id','presentacion');
    }

    public function clients() {
        return $this->hasOne('App\Users', 'id', 'client');
    }

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }
}
