<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Social;
use Response;
use Validator;
class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Social::all(), 200);
    }

    public function getThisByUser($id)
    {
        $objectSee = Social::where('user','=',$id)->with('users')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user'          => 'required',
            'title'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Social();
                $newObject->title           = $request->get('title');
                $newObject->description     = $request->get('description');
                $newObject->picture         = $request->get('picture');
                $newObject->state           = $request->get('state',1);
                $newObject->user            = $request->get('user');
                $newObject->link1            = $request->get('link1');
                $newObject->link2            = $request->get('link2');
                $newObject->link3            = $request->get('link3');
                $newObject->link4            = $request->get('link4');
                $newObject->save();
                return Response::json($newObject, 200);
            
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Social::find($id);
        if ($objectSee) {
            $objectSee->column;
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Social::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->title           = $request->get('title', $objectUpdate->title);
                $objectUpdate->description     = $request->get('description', $objectUpdate->description);
                $objectUpdate->picture         = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->state           = $request->get('state', $objectUpdate->state);
                $objectUpdate->user            = $request->get('user', $objectUpdate->user);
                $objectUpdate->link1            = $request->get('link1', $objectUpdate->link1);
                $objectUpdate->link2            = $request->get('link2', $objectUpdate->link2);
                $objectUpdate->link3            = $request->get('link3', $objectUpdate->link3);
                $objectUpdate->link4            = $request->get('link4', $objectUpdate->link4);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Social::find($id);
        if ($objectDelete) {
            try {
                Social::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}