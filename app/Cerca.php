<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cerca extends Model
{
    protected $table = 'cerca';
    
    public function users() {
        return $this->hasOne('App\Users', 'id', 'user_owner');
    }
}
