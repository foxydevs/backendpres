<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Workouts;
use App\Users;
use App\UsersClients;
use Response;
use Validator;
use Storage;
use DB;

class WorkoutsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Workouts::all(), 200);
    }

    public function getWorkoutByUser($id)
    {
        $objectSee = Workouts::where('user','=',$id)->with('users')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }



    public function getWorkoutByClients($id)
    {
        $objectSee = Workouts::where('client','=',$id)->with('users')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'          => 'required',
            'user'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Workouts();
                $newObject->title            = $request->get('title');
                $newObject->description      = $request->get('description');
                $newObject->picture          = $request->get('picture');
                $newObject->picture1         = $request->get('picture1');
                $newObject->picture2         = $request->get('picture2');
                $newObject->picture3         = $request->get('picture3');
                $newObject->video            = $request->get('video');
                $newObject->state            = $request->get('state',1);
                $newObject->user             = $request->get('user');
                $newObject->product          = $request->get('product');
                $newObject->event            = $request->get('event');
                $newObject->client            = $request->get('client');
                $newObject->save();
                $user = Users::find($request->get('user'));
                $this->sendNotificationToFriends($user, $newObject);
                return Response::json($newObject, 200);
            
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    function sendNotificationToFriends($user, $event) {
        $clients = UsersClients::select('client')->whereRaw('user=?',[$user->id])->get();
        if ($clients) {
            $objectSee = Users::whereIn('id',$clients)->where('state','=','1')->with('types')->get();
        }
        $data = array();
        
        foreach ($objectSee as $key => $value) {
            if ($value->one_signal_id != "") {
                array_push($data, $value->one_signal_id);
            }
        }

        $fullname = $user->firstname." ".$user->lastname;
        $content = array('en' => $fullname.' create a new event in zfit', 
                         'es' => $fullname.' creo un nuevo evento zfit');

        $fields = array(
            'app_id' => '884209b4-840e-426d-9dd6-62e3b83d8d5c',
            'include_player_ids' => $data,
            'data' => array('view' => 'EventsView', 'event' => $event->id),
            'contents' => $content
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Authorization: Basic MzczNWVmNmYtNzY0NC00MDY5LWJmZTktM2IzZjc0MDMyZjlj'));
    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    
        $response = curl_exec($ch);
        curl_close($ch);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Workouts::find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Workouts::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->title            = $request->get('title', $objectUpdate->title);
                $objectUpdate->description      = $request->get('description', $objectUpdate->description);
                $objectUpdate->picture          = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->picture1          = $request->get('picture1', $objectUpdate->picture1);
                $objectUpdate->picture2          = $request->get('picture2', $objectUpdate->picture2);
                $objectUpdate->picture3          = $request->get('picture3', $objectUpdate->picture3);
                $objectUpdate->video          = $request->get('video', $objectUpdate->video);
                $objectUpdate->state            = $request->get('state', $objectUpdate->state);
                $objectUpdate->user             = $request->get('user', $objectUpdate->user);
                $objectUpdate->product         = $request->get('product', $objectUpdate->product);
                $objectUpdate->event            = $request->get('event', $objectUpdate->event);
                $objectUpdate->client            = $request->get('client', $objectUpdate->client);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Workouts::find($id);
        if ($objectDelete) {
            try {
                Workouts::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
