<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Descuentos extends Model
{
    protected $table = 'descuentos';

    public function clientes(){
        return $this->hasOne('App\Users','id','cliente');
    }

    public function apps(){
        return $this->hasOne('App\Users','id','app');
    }
}
