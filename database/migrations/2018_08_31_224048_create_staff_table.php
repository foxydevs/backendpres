<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('id');
            $table->string('puesto')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->integer('state')->default(1);
            $table->integer('tipo')->default(1);
            $table->integer('permiso')->default(1);

            $table->integer('app')->nullable()->default(null)->unsigned();
            $table->foreign('app')->references('id')->on('users')->onDelete('cascade');

            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
