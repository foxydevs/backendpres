<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdopcionesPerros extends Model
{
    protected $table = 'adopciones';
    
    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }
}
