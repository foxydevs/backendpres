<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rutinas extends Model
{
    protected $table = 'rutinas';

    public function clients() {
        return $this->hasOne('App\Users', 'id', 'client');
    }

    public function workouts() {
        return $this->hasOne('App\Workouts', 'id', 'workout');
    }

    public function periodos() {
        return $this->hasOne('App\Periodos', 'id', 'periodo')->with('parents');
    }

}
