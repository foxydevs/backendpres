<?php

use Illuminate\Database\Seeder;

class CategorysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorys')->insert([
            "id"                => 1,
            "name"              => "Software",
            "description"       => "Productos Software para todo tipo de empresas o personas",
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/categorys/whrfFwFxwvFsVA8gjGVBVP2YRuKof8ejxvxZ87b5.png",
            "state"             => 1,
            "user_created"      => 2,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('categorys')->insert([
            "id"                => 2,
            "name"              => "Aplicaciones",
            "description"       => "Aplicaciones Masivas para cualquier empresa",
            "picture"           => null,
            "state"             => 1,
            "user_created"      => 3,
            "created_at"        => "2018-03-13 15:03:35",
            "updated_at"        => "2018-03-13 15:03:35"
        ]);

        DB::table('categorys')->insert([
            "id"                => 3,
            "name"              => "Coach tanatologico",
            "description"       => "Aprende a manejar el duelo",
            "picture"           => null,
            "state"             => 1,
            "user_created"      => 5,
            "created_at"        => "2018-04-19 11:51:07",
            "updated_at"        => "2018-04-19 11:51:07"
        ]);

        DB::table('categorys')->insert([
            "id"                => 5,
            "name"              => "Renta de Casas",
            "description"       => "Renta de casas para los miembros",
            "picture"           => null,
            "state"             => 1,
            "user_created"      => 9,
            "created_at"        => "2018-04-19 13:27:09",
            "updated_at"        => "2018-04-19 13:31:35"
        ]);

        DB::table('categorys')->insert([
            "id"                => 7,
            "name"              => "Hardware",
            "description"       => null,
            "picture"           => null,
            "state"             => 1,
            "user_created"      => 2,
            "created_at"        => "2018-04-19 16:30:00",
            "updated_at"        => "2018-04-19 16:30:00"
        ]);

        DB::table('categorys')->insert([
            "id"                => 8,
            "name"              => "medio social",
            "description"       => "medio social humanitario que ayude alas familias económicamente.",
            "picture"           => null,
            "state"             => 1,
            "user_created"      => 9,
            "created_at"        => "2018-04-20 17:19:23",
            "updated_at"        => "2018-04-20 17:19:23"
        ]);

        DB::table('categorys')->insert([
            "id"                => 38,
            "name"              => "Categoria 1",
            "description"       => "Esta es la descripcion de la categoria 1",
            "picture"           => null,
            "state"             => 1,
            "user_created"      => 16,
            "created_at"        => "2018-06-04 12:55:33",
            "updated_at"        => "2018-06-04 12:56:59"
        ]);

        DB::table('categorys')->insert([
            "id"                => 42,
            "name"              => "Plata",
            "description"       => "Cadenas y pulseras de plata",
            "picture"           => null,
            "state"             => 1,
            "user_created"      => 19,
            "created_at"        => "2018-06-04 23:59:47",
            "updated_at"        => "2018-06-04 23:59:47"
        ]);

        DB::table('categorys')->insert([
            "id"                => 55,
            "name"              => "Aplicaciones",
            "description"       => "Aplicaciones moviles para todo tipo de usuario",
            "picture"           => null,
            "state"             => 1,
            "user_created"      => 22,
            "created_at"        => "2018-06-06 15:57:26",
            "updated_at"        => "2018-06-06 15:57:26"
        ]);

        DB::table('categorys')->insert([
            "id"                => 65,
            "name"              => "Bocinas y Amplificadores",
            "description"       => "Para que tengas el mejor Zumbido...",
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/categorys/oNG5xgzkrCHjmIlVPAo6ZG6zAg2HsqNiOY5TinCv.jpeg",
            "state"             => 1,
            "user_created"      => 24,
            "created_at"        => "2018-06-20 16:08:26",
            "updated_at"        => "2018-07-10 23:06:23"
        ]);

        DB::table('categorys')->insert([
            "id"                => 80,
            "name"              => "Premium",
            "description"       => "Café de la mas alta calidad",
            "picture"           => null,
            "state"             => 1,
            "user_created"      => 25,
            "created_at"        => "2018-06-22 10:03:55",
            "updated_at"        => "2018-06-22 10:03:55"
        ]);

        DB::table('categorys')->insert([
            "id"                => 83,
            "name"              => "Cables y Accesorios",
            "description"       => "Para que no te enredes ingresa...",
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/categorys/rYKWfWEaLYtDMxJabx0mvgDZnrfsxu4XM1flCFXQ.jpeg",
            "state"             => 1,
            "user_created"      => 24,
            "created_at"        => "2018-07-09 15:54:21",
            "updated_at"        => "2018-07-09 23:08:05"
        ]);

        DB::table('categorys')->insert([
            "id"                => 84,
            "name"              => "Microfonía",
            "description"       => "¿Te atreves a gritar?",
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/categorys/A816dd6d4n7GXs2PYwN0jNgziGnlYgx0ujDJ5nLk.jpeg",
            "state"             => 1,
            "user_created"      => 24,
            "created_at"        => "2018-07-09 16:04:49",
            "updated_at"        => "2018-07-09 23:09:23"
        ]);

        DB::table('categorys')->insert([
            "id"                => 85,
            "name"              => "Instrumentos",
            "description"       => "Todo lo que buscas para hacer buya.",
            "picture"           => "https://bpresentacion.s3.us-west-2.amazonaws.com/categorys/oLCOgnbayUtEECxoAlUVuQufmU9CaCPwHglzJZXZ.jpeg",
            "state"             => 1,
            "user_created"      => 24,
            "created_at"        => "2018-07-09 17:39:53",
            "updated_at"        => "2018-07-09 17:41:55"
        ]);

    }
}
