<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sliders extends Model
{
    protected $table = 'sliders';

    public function apps() {
        return $this->hasOne('App\Users', 'id', 'app');
    }

    public function clients() {
        return $this->hasOne('App\Users', 'id', 'client');
    }

}
