<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUbicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ubications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->time('open')->nullable()->default(null);
            $table->time('close')->nullable()->default(null);
            $table->double('latitude',15,8)->nullable()->default(null);
            $table->double('longitude',15,8)->nullable()->default(null);
            $table->integer('state')->default(1);
            
            $table->integer('user_created')->nullable()->default(null)->unsigned();
            $table->foreign('user_created')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ubications');
    }
}
