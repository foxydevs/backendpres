<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentsNews extends Model
{
    protected $table = 'comments_news';

    public function thiss() {
        return $this->hasOne('App\News', 'id', 'new');
    }

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }
}
