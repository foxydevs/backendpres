<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\PresentacionProducto;
use Response;
use Validator;

class PresentacionProductoController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(PresentacionProducto::with('apps','clients','products','presentaciones')->get(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = PresentacionProducto::whereRaw('app=? and state=?',[$id,$state])->with('apps','clients','products','presentaciones')->get();
                    break;
                }
                case 'product':{
                    $objectSee = PresentacionProducto::whereRaw('app=? and product=?',[$id,$state])->with('apps','clients','products','presentaciones')->get();
                    break;
                }
                case 'type':{
                    $objectSee = PresentacionProducto::whereRaw('app=? and tipo=?',[$id,$state])->with('apps','clients','products','presentaciones')->get();
                    break;
                }
                default:{
                    $objectSee = PresentacionProducto::whereRaw('app=? and state=?',[$id,$state])->with('apps','clients','products','presentaciones')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = PresentacionProducto::whereRaw('app=? and state=?',[$id,$state])->with('apps','clients','products','presentaciones')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = PresentacionProducto::where('app','=',$id)->with('apps','clients','products','presentaciones')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = PresentacionProducto::where('client','=',$id)->with('apps','clients','products','presentaciones')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product'          => 'required',
            'presentacion'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new PresentacionProducto();
                $newObject->description            = $request->get('description', null);
                $newObject->calificacion            = $request->get('calificacion', null);
                $newObject->tipo            = $request->get('tipo', null);
                $newObject->state            = $request->get('state', null);
                $newObject->app            = $request->get('app', null);
                $newObject->client            = $request->get('client', null);
                $newObject->product            = $request->get('product', null);
                $newObject->presentacion            = $request->get('presentacion', null);
                $newObject->cost                    = $request->get('cost', null);
                $newObject->quantity                = $request->get('quantity', null);
                $newObject->price                   = $request->get('price', null);
                $newObject->priceEspecial           = $request->get('priceEspecial', null);
                $newObject->priceFinal              = $request->get('priceFinal', null);
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    public function uploadAvatar(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'avatar'      => 'required|image|mimes:jpeg,png,jpg'
        ]);
    
        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
    
                $path = Storage::disk('s3')->put($request->carpeta, $request->avatar);
    
                $objectUpdate->picture = Storage::disk('s3')->url($path);
                $objectUpdate->save();
    
                return Response::json($objectUpdate, 200);
    
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
            }
    
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = PresentacionProducto::find($id);
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = PresentacionProducto::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->column = $request->get('get', $objectUpdate->column);
                $objectUpdate->description            = $request->get('description', $objectUpdate->description);
                $objectUpdate->calificacion            = $request->get('calificacion', $objectUpdate->calificacion);
                $objectUpdate->tipo            = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->state            = $request->get('state', $objectUpdate->state);
                $objectUpdate->app            = $request->get('app', $objectUpdate->app);
                $objectUpdate->client            = $request->get('client', $objectUpdate->client);
                $objectUpdate->product            = $request->get('product', $objectUpdate->product);
                $objectUpdate->presentacion            = $request->get('presentacion', $objectUpdate->presentacion);
                $objectUpdate->cost                   = $request->get('cost', $objectUpdate->cost);
                $objectUpdate->quantity                   = $request->get('quantity', $objectUpdate->quantity);
                $objectUpdate->price                   = $request->get('price', $objectUpdate->price);
                $objectUpdate->priceEspecial           = $request->get('priceEspecial', $objectUpdate->priceEspecial);
                $objectUpdate->priceFinal              = $request->get('priceFinal', $objectUpdate->priceFinal);
                $objectUpdate->save();
                $objectUpdate->products;
                $objectUpdate->apps;
                $objectUpdate->clients;
                $objectUpdate->presentaciones;
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = PresentacionProducto::find($id);
        if ($objectDelete) {
            try {
                PresentacionProducto::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
