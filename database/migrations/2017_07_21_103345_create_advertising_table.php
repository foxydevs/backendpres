<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertising', function (Blueprint $table) {
            $table->increments('id');
            $table->string('place')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->string('client')->nullable()->default(null);
            $table->dateTime('begin')->nullable()->default(null);
            $table->dateTime('end')->nullable()->default(null);
            $table->double('first_latitud',15,8)->nullable()->default(null);
            $table->double('first_longitud',15,8)->nullable()->default(null);
            $table->double('last_latitud',15,8)->nullable()->default(null);
            $table->double('last_longitud',15,8)->nullable()->default(null);
            $table->tinyInteger('state')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertising');
    }
}
