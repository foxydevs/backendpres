<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentsUsers extends Model
{
    protected $table = 'comments_users';
}
