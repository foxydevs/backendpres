<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdopcionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adopciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombreAdoptante')->nullable()->default(null);
            $table->string('edadAdoptante')->nullable()->default(null);
            $table->string('dpiAdoptante')->nullable()->default(null);
            $table->string('nombreMascota')->nullable()->default(null);
            $table->timestamp('fechaSolicita')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fechaAutoriza')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->boolean('compromiso1')->nullable()->default(false);
            $table->boolean('compromiso2')->nullable()->default(false);
            $table->boolean('compromiso3')->nullable()->default(false);
            $table->boolean('compromiso4')->nullable()->default(false);
            $table->boolean('compromiso5')->nullable()->default(false);
            $table->boolean('compromiso6')->nullable()->default(false);
            $table->boolean('compromiso7')->nullable()->default(false);
            $table->boolean('compromiso8')->nullable()->default(false);
            $table->boolean('compromiso9')->nullable()->default(false);
            $table->string('firma')->nullable()->default(false);

            $table->string('pregunta1')->nullable()->default(false);
            $table->string('pregunta2')->nullable()->default(false);
            $table->string('pregunta3')->nullable()->default(false);
            $table->string('pregunta4')->nullable()->default(false);
            $table->string('pregunta5')->nullable()->default(false);
            $table->string('pregunta6')->nullable()->default(false);
            $table->string('pregunta7')->nullable()->default(false);
            $table->string('pregunta8')->nullable()->default(false);
            $table->string('pregunta9')->nullable()->default(false);
            $table->string('pregunta10')->nullable()->default(false);
            $table->string('pregunta11')->nullable()->default(false);
            $table->string('pregunta12')->nullable()->default(false);
            $table->string('pregunta13')->nullable()->default(false);
            $table->string('pregunta14')->nullable()->default(false);
            $table->string('pregunta15')->nullable()->default(false);
            $table->string('pregunta16')->nullable()->default(false);
            $table->string('pregunta17')->nullable()->default(false);
            $table->string('pregunta18')->nullable()->default(false);
            $table->string('pregunta19')->nullable()->default(false);
            $table->string('pregunta20')->nullable()->default(false);
            $table->string('pregunta21')->nullable()->default(false);
            $table->string('pregunta22')->nullable()->default(false);
            $table->string('pregunta23')->nullable()->default(false);
            $table->string('pregunta24')->nullable()->default(false);
            $table->string('pregunta25')->nullable()->default(false);
            
            $table->string('nombreAdulto1')->nullable()->default(null);
            $table->string('edadAdulto1')->nullable()->default(null);
            $table->string('telefonoAdulto1')->nullable()->default(null);
            $table->string('nombreAdulto2')->nullable()->default(null);
            $table->string('edadAdulto2')->nullable()->default(null);
            $table->string('telefonoAdulto2')->nullable()->default(null);
            $table->string('nombreAdulto3')->nullable()->default(null);
            $table->string('edadAdulto3')->nullable()->default(null);
            $table->string('telefonoAdulto3')->nullable()->default(null);
            $table->string('nombreAdulto4')->nullable()->default(null);
            $table->string('edadAdulto4')->nullable()->default(null);
            $table->string('telefonoAdulto4')->nullable()->default(null);

            $table->string('telefonoCasa')->nullable()->default(null);
            $table->string('telefonoCelular')->nullable()->default(null);
            $table->string('telefonoTrabajo')->nullable()->default(null);
            $table->string('correo')->nullable()->default(null);
            $table->string('direccionCasa')->nullable()->default(null);
            $table->string('direccionTrabajo')->nullable()->default(null);
            $table->integer('noFamilia')->nullable()->default(null);
            $table->integer('noNino')->nullable()->default(null);
            $table->string('edadesNino')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);

            $table->integer('aprobacion')->default(0);
            $table->integer('state')->default(1);
            $table->integer('tipo')->default(1);
            
            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            
            $table->integer('autoriza')->nullable()->default(null)->unsigned();
            $table->foreign('autoriza')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adopciones');
    }
}
