<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformacionContacto extends Model
{
    protected $table = 'informacion_contacto';

    public function clients() {
        return $this->hasOne('App\Users', 'id', 'client');
    }

    public function apps() {
        return $this->hasOne('App\Users', 'id', 'app');
    }

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }
}
