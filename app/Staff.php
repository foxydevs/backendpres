<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staff';

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }

    public function apps() {
        return $this->hasOne('App\Users', 'id', 'app');
    }
}
