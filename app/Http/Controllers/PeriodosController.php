<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Periodos;
use Response;
use Validator;

class PeriodosController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(Periodos::whereRaw('parent IS NULL')->with('childs')->get(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Periodos::whereRaw('user_created=? and state=? and parent IS NULL',[$id,$state])->with('user')->with('childs')->get();
                    break;
                }
                case 'type':{
                    $objectSee = Periodos::whereRaw('user_created=? and tipo=? and parent IS NULL',[$id,$state])->with('user')->with('childs')->get();
                    break;
                }
                default:{
                    $objectSee = Periodos::whereRaw('user_created=? and state=? and parent IS NULL',[$id,$state])->with('user')->with('childs')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Periodos::whereRaw('user_created=? and state=? and parent IS NULL',[$id,$state])->with('user')->with('childs')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = Periodos::where('app','=',$id)->whereRaw('parent IS NULL')->with('users')->with('childs')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByPeriodos($id)
    {
        $objectSee = Periodos::where('parent','=',$id)->with('users')->with('parents')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'          => 'required',
            'user_created'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Periodos();
                $newObject->title            = $request->get('title');
                $newObject->description      = $request->get('description');
                $newObject->picture          = $request->get('picture');
                $newObject->video            = $request->get('video');
                $newObject->state            = $request->get('state');
                $newObject->type             = $request->get('type');
                $newObject->user_created     = $request->get('user_created');
                $newObject->app              = $request->get('app');
                $newObject->parent           = $request->get('parent');
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    public function uploadAvatar(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'avatar'      => 'required|image|mimes:jpeg,png,jpg'
        ]);
    
        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
    
                $path = Storage::disk('s3')->put($request->carpeta, $request->avatar);
    
                $objectUpdate->picture = Storage::disk('s3')->url($path);
                $objectUpdate->save();
    
                return Response::json($objectUpdate, 200);
    
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
            }
    
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = Periodos::whereRaw('id=?',$id)->with('parents','childs','users')->first();
        if ($objectSee) {

            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = Periodos::whereRaw('id=?',$id)->with('parents','childs','users')->first();
        if ($objectUpdate) {
            try {
                $objectUpdate->title            = $request->get('title', $objectUpdate->title);
                $objectUpdate->description      = $request->get('description', $objectUpdate->description);
                $objectUpdate->picture          = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->video            = $request->get('video', $objectUpdate->video);
                $objectUpdate->state            = $request->get('state', $objectUpdate->state);
                $objectUpdate->type             = $request->get('type', $objectUpdate->type);
                $objectUpdate->user_created     = $request->get('user_created', $objectUpdate->user_created);
                $objectUpdate->app              = $request->get('app', $objectUpdate->app);
                $objectUpdate->parent           = $request->get('parent', $objectUpdate->parent);
                
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = Periodos::find($id);
        if ($objectDelete) {
            try {
                Periodos::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
