<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresentacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presentacion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->double('price')->nullable()->default(0);
            $table->double('priceEspecial')->nullable()->default(0);
            $table->double('priceFinal')->nullable()->default(0);
            $table->double('quantity')->nullable()->default(0);
            $table->double('cost')->nullable()->default(0);
            $table->string('picture')->default('http://resinfer.com/wp-content/uploads/2015/09/productos.png');
            $table->integer('tiempo')->nullable()->default(0);
            $table->integer('periodo')->nullable()->default(0);
            $table->integer('membresia')->nullable()->default(0);
            $table->integer('nivel')->nullable()->default(0);
            $table->integer('tipo')->nullable()->default(0);
            $table->integer('opcion1')->nullable()->default(0);
            $table->integer('pos')->nullable()->default(0);
            $table->string('link1')->nullable()->default(null);
            $table->string('link')->nullable()->default(null);
            $table->string('archivo')->nullable()->default(null);
            $table->string('archivo1')->nullable()->default(null);
            $table->integer('duracion')->nullable()->default(null);
            $table->integer('periodo_duracion')->nullable()->default(null);
            $table->integer('state')->default(1);

            $table->integer('user_created')->nullable()->default(null)->unsigned();
            $table->foreign('user_created')->references('id')->on('users')->onDelete('cascade');

            $table->integer('app')->nullable()->default(null)->unsigned();
            $table->foreign('app')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presentacion');
    }
}
