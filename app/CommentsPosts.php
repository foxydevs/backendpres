<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentsPosts extends Model
{
    protected $table = 'comments_posts';

    public function thiss() {
        return $this->hasOne('App\Posts', 'id', 'post');
    }

    public function users() {
        return $this->hasOne('App\Users', 'id', 'user');
    }
}
