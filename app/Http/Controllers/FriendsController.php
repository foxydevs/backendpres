<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Friends;
use App\Users;
use App\UserInterest;
use Response;
use Collection;
use Validator;
use DB;

class FriendsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Friends::with('send','receipt')->get(), 200);
    }

    public function getFriends(Request $request, $id)
    {
        $user = Users::find($id);
        if ($user) {
            try {
                $senderUser  = Friends::whereRaw("user_send = ? AND state = 1", [$user->id])->with('receipt')->get();
                $receiveUSer = Friends::whereRaw("user_receipt = ? AND state = 1", [$user->id])->with('send')->get();
                $lat = $request->get('latitude');
                $lon = $request->get('longitude');
                $data = collect();

                foreach ($senderUser as $key => $value) {
                    $value->receipt->distance = $this->distance($value->receipt->last_latitud,$value->receipt->last_longitud,$lat,$lon,"K");
                    $data->push($value->receipt);
                }

                foreach ($receiveUSer as $key => $value) {
                    $value->send->distance = $this->distance($value->send->last_latitud,$value->send->last_longitud,$lat,$lon,"K");
                    $data->push($value->send);
                }

                return Response::json($data, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getNoFriends(Request $request, $id) {
        $user = Users::find($id);
        if ($user) {
            try {
                $senderUser  = Friends::whereRaw("user_send = ?", [$user->id])->with('receipt')->get();
                $receiveUSer = Friends::whereRaw("user_receipt = ?", [$user->id])->with('send')->get();
                $lat = $request->get('latitude');
                $lon = $request->get('longitude');

                $data = collect();

                foreach ($senderUser as $key => $value) {
                    $data->push($value->receipt->id);
                }

                foreach ($receiveUSer as $key => $value) {                    
                    $data->push($value->send->id);
                }

                $data->push($user->id);

                $nofriends = Users::whereNotIn("id", $data)->get();
                $myNoFriends = collect();

                foreach ($nofriends as $key => $value) {
                    $value->distance = $this->distance($value->last_latitud,$value->last_longitud,$lat,$lon,"K");
                    $myNoFriends->push($value);
                }    

                return Response::json($nofriends, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getInvitations(Request $request, $id)
    {
        $user = Users::find($id);
        if ($user) {
            try {
                $senderUser  = Friends::whereRaw("user_send = ? AND state = 0", [$user->id])->with('receipt')->get();
                $receiveUSer = Friends::whereRaw("user_receipt = ? AND state = 0", [$user->id])->with('send')->get();

                $data['send'] = $senderUser;
                $data['receive'] = $receiveUSer;

                return Response::json($data, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_send'     => 'required',
            'user_receipt'  => 'required'
        ]);

        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $evalObject = Friends::whereRaw('user_send = ? AND user_receipt = ?', [$request->get('user_send'), $request->get('user_receipt')])->first();
                $evalObject2 = Friends::whereRaw('user_send = ? AND user_receipt = ?', [$request->get('user_receipt'), $request->get('user_send')])->first();
                if ($evalObject || $evalObject2) {
                    $returnData = array (
                        'status' => 409,
                        'message' => 'Conflict with Entities'
                    );
                    return Response::json($returnData, 409);
                }
                else {
                    $newObject = new Friends();
                    $newObject->user_send = $request->get('user_send');
                    $newObject->user_receipt = $request->get('user_receipt');
                    $newObject->state = false;
                    $newObject->save();
                    
                    $userSend = Users::find($newObject->user_send);
                    $userReceipt = Users::find($newObject->user_receipt); 
                    $this->sendPushInvitate($userSend, $userReceipt);

                    return Response::json($newObject, 200);
                }
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    public function sendPushInvitate($userSend, $userReceipt)
    {   
        $fullname = $userSend->firstname." ".$userSend->lastname;
        $content = array('en' => 'You have a invitation from '.$fullname, 
                         'es' => 'Tienes una invitación de '.$fullname);

        $fields = array(
            'app_id' => '03e49561-83bf-466e-80b9-ce3021769d25',
            'include_player_ids' => array($userReceipt->one_signal_id),
            'data' => array('view' => 'InvitationView'),
            'contents' => $content
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Authorization: Basic MWZiMWRhZTUtODU1NC00ODIyLTlkNTYtN2Q4ZDgxN2Q5ZDQ5'));
    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    
        $response = curl_exec($ch);
        curl_close($ch);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Friends::find($id);
        if ($objectSee) {
            $objectSee->send;
            $objectSee->receipt;
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Friends::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->user_send = $request->get('user_send', $objectUpdate->user_send);
                $objectUpdate->user_receipt = $request->get('user_receipt', $objectUpdate->user_receipt);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->save();

                $objectUpdate->tipousuarios;
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    function getBoundaries($lat, $lng, $distance = 1, $earthRadius = 6371)
    {
        $return = array();
        
        // Los angulos para cada dirección
        $cardinalCoords = array('north' => '0',
                                'south' => '180',
                                'east' => '90',
                                'west' => '270');
        $rLat = deg2rad($lat);
        $rLng = deg2rad($lng);
        $rAngDist = $distance/$earthRadius;
        foreach ($cardinalCoords as $name => $angle)
        {
            $rAngle = deg2rad($angle);
            $rLatB = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
            $rLonB = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));
            $return[$name] = array('lat' => (float) rad2deg($rLatB), 
                                    'lng' => (float) rad2deg($rLonB));
        }
        return array('min_lat'  => $return['south']['lat'],
                    'max_lat' => $return['north']['lat'],
                    'min_lng' => $return['west']['lng'],
                    'max_lng' => $return['east']['lng']);
    }
    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
            } else {
                return $miles;
            }
    }
    public function getNearFriends(Request $request){
        try {
            $lat = $request->get('latitude');
            $lon = $request->get('longitude');
            $distance = $request->get('distance');
            $id = $request->get('id');
            $box = $this->getBoundaries($lat, $lon, $distance);
            $user = Users::find($id);
            if ($user) {
                try {
                    $USerNear = DB::table('users')->select(DB::raw('id, (6371 * ACOS( 
                                                    SIN(RADIANS(last_latitud)) 
                                                    * SIN(RADIANS(' . $lat . ')) 
                                                    + COS(RADIANS(last_longitud - ' . $lon . ')) 
                                                    * COS(RADIANS(last_latitud)) 
                                                    * COS(RADIANS(' . $lat . '))
                                                    )
                                    ) AS distance'))->whereRaw('((last_latitud BETWEEN ? AND ?) AND (last_longitud BETWEEN ? AND ?)) HAVING distance < ? ', 
                                    [$box['min_lat'], $box['max_lat'], $box['min_lng'], $box['max_lng'], $distance])->get();
                    
                    $data2 = collect();
                    foreach ($USerNear as $key => $value) {
                        $data2->push($value->id);
                    }

                    $senderUser  = Friends::whereRaw("user_send = ? and state=1", [$user->id])->whereIn('user_receipt',$data2)->with('receipt')->get();
                    $receiveUSer = Friends::whereRaw("user_receipt = ? and state=1", [$user->id])->whereIn('user_send',$data2)->with('send')->get();
                    $data = collect();
                    foreach ($senderUser as $key => $value) {
                        $value->receipt->distance = $this->distance($value->receipt->last_latitud,$value->receipt->last_longitud,$lat,$lon,"K");
                        $data->push($value->receipt);
                    }

                    foreach ($receiveUSer as $key => $value) {
                        $value->send->distance = $this->distance($value->send->last_latitud,$value->send->last_longitud,$lat,$lon,"K");
                        $data->push($value->send);
                    }


                    
                    return Response::json($data, 200);
                } catch (Exception $e) {
                    $returnData = array (
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                    return Response::json($returnData, 500);
                }
            }
            else {
                $returnData = array (
                    'status' => 404,
                    'message' => 'No record found'
                );
                return Response::json($returnData, 404);
            }
            return Response::json($eventsNear, 200);
        } catch (Exception $e) {
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
    }

    public function getNearNoFriends(Request $request){
        try {
            $lat = $request->get('latitude');
            $lon = $request->get('longitude');
            $distance = $request->get('distance');
            $id = $request->get('id');
            $box = $this->getBoundaries($lat, $lon, $distance);
            $user = Users::find($id);
            if ($user) {
                try {
                    $USerNear = DB::table('users')->select(DB::raw('id, (6371 * ACOS( 
                                                    SIN(RADIANS(last_latitud)) 
                                                    * SIN(RADIANS(' . $lat . ')) 
                                                    + COS(RADIANS(last_longitud - ' . $lon . ')) 
                                                    * COS(RADIANS(last_latitud)) 
                                                    * COS(RADIANS(' . $lat . '))
                                                    )
                                    ) AS distance'))->whereRaw('((last_latitud BETWEEN ? AND ?) AND (last_longitud BETWEEN ? AND ?)) HAVING distance < ? ', 
                                    [$box['min_lat'], $box['max_lat'], $box['min_lng'], $box['max_lng'], $distance])->get();
                    
                    $data2 = collect();
                    foreach ($USerNear as $key => $value) {
                        $data2->push($value->id);
                    }

                    $senderUser  = Friends::whereRaw("user_send = ?", [$user->id])->with('receipt')->get();
                    $receiveUSer = Friends::whereRaw("user_receipt = ?", [$user->id])->with('send')->get();
                    $data = collect();
                    $nearFriends = collect();
                    foreach ($senderUser as $key => $value) {
                        $value->receipt->distance = $this->distance($value->receipt->last_latitud,$value->receipt->last_longitud,$lat,$lon,"K");
                        $data->push($value->receipt->id);
                        $nearFriends->push($value);
                    }

                    foreach ($receiveUSer as $key => $value) {
                        $value->send->distance = $this->distance($value->send->last_latitud,$value->send->last_longitud,$lat,$lon,"K");
                        $data->push($value->send->id);
                    }

                    //$nearUsers = Users::whereRaw('id in (?) and id not in (?) and id != ?',[$data2,$data,$user->id])->get();
                    
                    $nearUsers = Users::whereIn('id',$data2)->whereNotIn('id',$data)->where('id','!=',$user->id)->get();
                    $idNearUsers = collect();
                    $nearUsers2 = collect();
                    foreach ($nearUsers as $key => $value) {
                        $value->distance = $this->distance($value->last_latitud,$value->last_longitud,$lat,$lon,"K");
                        $nearUsers2->push($value);
                        $idNearUsers->push($value->id);
                    }

                    if($interest = $request->get('interest')){
                        $nearUserInterest = UserInterest::where('name',$interest)->whereIn('user',$idNearUsers)->with('userInterest')->get();
                        $nearUsersInterest = collect();
                        foreach ($nearUserInterest as $key => $value) {
                            $value->userInterest->distance = $this->distance($value->userInterest->last_latitud,$value->userInterest->last_longitud,$lat,$lon,"K");
                            $nearUsersInterest->push($value->userInterest);
                        }
                        return Response::json($nearUsersInterest, 200);
                    }
                    return Response::json($nearUsers2, 200);
                } catch (Exception $e) {
                    $returnData = array (
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                    return Response::json($returnData, 500);
                }
            }
            else {
                $returnData = array (
                    'status' => 404,
                    'message' => 'No record found'
                );
                return Response::json($returnData, 404);
            }
            return Response::json($eventsNear, 200);
        } catch (Exception $e) {
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
    }
    public function getNearPeople(Request $request){
        try {
            $lat = $request->get('latitude');
            $lon = $request->get('longitude');
            $distance = $request->get('distance');
            $id = $request->get('id');
            $box = $this->getBoundaries($lat, $lon, $distance);
            $user = Users::find($id);
            if ($user) {
                try {
                    $USerNear = DB::table('users')->select(DB::raw('id, (6371 * ACOS( 
                                                    SIN(RADIANS(last_latitud)) 
                                                    * SIN(RADIANS(' . $lat . ')) 
                                                    + COS(RADIANS(last_longitud - ' . $lon . ')) 
                                                    * COS(RADIANS(last_latitud)) 
                                                    * COS(RADIANS(' . $lat . '))
                                                    )
                                    ) AS distance'))->whereRaw('((last_latitud BETWEEN ? AND ?) AND (last_longitud BETWEEN ? AND ?)) HAVING distance < ? ', 
                                    [$box['min_lat'], $box['max_lat'], $box['min_lng'], $box['max_lng'], $distance])->get();
                    
                    $idNearUsers = collect();
                    foreach ($USerNear as $key => $value) {
                        $idNearUsers->push($value->id);
                    }

                    //friends
                    $senderUser  = Friends::whereRaw("user_send = ? AND state = 1", [$user->id])->with('receipt')->get();
                    $receiveUSer = Friends::whereRaw("user_receipt = ? AND state = 1", [$user->id])->with('send')->get();

                    $idFriends = collect();
                    $Friends = collect();
                    foreach ($senderUser as $key => $value) {
                        $value->receipt->distance = $this->distance($value->receipt->last_latitud,$value->receipt->last_longitud,$lat,$lon,"K");
                        $idFriends->push($value->receipt->id);
                        $Friends->push($value->receipt);
                    }


                    foreach ($receiveUSer as $key => $value) {
                        $value->send->distance = $this->distance($value->send->last_latitud,$value->send->last_longitud,$lat,$lon,"K");
                        $idFriends->push($value->send->id);
                        $Friends->push($value->send);
                        
                    }

                    //$nearUsers = Users::whereRaw('id in (?) and id not in (?) and id != ?',[$data2,$data,$user->id])->get();
                    
                    $nearUsersSQL = Users::whereIn('id',$idNearUsers)->whereNotIn('id',$idFriends)->where('id','!=',$user->id)->get();
                    $nearFriendsSQL = Users::whereIn('id',$idNearUsers)->whereIn('id',$idFriends)->where('id','!=',$user->id)->get();
                    
                    $nearUsers = collect();
                    foreach ($nearUsersSQL as $key => $value) {
                        $value->distance = $this->distance($value->last_latitud,$value->last_longitud,$lat,$lon,"K");
                        $nearUsers->push($value);
                    }

                    $nearFriends = collect();
                    foreach ($nearFriendsSQL as $key => $value) {
                        $value->distance = $this->distance($value->last_latitud,$value->last_longitud,$lat,$lon,"K");
                        $nearFriends->push($value);
                    }
                    $returnData = array (
                        'nearpeople' => $nearUsers,
                        'nearfriends' => $nearFriends
                    );
                    
                    return Response::json($returnData, 200);
                } catch (Exception $e) {
                    $returnData = array (
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                    return Response::json($returnData, 500);
                }
            }
            else {
                $returnData = array (
                    'status' => 404,
                    'message' => 'No record found'
                );
                return Response::json($returnData, 404);
            }
            return Response::json($eventsNear, 200);
        } catch (Exception $e) {
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Friends::find($id);
        if ($objectDelete) {
            try {
                Friends::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    function removeFriend($user, $friend){
        $findObject  = Friends::whereRaw("(user_send = ? AND user_receipt = ? ) OR (user_send = ? AND user_receipt = ? )", [$user, $friend, $friend, $user])->first();
        if ($findObject) {
            try {
                Friends::destroy($findObject->id);
                return Response::json($findObject, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}