<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsUsers extends Model
{
    protected $table = 'products_users';

    public function clients() {
        return $this->hasOne('App\Users', 'id', 'client');
    }

    public function apps() {
        return $this->hasOne('App\Users', 'id', 'app');
    }

    public function documents() {
        return $this->hasOne('App\Documentos', 'id', 'documento');
    }

    public function products() {
        return $this->hasOne('App\Products', 'id', 'product');
    }
}
