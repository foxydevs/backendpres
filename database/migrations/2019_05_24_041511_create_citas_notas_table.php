<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitasNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas_notas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo')->nullable()->default(null);
            $table->string('descripcion')->nullable()->default(null);
            $table->string('comentario')->nullable()->default(null);
            $table->timestamp('fechaInicio')->useCurrent();
            $table->timestamp('fechaFin')->nullable()->default(null);

            $table->integer('leido')->default(0);
            $table->integer('state')->default(1);
            
            $table->integer('cita')->nullable()->default(null)->unsigned();
            $table->foreign('cita')->references('id')->on('citas')->onDelete('cascade');
            
            $table->integer('client')->nullable()->default(null)->unsigned();
            $table->foreign('client')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas_notas');
    }
}
